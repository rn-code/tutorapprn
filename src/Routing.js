import React, { useEffect, useState } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { useSelector } from 'react-redux'

//Creating Instance of Stack
const Stack = createStackNavigator();

//Screens
import SplashScreen from './screens/SplashScreen'

//Stacks
import AuthStack from './screens/AuthStack'
import HomeStack from './screens/HomeStack'
import TutorStack from './screens/TutorStack'

/** Main Stack of the app */
const MainStack = () => {
    const { isSignedIn, userType } = useSelector((state) => state.userSession)
    useEffect(() => {
        setTimeout(() => {
            console.log('isSignedIn', isSignedIn, userType)
        }, 9000)
    }, [])
    return (
        <Stack.Navigator headerMode="none" initialRouteName={"SplashScreen"}>
            <Stack.Screen name="SplashScreen" component={SplashScreen} />
            {isSignedIn === false && userType === 'Auth' ?
                <Stack.Screen name="AfterSplash" component={AuthStack} />
                : (userType === 'parent' || userType === 'individual' )&& isSignedIn ?
                    <Stack.Screen name="AfterSplash" component={HomeStack} />
                    : userType === 'tutor' && isSignedIn  ?
                        <Stack.Screen name="AfterSplash" component={TutorStack} />
                        : null
            }
        </Stack.Navigator>
    );
}

/** Theme will help to change app light mode to dark mode */
export default AppNavigator = () => {
    const MyTheme = {
        dark: false,
        colors: {
            primary: 'rgb(255, 45, 85)',
            background: 'rgb(255, 255, 255)',
            card: 'rgb(255, 255, 255)',
            text: 'rgb(28, 28, 30)',
            border: 'rgb(199, 199, 204)',
            notification: 'rgb(255, 69, 58)',
        },
    };

    return (
        <NavigationContainer theme={MyTheme}>
            <MainStack />
        </NavigationContainer>
    )
};
