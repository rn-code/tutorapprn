//auth actions...
import {
  USER_OBJECT,
  SET_COIN_NAME
} from './types';

export const setMarketCoinName = (coinName) => ({
  type: SET_COIN_NAME,
  coinName: coinName
});

export const setUserObjectQuery = (userObject) => ({
  type: USER_OBJECT,
  payload: userObject
});
