//auth actions...
import {
  PROFILE_GET_RESPONSE,
  PROFILE_UPDATE_RESPONSE
} from './types';

export const setGetProfile = (profileResponse) => (
  {
    type: PROFILE_GET_RESPONSE,
    payload: profileResponse
  });
export const setUpdateProfile = (profileResponse) => (
  {
    type: PROFILE_UPDATE_RESPONSE,
    payload: profileResponse
  });


