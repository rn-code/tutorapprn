/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useRef } from "react";
import { LogBox, StatusBar } from 'react-native'
import { store } from './store'
import { Provider } from 'react-redux';
import messaging from "@react-native-firebase/messaging";
import NotificationPopup from "react-native-push-notification-popup";
import FlashMessage from 'react-native-flash-message';
import SplashScreen from 'react-native-splash-screen'
import { useDispatch } from 'react-redux'

import Routing from './Routing'
import icons from "./assets/icons";
// import { userSessionChangeState } from "./store/actions/userSession"; 

LogBox.ignoreAllLogs(true)
StatusBar.setTranslucent(true)
StatusBar.setBackgroundColor('#00000000')
StatusBar.setBarStyle('dark-content')

const MainRouting = (props) => {

  const dispatch = useDispatch()

  const popupRef = useRef(null);
  const notificationListenerRef = useRef(null);
  const backgroundStateListenerRef = useRef(null);

  useEffect(() => {
    SplashScreen.hide();
    createNotificationListenersLatest();

    return () => {
      notificationListenerRef.current = null
      backgroundStateListenerRef.current = null
    }
  }, []);

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    console.log("requestUserPermission", "enabled", enabled);
  };
  const createNotificationListenersLatest = async () => {
    await requestUserPermission();
    // const token = await messaging().getToken()
    notificationListenerRef.current = messaging().onMessage(async (remoteMessage) => {
      console.log(
        "createNotificationListenersLatest",
        "notificationListener-remoteMessage",
        JSON.stringify(remoteMessage)
      );
      const { notification, data } = remoteMessage;
      const { title, body } = notification;
      showNotification(title, body, data);
    });
    backgroundStateListenerRef.current = messaging().onNotificationOpenedApp(
      async (remoteMessage) => {
        if (remoteMessage) {
          const { notification, data } = remoteMessage;
          // const { title } = notification
          console.log(
            "Notification caused app to open from backgroundStateListener:"
          );
          // handleNotification(data, data.status);
        }
      }
    );
  };

  const handleNotification = (title, body) => {

  };

  const showNotification = (title, body, data) => {
    // dispatch(userSessionChangeState({ notification_status: 1 }))
    if (popupRef.current) {
      popupRef.current.show({
        onPress: () => handleNotification(title, body),
        appIconSource: icons.notificationIcon,
        appTitle: "Onion",
        timeText: "Now",
        title: title,
        body: body,
        slideOutTime: 5000,
      });
    }
  };

  return (
    <>
      <Routing />
      <NotificationPopup ref={popupRef} style={{ zIndex: 99 }} />
      <FlashMessage position="bottom" floating={true} />
    </>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <MainRouting />
    </Provider>
  );
};

export default App