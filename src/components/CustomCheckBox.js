import React, { Component, useState } from 'react';
import { View, Image, TextInput, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import _ from 'lodash';
import Icons from './../assets/icons/'
import colors from './../utils/colors'
import fonts from '../assets/fonts'
const CustomCheckBox = props => {
    return (
        <View style={[{
            width: '100%', /* height: 50, */
            alignItems: 'center', flexDirection: 'row',
            marginBottom: 20,
        }, props.containerStyle]} >
            {props.isCircle === true ?
                <TouchableOpacity
                    onPress={props.onChange}
                    disabled={props.isDisabled ? props.isDisabled : false}
                    style={[styles.checkBoxContainer, {
                        backgroundColor: props.isChecked ? props.checkedColor : props.uncheckedColor,
                        borderWidth: props.isChecked ? 0: 0.8,
                        borderColor:colors.dgColor
                    }, props.checkstyle]}>
                </TouchableOpacity>
                :
                <TouchableOpacity
                    onPress={props.onChange}
                    disabled={props.isDisabled ? props.isDisabled : false}
                    style={[styles.checkBoxContainer, { backgroundColor: props.isChecked ? props.checkedColor : props.uncheckedColor }, props.checkstyle]}>
                    {props.isChecked &&
                        <Image
                            style={{ width: '65%', height: '65%', resizeMode: "contain", tintColor: !_.isNil(props.tintColor) ? props.tintColor : 'white' }}
                            source={Icons.tickIcon}
                        />
                    }
                </TouchableOpacity>
            }
            <View onPress={props.onChangeTerm}>
                {props.customTxt ?
                    <Text style={styles.errorTxt}>{props.customTxt}</Text>
                    :
                    <Text style={styles.errorTxt}>{"I’ve read and agree with"}<Text style={{ textDecorationLine: 'underline' }}>
                        {"Terms of Service"}</Text> {"and"}
                        <Text style={{ textDecorationLine: 'underline' }}> {"Privacy Policy."}</Text>
                    </Text>
                }
            </View>
            {/* <View style={{ flex: 1, marginLeft: 10, }}>
                <Text style={[{
                    width: '100%',
                    fontSize: 18,
                    color: 'black',
                }, props.labelStyle]}>{props.label}</Text>
                {!_.isNil(props.label2) &&
                <Text style={[{
                    width: '100%',
                    fontSize: 14,
                    color: '#999999',
                }, props.labelStyle2]}>{props.label2}</Text>
            }
            </View> */}
        </View>
    )
}

export default CustomCheckBox;
const styles = StyleSheet.create(
    {
        checkBoxContainer: {
            width: 20,
            height: 20,
            borderRadius: 3,
            backgroundColor: '#F4F4F4',
            alignItems: 'center',
            justifyContent: 'center',
        },
        errorTxt: {
            color: '#0A0D31',
            fontSize: 12,
            fontFamily: fonts.Medium,
            lineHeight: 18,
            marginLeft: 10,
            // width: '85%',
        },
        errorImageStyle: {
            width: 16,
            height: 16,
            marginRight: 10,
            resizeMode: 'cover'
        },
    }
)
