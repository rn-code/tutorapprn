import React, { Component, useEffect, useState } from 'react';
import {
    View,
    Text,
    Image,
    Alert,
    StyleSheet,
    ScrollView,
    Share,
    TouchableOpacity,
    Pressable,
    Linking
} from 'react-native';
import { useSelector, useDispatch } from "react-redux";

//Assets
import fonts from '../assets/fonts';
import icons from '../assets/icons';

//Utils
import colors from '../utils/colors';

//Components
// import DrawerItem from './DrawerItem';

import { logoutAPI } from '../api/methods/auth'
import { logoutUser } from '../actions/userSession'
import Loader from './Loader'
import AvatarComponent from './AvatarComponent'
import { userSessionChangeState } from '../actions/userSession'

const DrawerItem = (props) => {
    const dispatch = useDispatch()

    return (
        <TouchableOpacity
            activeOpacity={0.5}
            style={[styles.drawerItemContainer, props.containerStyle]}
            onPress={() => {
                if (props.onPress && typeof props.onPress == 'function') props.onPress()
            }}>
            {/* <Image style={styles.itemLeftImage} source={props.icon} /> */}
            <Text style={[styles.itemText, props.titleStyle]}>{props.title}</Text>
            {props.isNotification &&
            <View style={styles.notificationContainer}>
            <Text style={[styles.notificationCounter, props.titleStyle]}>{'13'}</Text>
            </View>
            }
            {props.loading &&
                <ActivityIndicator
                    animating={props.loading}
                    size={'small'}
                    color={colors.lightBlack}
                    style={[{ marginLeft: 5 }]}
                />
            }
        </TouchableOpacity>
    )
}

const CompanyDrawerContent = (props) => {
    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch()
    const { currentUser } = useSelector((state) => state.userSession)
    useEffect(() => {
        console.log('currentUser-drawer', currentUser?.name)
    }, [])

    const navigateToNext = (nextRoute, isUserNotRequired = false) => {
        if (isUserNotRequired || currentUser?.name) {
            navigation.closeDrawer()
            navigation.navigate(nextRoute)
        } else {
            dispatch(userSessionChangeState({
                isSignedIn: false,
                showBack: true
            }))
        }
    }

    return (
        <ScrollView
            bounces={false}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.drawerContainer}>
                <View style={styles.topContainer}>
                    <Pressable
                        onPress={() => {
                        }}
                        style={styles.profileContainer}>
                        <AvatarComponent
                            source={null}
                            defaultSource={icons.dumyIcon}
                            style={[styles.imageContainer]}
                            size={'small'}
                            imageStyle={{ resizeMode: 'cover' }}
                        />
                        <View>
                            <Text style={styles.profileName}>{'John Doe'}</Text>
                            <Text style={styles.profileEmail}>{'myname@email.com'}</Text>
                        </View>
                    </Pressable>
                </View>
                <DrawerItem
                    title={'Dashboard'}
                    containerStyle={{ marginTop: 30 }}
                    onPress={() => {
                        navigateToNext('DashboardScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Classes'}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        navigateToNext('ClassesScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Messages'}
                    isNotification={true}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        navigateToNext('ChatScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Purchases'}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        navigateToNext('PurchaseScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Notifications'}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        // navigateToNext('AboutUsScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Profile and settings'}
                    containerStyle={{ marginTop: 25 ,marginBottom:27}}
                    onPress={() => {
                        navigateToNext('ProfileSettingScreen', true)
                    }}
                />
                 <View style={styles.barStyle} />
                <DrawerItem
                    title={'Home'}
                    containerStyle={{ marginTop: 27 }}
                    onPress={() => {
                        navigateToNext('HomeScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Search'}
                    containerStyle={{ marginTop: 25,marginBottom:30 }}
                    onPress={() => {
                        navigateToNext('SearchScreen', true)
                    }}
                />
                <View style={styles.barStyle} />
                <DrawerItem
                    title={'Sign out'}
                    containerStyle={{ 
                        marginTop: 27 ,
                        marginBottom:27
                    }}
                    titleStyle={{color:colors.lightRed}}
                    onPress={() => {
                        // navigateToNext('AboutUsScreen', true)
                        dispatch(logoutUser())
                    }}
                />

            </View>
            <Loader isShowIndicator={false} loading={loading} />
        </ScrollView>
    )
}
export default CompanyDrawerContent
const styles = StyleSheet.create({
    drawerContainer: {
        flex: 1,
        // alignItems: 'center',
    },
    topContainer: {
        backgroundColor: '#F1F8F8',
        width: '100%',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 43,
        paddingBottom: 25,
    },
    profileContainer: {
    },
    imageContainer: {
        width: 73,
        height: 73,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    drawerItemContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 20,
        marginRight:20,
        justifyContent:'space-between'
    },
    itemLeftImage: {
        width: 12,
        height: 13,
        resizeMode: 'contain'
    },
    itemText: {
        color: '#0A0D31',
        fontSize: 14,
        width:'60%',
        fontFamily: fonts.Regular,
    },
    profileName:{
        color:'#0A0D31',
        fontSize:18,
        fontFamily:fonts.Bold,
        marginTop:10,
    },
    profileEmail:{
        color:colors.dgColor,
        fontSize:14,
        fontFamily:fonts.Regular,
    },
    notificationCounter:{
        color:colors.white,
        fontSize:12,
        fontFamily:fonts.Medium,
    },
    barStyle:{
        height:1,
        width:'90%',
        backgroundColor:colors.dgColor,
        alignSelf:'flex-end'
    },
    notificationContainer:{
        width:18,
        height:18,
        borderRadius:9,
        backgroundColor:colors.lightGreen,
        alignItems:'center',
        justifyContent:'center'
    }
})