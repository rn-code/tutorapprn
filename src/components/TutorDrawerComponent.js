import React, { Component, useEffect, useState } from 'react';
import {
    View,
    Text,
    Image,
    Alert,
    StyleSheet,
    ScrollView,
    Share,
    TouchableOpacity,
    Pressable,
    Linking
} from 'react-native';
import { useSelector, useDispatch } from "react-redux";

//Assets
import fonts from '../assets/fonts';
import icons from '../assets/icons';

//Utils
import colors from '../utils/colors';

//Components
// import DrawerItem from './DrawerItem';

import { logoutAPI } from '../api/methods/auth'
import { logoutUser } from '../actions/userSession'
import Loader from './Loader'
import AvatarComponent from './AvatarComponent'
import { userSessionChangeState } from '../actions/userSession'

const DrawerItem = (props) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            style={[styles.drawerItemContainer, props.containerStyle]}
            onPress={() => {
                if (props.onPress && typeof props.onPress == 'function') props.onPress()
            }}>
            {/* <Image style={styles.itemLeftImage} source={props.icon} /> */}
            <Text style={[styles.itemText, props.titleStyle]}>{props.title}</Text>
            {props.isNotification &&
                <View style={styles.notificationContainer}>
                    <Text style={[styles.notificationCounter, props.titleStyle]}>{'9'}</Text>
                </View>
            }
            {props.loading &&
                <ActivityIndicator
                    animating={props.loading}
                    size={'small'}
                    color={colors.lightBlack}
                    style={[{ marginLeft: 5 }]}
                />
            }
        </TouchableOpacity>
    )
}

const CompanyDrawerContent = (props) => {
    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch()
    const { currentUser } = useSelector((state) => state.userSession)
    useEffect(() => {
        console.log('currentUser-drawer', currentUser?.name)
    }, [])

    const navigateToNext = (nextRoute, isUserNotRequired = false) => {
        if (isUserNotRequired || currentUser?.name) {
            navigation.closeDrawer()
            navigation.navigate(nextRoute)
        } else {
            dispatch(userSessionChangeState({
                isSignedIn: false,
                showBack: true
            }))
        }
    }

    return (
        <View
            // bounces={false}
            // showsHorizontalScrollIndicator={false}
            // showsVerticalScrollIndicator={false}
            // contentContainerStyle={{ flexGrow: 1 }}
            style={{ flex: 1 }}
        >
            <View style={styles.drawerContainer}>
                <View style={styles.topContainer}>
                    <Pressable
                        onPress={() => {
                        }}
                        style={styles.profileContainer}>
                        <AvatarComponent
                            source={null}
                            defaultSource={icons.dumyIcon}
                            style={[styles.imageContainer]}
                            size={'small'}
                            imageStyle={{ resizeMode: 'cover' }}
                        />
                        <View>
                            <Text style={styles.profileName}>{'Tutor Id'}</Text>
                            <Text style={styles.profileEmail}>{'tutor@gmail.com'}</Text>
                        </View>
                    </Pressable>
                </View>
                <DrawerItem
                    title={'Dashboard'}
                    containerStyle={{ marginTop: 30 }}
                    onPress={() => {
                        navigateToNext('DashboardScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Classes'}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        navigateToNext('ClassesScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Messages'}
                    isNotification={true}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        navigateToNext('ChatScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Earnings'}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        navigateToNext('EarningScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Notifications'}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        navigateToNext('NotificationScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Profile and settings'}
                    containerStyle={{ marginTop: 25 }}
                    onPress={() => {
                        navigateToNext('ProfileSettingScreen', true)
                    }}
                />
                <DrawerItem
                    title={'Subscription'}
                    containerStyle={{ marginTop: 25, marginBottom: 27 }}
                    onPress={() => {
                        navigateToNext('SubscriptionScreen', true)
                    }}
                />
                <View style={{position:'absolute',bottom:30,width:'100%'}}>
                    <View style={styles.barStyle} />
                    <DrawerItem
                        title={'Sign out'}
                        containerStyle={{
                            marginTop: 27,
                            marginBottom: 27,

                        }}
                        titleStyle={{ color: colors.lightRed }}
                        onPress={() => {
                            // navigateToNext('AboutUsScreen', true)
                            dispatch(logoutUser())

                        }}
                    />
                </View>
            </View>
            <Loader isShowIndicator={false} loading={loading} />
        </View>
    )
}
export default CompanyDrawerContent
const styles = StyleSheet.create({
    drawerContainer: {
        flex: 1,
        // alignItems: 'center',
    },
    topContainer: {
        backgroundColor: '#F1F8F8',
        width: '100%',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 43,
        paddingBottom: 25,
    },
    profileContainer: {
    },
    imageContainer: {
        width: 73,
        height: 73,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    drawerItemContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 20,
        marginRight: 20,
        justifyContent: 'space-between'
    },
    itemLeftImage: {
        width: 12,
        height: 13,
        resizeMode: 'contain'
    },
    itemText: {
        color: '#0A0D31',
        fontSize: 14,
        width: '60%',
        fontFamily: fonts.Regular,
    },
    profileName: {
        color: '#0A0D31',
        fontSize: 18,
        fontFamily: fonts.Bold,
        marginTop: 10,
    },
    profileEmail: {
        color: colors.dgColor,
        fontSize: 14,
        fontFamily: fonts.Regular,
    },
    notificationCounter: {
        color: colors.white,
        fontSize: 12,
        fontFamily: fonts.Medium,
    },
    barStyle: {
        height: 1,
        width: '90%',
        backgroundColor: colors.dgColor,
        alignSelf: 'flex-end'
    },
    notificationContainer: {
        width: 18,
        height: 18,
        borderRadius: 9,
        backgroundColor: colors.lightGreen,
        alignItems: 'center',
        justifyContent: 'center'
    }
})