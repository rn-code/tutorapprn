import { postRequest, getRequest, putRequest } from '../index';

export const updateProfileAPI = (payload) =>
    putRequest(`/api/tutor/update-profile`, payload);
