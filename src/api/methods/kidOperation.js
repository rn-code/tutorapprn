import { postRequest, getRequest, putRequest } from '../index';

export const addKidAPI = (payload) =>
    putRequest(`/parent/add-kid`, payload);

export const removeKidAPI = (payload) =>
    putRequest(`/parent/remove-kid/`+payload);

export const updateKidAPI  = (payload) =>
    putRequest(`/parent/update-kid/`+payload.id  ,payload);

export const geKidDataAPI = (payload) =>
    getRequest(`/parent/get-all-kids`);
