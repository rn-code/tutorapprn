import { postRequest, getRequest, putRequest } from '../index';

export const sigupAPI = (payload) =>
    postRequest(`/auth/sign-up`, payload);

export const loginAPI = (payload) =>
    postRequest(`/auth/login`, payload);
    
export const forgetPasswordAPI = (payload) =>
    putRequest(`/auth/forgot-password`, payload);

export const resetPasswordAPI = (payload) =>
    putRequest(`/auth/reset-password`, payload);

export const otpVerifyPasswordAPI = (payload) =>
    putRequest(`/auth/otp-verify`, payload);

export const getUserDataAPI = (payload) =>
    getRequest(`/auth/me`);
