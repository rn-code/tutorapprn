import axios from 'axios';

import { store } from '../store'

const ROOT_URL = 'https://tutor-service-backend.herokuapp.com/api';

const BASE_URL = `${ROOT_URL}`;

const client = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 18000
});

client.interceptors.request.use(
  async (config) => {
    const requestConfig = config;
    const { authToken } = store.getState().userSession
    console.log('authToken',authToken)
    if (authToken != null) {
      requestConfig.headers = {
        'Authorization': `Bearer ${authToken}`,
      };
    }
    return requestConfig;
  },
  (err) => {
    console.log('client.interceptors-->err', err)
    return Promise.reject(err);
  },
);

export {
  ROOT_URL,
  BASE_URL,
  client,
};
