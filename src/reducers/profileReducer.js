import * as types from '../actions/types'

const INITIAL_STATE = {
    getProfile: null,
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {

        case types.PROFILE_GET_RESPONSE:
            return {
                ...state,
                getProfile: action.payload
            };
        default:
            return state;
    }
}