import * as types from '../actions/types'

const INITIAL_STATE = {
    userObjectQuery: null,
    marketCoinName: 'BTC'
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.SET_COIN_NAME:
            return {
                ...state,
                marketCoinName: action.coinName,
            };
        case types.USER_OBJECT:
            return {
                ...state,
                userObjectQuery: action.payload,
            };
        default:
            return state;
    }
}