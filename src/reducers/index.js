import { combineReducers } from 'redux';
import userSession from './userSession';
import generalParam from './generalParam'

export default combineReducers({
  userSession: userSession,
  generalParam: generalParam
});
