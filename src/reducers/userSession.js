import * as types from '../actions/types'

const INITIAL_STATE = {
    currentUser: null,
    isSignedIn: false,
    authToken: null,
    showBack: false,
    userType:'Auth'
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.LOGOUT:
            return INITIAL_STATE;
        case types.USER_SESSION_CHANGE_STATE:
            return {
                ...state,
                ...action.payload,
            };
        case types.SET_USER:
            return {
                ...state,
                currentUser: action.payload.data
            };
        case types.LOGIN_RESPONSE:
            return {
                ...state,
                currentUser: action.payload.data,
                isSignedIn: true,
                userType: action.payload?.user_type,
                authToken: action.payload.token
            };
        case types.SIGNUP_RESPONSE:
            return {
                ...state,
                currentUser: action.payload.data,
                isSignedIn: true , 
                userType: action.payload?.user_type,
                // isSignedIn: action.payload?.user_type==='parent' ? false : action.payload?.user_type==='individual' ? true : true , 
                authToken: action.payload.token
            };
        default:
            return state;
    }
}