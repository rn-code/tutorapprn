import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    FlatList,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Dimensions
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { Calendar, LocaleConfig } from 'react-native-calendars';

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";

import { styles } from './Styles'
import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
import { Item } from "native-base";
import { color } from "react-native-reanimated";
import moment from "moment";

LocaleConfig.locales['fr'] = {
    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    today: 'Aujourd\'hui',
    dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
};
LocaleConfig.defaultLocale = 'fr';


const DashboardScreen = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [isWeekly, setIsWeekly] = useState(true);
    const [classCountList, setClassCountList] = useState([
        {classCount:4,name:'Upcoming Classes'},
        {classCount:1,name:'Completed Classes'},
        {classCount:5,name:'Total Hours'},
        {classCount:51,name:'Total students'},
    ]);
    const [slotDetail, setSlotDetail] = useState([
        {
            description: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            description: 'Learn how to read and write while having fun - kindergarten',

        },

    ]);
    const [weeklyList, setWeeklyList] = useState([
        {
            date: '2021-12-21', weekDayName: 'M'
        },
        {
            date: '2021-12-22', weekDayName: 'T'
        },
        {
            date: '2021-12-23', weekDayName: 'W'
        },
        {
            date: '2021-12-24', weekDayName: 'T'
        },
        {
            date: '2021-12-25', weekDayName: 'F'
        },

    ]);



    const renderWeeklyItem = ({ item, index }) => {
        return (
            <Pressable
                style={
                    index === 0 ?
                        styles.weeklyMainBorderContainer :
                        styles.weeklyMainContainer
                }>
                <Text style={
                    index === 0 ?
                        styles.weeklyBorderText :
                        styles.weeklyText
                }>
                    {moment(item.date).format('MMM')}
                </Text>
                <Text style={[
                    index === 0 ?
                        styles.weeklyBorderText :
                        styles.weeklyText,
                    { marginTop: 10, marginBottom: 10 }
                ]}>
                    {moment(item.date).format('DD')}
                </Text>
                <Text style={
                    index === 0 ?
                        styles.weeklyBorderText :
                        styles.weeklyText
                }>
                    {item.weekDayName}
                </Text>
            </Pressable>
        );
    };
    const renderSlotDetailItem = ({ item, index }) => {
        return (
            <Pressable
                style={[styles.itemMainContainer, {
                    paddingBottom: index === slotDetail.length - 1 ? 30 : 0,
                    borderBottomWidth: index === slotDetail.length - 1 ? 1 : 0,
                }]}>
                <View style={styles.leftContainer}>
                    <Image
                        source={icons.dumyIcon}
                        style={styles.leftImageItem}
                    />
                    <Text style={styles.editText}>Edit time</Text>
                </View>
                <View style={styles.rightContainer}>
                    <Text style={styles.headingItem}>{item.description}</Text>
                    <Text style={[styles.upcomingText, { marginTop: 15 }]}>
                        30 min - Meeting 1 of total 6
                    </Text>
                    <View style={styles.rowContainer}>
                        <Text style={styles.upcomingText}>1 - 1 class</Text>
                        <Text style={[styles.upcomingText, { marginLeft: 35 }]}>Online</Text>
                    </View>
                    <Text style={styles.itemName}>John Doe, Ayesha, ...</Text>
                </View>
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No chat exist'}</Text>
            </View>
        )
    };

    return (
        <View style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Dashboard'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    // navigation.toggleDrawer();
                }}
            />
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                style={{
                    marginTop: 10,
                    paddingLeft: 22,
                    paddingRight: 22
                }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.profileHeader}>
                    <Image
                        source={icons.dumyIcon}
                        style={styles.profileIcon}
                    />
                    <Text style={styles.headerText}>Hi, Grace!</Text>
                </View>
                <View style={styles.classesListContainer}>
                    {
                       classCountList.map((item, index) => {
                            return (
                                <View style={styles.classListItemContainer}>
                                    <Text style={styles.headingClassItem}>{item.classCount}</Text>
                                    <Text style={styles.descriptionClassItem}>{item.name}</Text>
                                </View>
                            )
                        })
                    }
                </View>
                <Text style={styles.upcompingText}>Upcoming classes</Text>
                <View style={styles.upcomingViewContainer}>
                    <Text style={styles.upcomingContainerHeading}>Today</Text>
                    <View style={styles.arrowContainer}>
                        <TouchableOpacity>
                            <Image
                                source={icons.leftArrowIcon}
                                style={styles.arrowIcon}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image
                                source={icons.rightArrowIcon}
                                style={styles.arrowIcon}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.topBarcontainer}>
                    <Pressable
                        style={
                            isWeekly ?
                                [styles.activeStyle, {
                                    borderTopLeftRadius: 10,
                                    borderBottomLeftRadius: 10,
                                }] :
                                [styles.unActiveStyle, {
                                    borderTopLeftRadius: 10,
                                    borderBottomLeftRadius: 10,
                                }]
                        }
                        onPress={() => {
                            if (!isWeekly) setIsWeekly(!isWeekly)
                        }}>
                        <Text
                            style={
                                isWeekly ?
                                    styles.activeTextStyle :
                                    styles.unActiveTextStyle
                            }>
                            Weekly
                        </Text>
                    </Pressable>
                    <Pressable
                        style={
                            !isWeekly ?
                                [styles.activeStyle, {
                                    borderTopRightRadius: 10,
                                    borderBottomRightRadius: 10,
                                }] :
                                [styles.unActiveStyle, {
                                    borderTopRightRadius: 10,
                                    borderBottomRightRadius: 10,
                                    borderRightWidth: 1,
                                }]
                        }
                        onPress={() => {
                            if (isWeekly) setIsWeekly(!isWeekly)
                        }}>
                        <Text
                            style={
                                !isWeekly ?
                                    styles.activeTextStyle :
                                    styles.unActiveTextStyle
                            }>
                            Monthly
                        </Text>
                    </Pressable>

                </View>

                {isWeekly ?
                    <View>
                        <FlatList
                            horizontal
                            data={weeklyList}
                            showsVerticalScrollIndicator={false}
                            // contentContainerStyle={{  }}
                            renderItem={renderWeeklyItem}
                            ListEmptyComponent={renderEmptyContainer}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                    :
                    <View style={styles.calendarAdjustment}>
                        <Calendar
                            firstDay={1}
                            current={'2021-11-01'}
                            minDate={'2012-05-10'}
                            maxDate={'2050-05-30'}
                            hideExtraDays={true}
                            hideArrows={true}
                            monthFormat={'MMMM'}
                            onPressArrowLeft={subtractMonth => subtractMonth()}
                            onPressArrowRight={addMonth => addMonth()}
                            onDayPress={(day) => { console.log('selected day', day) }}
                            onDayLongPress={(day) => { console.log('selected day', day) }}
                            onMonthChange={(month) => { console.log('month changed', month) }}

                            theme={{

                                'stylesheet.calendar.header': {
                                    header: {
                                        flexDirection: 'row',
                                        paddingLeft: 0,
                                        paddingRight: 0,
                                        marginTop: 6,
                                    },
                                },

                                dayTextColor: colors.dgColor,
                                monthTextColor: colors.black,
                                textSectionTitleColor: 'black',
                                textSectionTitleDisabledColor: 'black',
                                selectedDayBackgroundColor: 'black',
                                selectedDayTextColor: 'black',

                                textDayFontFamily: fonts.Medium,
                                textMonthFontFamily: fonts.Medium,
                                textDayHeaderFontFamily: fonts.Medium,
                                textMonthFontWeight: '500',

                                textDayFontSize: 14,
                                textMonthFontSize: 14,
                                textDayHeaderFontSize: 14
                            }}

                            markingType={'custom'}
                            markedDates={{
                                '2021-11-11': {
                                    customStyles: {
                                        container: {
                                            backgroundColor: colors.lightOrange,
                                        },
                                        text: {
                                            color: colors.white,
                                            fontWeight: '600'
                                        }
                                    }
                                },
                                '2021-11-20': {
                                    customStyles: {
                                        container: {
                                            backgroundColor: colors.lightGreen,
                                        },
                                        text: {
                                            color: colors.white,
                                            fontWeight: '600'
                                        }
                                    }
                                }
                            }}

                        />
                    </View>
                }
                <View style={styles.mainView}>
                    <FlatList
                        data={slotDetail}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ marginBottom: 100 }}
                        renderItem={renderSlotDetailItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </View>
    );
};

export default DashboardScreen;
