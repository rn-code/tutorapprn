import { StyleSheet } from 'react-native';
import colors from '../../../utils/colors'
import fonts from '../../../assets/fonts'
export const styles = StyleSheet.create({
  imageBackgroundStyle: {
    flex:1,
    backgroundColor:colors.lightGreen,
  },
  safeStyle: {
    flex:1,
    backgroundColor:colors.lightGreen,
    opacity:.8
  },
  backBtnContainer:{
    flexDirection:'row',
    marginTop:15,
    alignItems:'center',
    paddingLeft:22
  },
  headerText:{
    fontFamily:fonts.Bold,
    fontSize:24,
    lineHeight:36,
    color:colors.white
  },
  backImage:{
    width:19,
    height:19,
    resizeMode:'contain',
    marginRight:13,
    tintColor:colors.white
  },
  organizedBtnStyle:{
    backgroundColor:colors.white,
    height:36,
    marginTop:20
  },
  headingText:{
      fontFamily:fonts.Bold,
      fontSize:14,
      color:colors.white,
      marginTop:20
  },
  descriptionText:{
    fontSize:12,
    fontFamily:fonts.Medium,
    lineHeight:18,
    marginTop:5,
    color:colors.white
  },
  rowContainer:{
    marginTop:20,
    flexDirection:'row'
  },
  rowHeadingText:{
    fontFamily:fonts.Medium,
    fontSize:12,
    color:colors.white
  },
  rowDescriptionText:{
    fontFamily:fonts.Bold,
    fontSize:12,
    color:colors.white,
    marginLeft:10
  },
  emptyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    width:'33%',
    height:132,
    marginTop: 10,
    paddingLeft:5,
    paddingRight:5
  },
  galleryImage:{
    width:'100%',
    height:132,
    borderRadius:10
  }
});
