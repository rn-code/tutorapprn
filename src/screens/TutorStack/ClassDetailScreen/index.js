import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { useSelector } from 'react-redux'

import { isIphoneX } from "../../../utils/isIphoneX"
import Header from "../../../components/Header";
import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')

import ClassRoomComponent from "./ClassRoomComponent";
import AvailablityComponent from "./AvailablityComponent";

const ClassDetailScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [messageText, setMessageText] = useState('');

    const [isClassRoom, setIsClassRoom] = useState(true);

    useEffect(() => {

    }, [])

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Classes'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                }}
            />
            <View style={styles.mainContainer}>
                <View style={{
                    paddingLeft: 20,
                    paddingRight: 20
                }}>

                    {/* <Text style={[styles.dateText]}>Mon - Thu, Dec 10 - Jan 27 at 18:00 Istanbul time</Text> */}
                    <View style={styles.topBarcontainer}>
                        <Pressable
                            style={
                                isClassRoom ?
                                    [styles.activeStyle, {
                                        borderTopLeftRadius: 10,
                                        borderBottomLeftRadius: 10,
                                    }] :
                                    [styles.unActiveStyle, {
                                        borderTopLeftRadius: 10,
                                        borderBottomLeftRadius: 10,
                                    }]
                            }
                            onPress={() => {
                                if (!isClassRoom) setIsClassRoom(!isClassRoom)
                            }}>
                            <Text
                                style={
                                    isClassRoom ?
                                        styles.activeTextStyle :
                                        styles.unActiveTextStyle
                                }>
                                My Classes
                            </Text>
                        </Pressable>
                        <Pressable
                            style={
                                !isClassRoom ?
                                    [styles.activeStyle, {
                                        borderTopRightRadius: 10,
                                        borderBottomRightRadius: 10,
                                    }] :
                                    [styles.unActiveStyle, {
                                        borderTopRightRadius: 10,
                                        borderBottomRightRadius: 10,
                                        borderRightWidth: 1,
                                    }]
                            }
                            onPress={() => {
                                if (isClassRoom) setIsClassRoom(!isClassRoom)
                            }}>
                            <Text
                                style={
                                    !isClassRoom ?
                                        styles.activeTextStyle :
                                        styles.unActiveTextStyle
                                }>
                                Availablity
                            </Text>
                        </Pressable>

                    </View>
                   {isClassRoom&&
                    <Pressable
                        onPress={() => {
                            navigation.goBack()
                        }}
                        style={styles.backBtnContainer}>
                        <Image
                            style={styles.backImage}
                            source={icons.backIcon}
                        />
                        <Text style={styles.headerText}>Learn how to read and write while having fun - kindergarten</Text>
                    </Pressable>
                }
                </View>
                {isClassRoom ?
                <ClassRoomComponent />
                :
                <AvailablityComponent />
                }

            </View>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default ClassDetailScreen;
