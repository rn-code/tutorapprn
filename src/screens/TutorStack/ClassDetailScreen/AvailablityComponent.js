import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
    TextInput,
    Keyboard,
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView
} from "react-native";
import moment from 'moment'
import { useSelector } from 'react-redux'
import { Picker } from 'native-base';
import { Rating } from 'react-native-ratings';

import { isIphoneX } from "../../../utils/isIphoneX"
import Header from "../../../components/Header";
import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";
import fonts from "../../../assets/fonts";
import { color } from "react-native-reanimated";
import DatePicker from 'react-native-date-picker'

const { width, height } = Dimensions.get('window')



const AvailablityComponent = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [classTiming, setClassTiming] = useState([
        { time: '15 min' },
        { time: '30 min' },
        { time: '45 min' },
        { time: '1 hour' },
        { time: '1.5 hour' },
        { time: '2 hour' },
        { time: 'Custom' },
    ])
    const [bookList, setBookList] = useState([
        { name: 'Online class' },
        { name: 'Tutor’s home' },
        { name: 'Student’s home' }
    ])

    const [bufferBeforeTime, setBufferBeforeTime] = useState('15 minutes')
    const [bufferAfterTime, setBufferAfterTime] = useState('15 minutes')
    const [shortNotice, setShortNotice] = useState('15 hours')
    const [futureNotice, setFutureNotice] = useState('15 months')


    const [date, setDate] = useState(new Date())
    const [mondayTime, setTimeMonday] = useState(new Date())
    const [tuesdayTime, setTimeTuesday] = useState(new Date())
    const [wednesdayTime, setTimeWednesday] = useState(new Date())
    const [thursdayTime, setTimeThursday] = useState(new Date())
    const [fridayTime, setTimeFriday] = useState(new Date())
    const [saturdayTime, setTimeSaturday] = useState(new Date())
    const [sundayTime, setTimeSunday] = useState(new Date())

    const [open, setOpen] = useState(false)
    const [selectedDay, setSelectedDay] = useState('')
    const [openFirstTimeMon, setOpenFirstTimeMon] = useState(false)
    const [openFirstTimeTue, setOpenFirstTimeTue] = useState(false)
    const [openFirstTimeWed, setOpenFirstTimeWed] = useState(false)
    const [openFirstTimeThu, setOpenFirstTimeThu] = useState(false)
    const [openFirstTimeFri, setOpenFirstTimeFri] = useState(false)
    const [openFirstTimeSat, setOpenFirstTimeSat] = useState(false)
    const [openFirstTimeSun, setOpenFirstTimeSun] = useState(false)



    const [fromdate, setFromDate] = useState(new Date())
    const [untildate, setUntilDate] = useState(new Date())
    const [pickerDate, setPickerDate] = useState(new Date())
    const [openDate, setOpenDate] = useState(false)
    const [isFromDateOpen, setIsFromDateOpen] = useState(false)

    useEffect(() => {

    }, [])

    const onBufferBeforeChange = (value: string) => {
        setBufferBeforeTime(value)
    }
    const onBufferAferChange = (value: string) => {
        setBufferAfterTime(value)
    }
    const onShortNoticeChange = (value: string) => {
        setShortNotice(value)
    }
    const onFutureNoticeChange = (value: string) => {
        setFutureNotice(value)
    }
    return (
        <SafeAreaView style={styles.safeStyle}>
            <ScrollView style={styles.mainContainer}>
                <View style={styles.mainView}>
                    <Text style={styles.normalHeadingText}>Your review</Text>
                    <Text style={styles.greyTextNormal}>Please add schedule when you are available</Text>

                    <Text style={styles.headingText}>How long should each class be?</Text>
                    <View style={styles.classTimingMainContainer}>
                        {classTiming.map((item, index) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        let tempArray = [...classTiming]
                                        if (!tempArray[index]?.isSelected) {
                                            tempArray[index].isSelected = true
                                            setClassTiming(tempArray)
                                        }
                                        else {
                                            tempArray[index].isSelected = false
                                            setClassTiming(tempArray)
                                        }
                                    }}
                                    style={item?.isSelected ? styles.itemClassTimgingActive : styles.itemClassTimgingUnActive}>
                                    <Text style={item?.isSelected ? styles.itemTextActive : styles.itemText}>{item.time}</Text>
                                </TouchableOpacity>
                            )
                        })
                        }
                    </View>



                    <Text style={styles.headingText}>When is this availability active?</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setOpenDate(true)
                            setIsFromDateOpen(true)
                        }}
                        style={[styles.dateContainer, { marginTop: 10 }]}>
                        <Text style={[styles.dateText]}>{moment(fromdate).format('DD/MM/YYYY')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            setOpenDate(true)
                            setIsFromDateOpen(false)
                        }}
                        style={[styles.dateContainer, { marginTop: 10 }]}>
                        <Text style={[styles.dateText]}>{moment(untildate).format('DD/MM/YYYY')}</Text>
                    </TouchableOpacity>
                    <Text style={styles.headingText}>At which time are you available for classes?</Text>
                    <Text style={styles.pickerHeadingText}>Sunday</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setOpen(true)
                            setOpenFirstTimeSun(true)
                            setSelectedDay('sun')
                        }}
                        style={styles.dateContainer}>
                        {!openFirstTimeSun ?
                            <Text style={styles.availableHeading}>+ Add available time</Text>
                            :
                            <Text style={[styles.dateText]}>{moment(sundayTime).format('hh:mm A')}</Text>
                        }
                    </TouchableOpacity>
                    <Text style={styles.pickerHeadingText}>Monday</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setOpen(true)
                            setOpenFirstTimeMon(true)
                            setSelectedDay('mon')
                        }}
                        style={styles.dateContainer}>
                        {!openFirstTimeMon ?
                            <Text style={styles.availableHeading}>+ Add available time</Text>
                            :
                            <Text style={[styles.dateText]}>{moment(mondayTime).format('hh:mm A')}</Text>
                        }
                    </TouchableOpacity>
                    <Text style={styles.pickerHeadingText}>Tuesday</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setOpen(true)
                            setOpenFirstTimeTue(true)
                            setSelectedDay('tue')
                        }}
                        style={styles.dateContainer}>
                        {!openFirstTimeTue ?
                            <Text style={styles.availableHeading}>+ Add available time</Text>
                            :
                            <Text style={[styles.dateText]}>{moment(tuesdayTime).format('hh:mm A')}</Text>
                        }
                    </TouchableOpacity>
                    <Text style={styles.pickerHeadingText}>Wednesday</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setOpen(true)
                            setOpenFirstTimeWed(true)
                            setSelectedDay('wed')
                        }}
                        style={styles.dateContainer}>
                        {!openFirstTimeWed ?
                            <Text style={styles.availableHeading}>+ Add available time</Text>
                            :
                            <Text style={[styles.dateText]}>{moment(wednesdayTime).format('hh:mm A')}</Text>
                        }
                    </TouchableOpacity>
                    <Text style={styles.pickerHeadingText}>Thursday</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setOpen(true)
                            setOpenFirstTimeThu(true)
                            setSelectedDay('thr')
                        }}
                        style={styles.dateContainer}>
                        {!openFirstTimeThu ?
                            <Text style={styles.availableHeading}>+ Add available time</Text>
                            :
                            <Text style={[styles.dateText]}>{moment(thursdayTime).format('hh:mm A')}</Text>
                        }
                    </TouchableOpacity>
                    <Text style={styles.pickerHeadingText}>Friday</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setOpen(true)
                            setOpenFirstTimeFri(true)
                            setSelectedDay('fri')
                        }}
                        style={styles.dateContainer}>
                        {!openFirstTimeFri ?
                            <Text style={styles.availableHeading}>+ Add available time</Text>
                            :
                            <Text style={[styles.dateText]}>{moment(fridayTime).format('hh:mm A')}</Text>
                        }
                    </TouchableOpacity>
                    <Text style={styles.pickerHeadingText}>Saturday</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setOpen(true)
                            setOpenFirstTimeSat(true)
                            setSelectedDay('sat')
                        }}
                        style={styles.dateContainer}>
                        {!openFirstTimeSat ?
                            <Text style={styles.availableHeading}>+ Add available time</Text>
                            :
                            <Text style={[styles.dateText]}>{moment(saturdayTime).format('hh:mm A')}</Text>
                        }
                    </TouchableOpacity>

                    <Text style={styles.headingText}>Select all where the class can be booked</Text>
                    <View style={styles.classTimingMainContainer}>
                        {bookList.map((item, index) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        let tempArray = [...bookList]
                                        if (!tempArray[index]?.isSelected) {
                                            tempArray[index].isSelected = true
                                            setBookList(tempArray)
                                        }
                                        else {
                                            tempArray[index].isSelected = false
                                            setBookList(tempArray)
                                        }
                                    }}
                                    style={item?.isSelected ? styles.itemClassTimgingActive : styles.itemClassTimgingUnActive}>
                                    <Text style={item?.isSelected ? styles.itemTextActive : styles.itemText}>{item.name}</Text>
                                </TouchableOpacity>
                            )
                        })
                        }
                    </View>
                    <Text style={styles.headingText}>Buffer and time settings</Text>
                    <Text style={styles.pickerHeadingText}>Buffer time before class</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={bufferBeforeTime}
                            textStyle={{ color: colors.black, fontSize: 12 }}
                            onValueChange={onBufferBeforeChange}
                        >
                            <Picker.Item label="Country" value="key0" />
                            <Picker.Item label="United State" value="key1" />
                            <Picker.Item label="Pakistan" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.pickerHeadingText}>Buffer time after class</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={bufferAfterTime}
                            textStyle={{ color: colors.black, fontSize: 12 }}
                            onValueChange={onBufferAferChange}
                        >
                            <Picker.Item label="Country" value="key0" />
                            <Picker.Item label="United State" value="key1" />
                            <Picker.Item label="Pakistan" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.pickerHeadingText}>The shortest notice to book a class</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={shortNotice}
                            textStyle={{ color: colors.black, fontSize: 12 }}
                            onValueChange={onShortNoticeChange}
                        >
                            <Picker.Item label="Country" value="key0" />
                            <Picker.Item label="United State" value="key1" />
                            <Picker.Item label="Pakistan" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.pickerHeadingText}>How far in the future can someone book a class</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={futureNotice}
                            textStyle={{ color: colors.black, fontSize: 12 }}
                            onValueChange={onFutureNoticeChange}
                        >
                            <Picker.Item label="Country" value="key0" />
                            <Picker.Item label="United State" value="key1" />
                            <Picker.Item label="Pakistan" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                </View>
            </ScrollView>
            <DatePicker
                modal
                mode='time'
                open={open}
                date={date}
                onConfirm={(_date) => {
                    setOpen(false)
                    setDate(_date)
                    if (selectedDay === 'mon') {
                        setTimeMonday(_date)
                    }
                    else if (selectedDay === 'tue') {
                        setTimeTuesday(_date)
                    }
                    else if (selectedDay === 'wed') {
                        setTimeWednesday(_date)
                    }
                    else if (selectedDay === 'thr') {
                        setTimeThursday(_date)
                    }
                    else if (selectedDay === 'fri') {
                        setTimeFriday(_date)
                    }
                    else if (selectedDay === 'sat') {
                        setTimeSaturday(_date)
                    }
                    else if (selectedDay === 'sun') {
                        setTimeSunday(_date)
                    }
                }}
                onCancel={() => {
                    setOpen(false)
                }}
            />
            <DatePicker
                modal
                mode="date"
                open={openDate}
                date={pickerDate}
                onConfirm={(_date) => {
                    setOpenDate(false)
                    setPickerDate(_date)
                    if (isFromDateOpen) {
                        setFromDate(_date)
                    } else {
                        setUntilDate(_date)
                    }
                }}
                onCancel={() => {
                    setOpenDate(false)
                }}
            />
            <Loader loading={loading} />
        </SafeAreaView>
    );
};
export const styles = StyleSheet.create({
    safeStyle: {
        flex: 1,
    },

    mainView: {
        flex: 1,
        marginBottom: 50
    },
    mainContainer: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20
    },
    normalHeadingText: {
        fontFamily: fonts.Medium,
        fontSize: 12,
        color: colors.black,
        marginTop: 30
    },
    greyTextNormal: {
        fontFamily: fonts.Regular,
        marginTop: 10,
        fontSize: 12,
        color: colors.dgColor
    },
    headingText: {
        fontSize: 12,
        fontFamily: fonts.Bold,
        color: colors.black,
        marginTop: 20
    },
    classTimingMainContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%'
    },
    itemClassTimgingUnActive: {
        borderWidth: 1,
        borderColor: colors.dgColor,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        paddingLeft: 27,
        paddingRight: 27,
        marginRight: 10,
        marginTop: 10
    },
    itemClassTimgingActive: {
        borderWidth: 1,
        borderColor: colors.lightOrange,
        borderRadius: 10,
        backgroundColor: colors.lightOrange,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        paddingLeft: 27,
        paddingRight: 27,
        marginRight: 10,
        marginTop: 10
    },
    itemText: {
        fontFamily: fonts.Regular,
        color: colors.black,
        fontSize: 12
    },
    itemTextActive: {
        fontFamily: fonts.Bold,
        color: colors.white,
        fontSize: 12
    },
    pickerContainer: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '55%',
        backgroundColor: colors.white,
        borderRadius: 10,
        marginTop: 10,
        borderWidth: .8,
        borderColor: colors.dgColor
    },
    dropDownStyle: {
        width: 10,
        height: 6,
        resizeMode: 'contain',
        right: 18,
        position: 'absolute'
    },
    pickerHeadingText: {
        fontSize: 12,
        fontFamily: fonts.Regular,
        color: colors.black,
        marginTop: 20
    },
    dateContainer: {
        borderRadius: 10,
        width: '65%',
        height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#C8C8C8'
    },
    availableHeading: {
        fontFamily: fonts.Bold,
        fontSize: 13,
        color: '#7ABBB5'
    },
    dateText: {
        color: colors.borderColor
    }
});
export default AvailablityComponent;
