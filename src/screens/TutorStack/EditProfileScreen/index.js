import React, { useState } from "react";
import {
    View,
    Image,
    SafeAreaView,
    TouchableOpacity,
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { usernameUpdateAPI } from '../../../api/methods/auth'
import { updateProfileAPI } from '../../../api/methods/user'

import ImagePickerModel from '../../../components/ImagePickerModel'
import AvatarComponent from '../../../components/AvatarComponent'
import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'
import { showErrorMsg, showSuccessMsg } from '../../../utils/flashMessage'
import moment from "moment";

const EditProfileScreen = (props) => {
    const { navigation, route } = props

    const profileData = route.params?.profileData

    const [showRightText, setShowRightText] = useState(false);
    const [showRightLoading, setShowRightLoading] = useState(false);

    const [loading, setLoading] = useState(false);
    const [name, setName] = useState(profileData?.name || "");
    const [email, setEmail] = useState(profileData?.email || "");
    const [userName, setUserName] = useState(profileData?.user_name || "");
    const [password, passwordchange] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");
    const [secureTypePassword, setSecureTypePassword] = useState(true);
    const [secureTypePasswordConfirm, setSecureTypePasswordConfirm] = useState(true);
    const [profileImage, setProfileImage] = useState(profileData?.avatar || "");
    const [showPickerModel, setShowPickerModel] = useState(false);

    const onUpdatePress = async () => {
        if (profileData?.name != name && name === "") {
            showErrorMsg("Name is required");
        } else if (profileData?.email != email) {
            if (email === "") {
                showErrorMsg("Email is required");
            } else if ((/^\w+([\.+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/).test(email.trim()) == false) {
                showErrorMsg('Email format is invalid')
            }
        } else if (showRightText) {
            showErrorMsg("Username is not available");
        } else if (password !== "") {
            if (password.length < 6) {
                showErrorMsg("Password must be atleast 6 character");
            } else if (passwordConfirm != password) {
                showErrorMsg("Confirm password not match with password");
            }
        } else {
            updateProfile()
        }
    };

    const updateProfile = async () => {
        try {
            setLoading(true)
            console.log('updateAPI-check-hash')
            const formData = new FormData()
            formData.append('name', name)
            formData.append('user_name', userName)
            formData.append('old_password', password)
            formData.append('password', passwordConfirm)
            if (profileImage?.uri) formData.append('avatar', profileImage)

            const response = await updateProfileAPI(formData, {
                headers: {
                    'Content-Type': 'multipart/form-data;'
                }
            })
            console.log('updateAPI-check-response')
            if (response.status === 200) {
                console.log('updateAPI-response', response.data)
                showSuccessMsg(response.data?.message)
            }
        } catch (error) {
            console.log('updateAPI-error-->', error)
            showErrorMsg(error.message)
        } finally {
            setLoading(false)
        }
    }

    const handleChoosePhoto = (response) => {
        if (response.didCancel) {
            //// console.log('User cancelled image picker');
        } else if (response.error) {
            //// console.log('ImagePickerModel Error: ', response.error);
        } else {
            const source = { uri: response.uri };
            setProfileImage({ uri: response.uri, name: moment().format('x') + ".jpg", type: 'image/jpeg' })
        }
    }

    const usernameApi = async (userNameParam) => {
        try {
            setShowRightText(false)
            setShowRightLoading(true)
            console.log('usernameApi-check-hash')
            const response = await usernameUpdateAPI({
                "user_name": userNameParam,
            })
            console.log('usernameApi-check-response')
            setLoading(false)
            if (response.status === 200) {
                console.log('usernameApi-response', response.data)
                setShowRightLoading(false)
            }
            else {
                showErrorMsg('Something Went Wrong')
            }
        } catch (error) {
            setLoading(false)
            setShowRightText(true)
            setShowRightLoading(false)
            console.log('usernameApi-error-->', error?.response?.data?.message)
        }
    }

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                hearderText={'EDIT PROFILE'}
                leftIcon={icons.backIcon}
                onLeftAction={() => { navigation.goBack() }}
                leftButtonIconStyle={{ width: 18, height: 18 }}
            />
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.innerContainer}>
                    <View style={styles.profileOuterContainer}>
                        <AvatarComponent
                            source={profileImage}
                            defaultSource={icons.userProfileIcon}
                            style={styles.profileContainer}
                            size={'small'}
                            imageStyle={{ resizeMode: 'cover' }}
                        />
                        <TouchableOpacity
                            style={{ position: 'absolute', right: 15, bottom: 10 }}
                            onPress={() => { setShowPickerModel(true) }}>
                            <Image
                                style={styles.profileCamera}
                                source={icons.uploadIconImage}
                            />
                        </TouchableOpacity>
                    </View>
                    <InputField
                        inputHeading={'Name'}
                        value={name}
                        onChangeText={(text) => {
                            setName(text)
                        }}
                    />
                    <InputField
                        editable={false}
                        inputHeading={'Email ID'}
                        value={email}
                        onChangeText={(text) => {
                            setEmail(text)
                        }}
                    />
                    <InputField
                        showRightText={showRightText}
                        showRightLoading={showRightLoading}
                        // showRightText={true}
                        inputHeading={'Username'}
                        value={userName}
                        onChangeText={(text) => {
                            if (text.length > 2) {
                                usernameApi(text)
                            }
                            setUserName(text)
                        }}
                    />
                    <InputField
                        showRightIcon={true}
                        secureType={secureTypePassword}
                        inputHeading={'Old Password'}
                        value={password}
                        onChangeText={(text) => {
                            passwordchange(text)
                        }}
                        onRightPress={() => {
                            setSecureTypePassword(!secureTypePassword)
                        }}
                    />
                    <InputField
                        showRightIcon={true}
                        secureType={secureTypePasswordConfirm}
                        inputHeading={'Change Password'}
                        value={passwordConfirm}
                        onChangeText={(text) => {
                            setPasswordConfirm(text)
                        }}
                        onRightPress={() => {
                            setSecureTypePasswordConfirm(!secureTypePasswordConfirm)
                        }}
                    />
                    <Button
                        text={'Update'}
                        textStyle={styles.btnText}
                        backgroundColorStyle={styles.btnContainer}
                        clickAction={onUpdatePress}
                    />
                </View>
                <ImagePickerModel
                    showPickerModel={showPickerModel}
                    onHideModel={() => {
                        setShowPickerModel(false)
                    }}
                    handleChoosePhoto={handleChoosePhoto}
                />
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default EditProfileScreen;
