import { StyleSheet } from 'react-native';
import colors from '../../../utils/colors'
export const styles = StyleSheet.create({
  safeStyle: {
    width:'100%',
    height:'100%',
    
  },
  innerContainer:{ 
    width: '100%', 
    alignSelf: 'center', 
    marginTop: 15 ,
    paddingLeft: 22,
    paddingRight: 22,
    marginBottom:20
  },
  btnText: {
    fontSize: 14,
    lineHeight: 22,
    color:colors.white
  },
  btnContainer: {
    borderRadius: 3,
    marginTop: 20,
    backgroundColor:colors.green
  },
  profileOuterContainer: {
    // marginTop: 45,
    alignSelf: 'center',
    width: 140,
    height: 140,
    borderRadius:70,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: colors.yellow,
    shadowOffset:{
    width: 0,
    height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,
    elevation: 8,
  },
  profileContainer: {
    width: 140,
    height: 140,
    borderRadius: 70,
    resizeMode: 'cover',
    backgroundColor: colors.lightGrey,
  },
  profileCamera: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
});
