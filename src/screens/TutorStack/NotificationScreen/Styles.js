import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
  },
  mainView: {
    width: '100%',
  },
  itemMainContainer: {
    marginTop: 22
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  textContainer: {
    flex: 1,
    // justifyContent:'center',
    paddingLeft: 20,
    paddingRight: 20,
    marginTop:10
  },
  nameText: {
    fontFamily: fonts.SemiBold,
    fontSize: 12,
    color: colors.black,
    width: '97%',
  },
  detailDate: {
    marginTop: 10,
    color: colors.lightBlack,
    fontSize: 12,
    fontFamily: fonts.Medium,
    // width:'50%'
  },
  barView: {
    width: '95%',
    backgroundColor: colors.dgColor,
    height: 1,
    alignSelf: 'flex-end',
    marginTop: 20
  },
  counterContainer: {
    borderWidth: 1,
    borderColor: colors.dgColor,
    padding: 6,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 5,
    marginLeft: 30,
    marginRight: 30
  },
  headerStyle:{
    paddingLeft:20
  },
  headerText:{
    fontSize:12,
    fontFamily:fonts.Bold,
    color:colors.black
  },
  loadText:{
    color:colors.lightGreen,
    fontFamily:fonts.Bold,
    fontSize:12,
    textDecorationLine:'underline'
  },
  loadMoreContainer:{
    alignItems:'center',
    justifyContent:'center',
    marginTop:30,
    marginBottom:100
  },
  classStatusStyle:{
    fontSize:12,
    marginTop:10,
    fontFamily:fonts.SemiBold,
    color:colors.lightOrange
  }
});
