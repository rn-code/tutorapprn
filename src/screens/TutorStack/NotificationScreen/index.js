import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { useSelector } from 'react-redux'

import Header from "../../../components/Header";
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')



const NotificationScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [pageCounter, setPageCounter] = useState(1);
    const [notificationList, setNotificationList] = useState([
        {
            date: '19/11/2021',
            classStatus: 'Open classroom',
            heading: 'Are you ready? Your next class is going to held in just 20 minutes.',
        },
        {
            date: '11/10/2020',
            classStatus: 'Class finished',
            heading: 'You have scheduled a class with Ahmad on Monday 27 July, 2021 at 18:00',
        },
        {
            date: '11/08/2020',
            classStatus: 'View Transactions',
            heading: 'You have been charged with $24.6 for the class you booked on Sunday 26 July, 2021 at 15:00',
        },
        {
            date: '11/05/2020',
            classStatus: 'View classes',
            heading: 'You have reschuduled the class ABC to 30 July, 2021 at 16:00',
        }
    ])

    useEffect(() => {


    }, [])

    const renderChatItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer]}>
                { index===0 &&
                    <View style={styles.headerStyle}>
                        <Text style={styles.headerText}>{'Today'}</Text>
                        <View style={[styles.barView, { marginTop: 10 }]} />
                    </View>
                }
                { index===1 &&
                    <View style={styles.headerStyle}>
                        <Text style={styles.headerText}>{'Earlier'}</Text>
                        <View style={[styles.barView, { marginTop: 10 }]} />
                    </View>
                }
                <View style={styles.textContainer}>
                    <Text style={styles.nameText}>{item.heading}</Text>
                    <Text style={[styles.classStatusStyle, {
                        color: item.classStatus === 'Class finished' ? colors.dgColor
                            : colors.lightOrange
                    }]}>{item.classStatus}</Text>
                </View>
                {index + 1 === notificationList.length &&
                    <Pressable
                        style={styles.loadMoreContainer}>
                        <Text style={styles.loadText}>Load More</Text>
                    </Pressable>
                }
                { index===1 &&
                    <View style={styles.barView} />
                }
                { index===2 &&
                    <View style={styles.barView} />
                }
                {/* {moment(item.date, "DD/MM/YYYY").isAfter(moment(new Date, "DD/MM/YYYY")) != moment(notificationList[index > 0 ? index - 1 : 0].date, "DD/MM/YYYY").isAfter(moment(new Date, "DD/MM/YYYY")) &&
                    <View style={styles.barView} />
                } */}
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No chat exist'}</Text>
            </View>
        )
    };

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Notifications'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    // navigation.toggleDrawer();
                }}
            />
            <View style={styles.mainView}>
                <FlatList
                    data={notificationList}
                    showsVerticalScrollIndicator={false}
                    // contentContainerStyle={{ }}
                    renderItem={renderChatItem}
                    ListEmptyComponent={renderEmptyContainer}
                    showsVerticalScrollIndicator={false}
                />
            </View>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default NotificationScreen;
