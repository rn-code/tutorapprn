import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
  },
  mainView: {
    width: '100%',
    // marginBottom: 170,
  },
  itemMainContainer: {
    marginTop: 22,
    width:'100%'
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  textContainer: {
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
    paddingLeft: 20,
    paddingRight: 20,
  },
  greyText: {
    marginTop: 10,
    color: colors.chatMessage,
    fontSize: 12,
    fontFamily: fonts.Medium,
  },
  barView: {
    width: '95%',
    backgroundColor: colors.dgColor,
    height: 1,
    alignSelf: 'flex-end',
    marginTop: 20
  },
  secondRowContainer:{
    paddingLeft:20,
    marginTop:5
  },
  headingText:{
    color:colors.black,
    fontSize:12,
    fontFamily:fonts.Medium,
    paddingLeft:20,
    marginTop:25
  },
  descriptionText:{
    color:colors.dgColor,
    fontSize:12,
    fontFamily:fonts.Medium,
    paddingLeft:20,
    marginTop:10
  },
  classesListContainer:{
    width:'100%',
    paddingLeft:20,
    paddingRight:20,
    flexWrap:'wrap',
    flexDirection:'row',
    justifyContent:'space-between'
  },
  classListItemContainer:{
    width:'47%',
    borderRadius:20,
    padding:20,
    borderWidth:1,
    borderColor:colors.dgColor,
    marginTop:10,
    alignItems:'center',
    justifyContent:'center'
  },
  headingClassItem:{
    fontSize:24,
    fontFamily:fonts.Bold,
    color:colors.black
  },
  descriptionClassItem:{
    fontSize:12,
    fontFamily:fonts.Bold,
    color:colors.dgColor
  },
  monthlySumaryHeadingContainer:{
    flexDirection:'row',
    width:'100%',
    justifyContent:'space-between',
    paddingRight:20,
    paddingLeft:20
  },
  headingSummaryText:{
    color:colors.black,
    fontFamily:fonts.SemiBold,
    fontSize:12
  }
});
