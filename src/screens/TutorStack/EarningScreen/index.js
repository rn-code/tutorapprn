import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
    ScrollView,
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { useSelector } from 'react-redux'

import Header from "../../../components/Header";
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')



const EarningScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [classCountList, setClassCountList] = useState([
        { classCount: 4, name: 'Total students' },
        { classCount: 1, name: 'Total hours' },
        { classCount: '$1,287', name: 'Online earning' },
    ]);
    const [recentTransactionList, setRecentTransactionList] = useState([
        {
            date: '11/30/2020',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            date: '11/30/2020',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            date: '11/30/2020',
            price: '$1,287',
            heading: 'Learn how to read and write',
        }
    ])
    const [monthlyList, setMonthlyList] = useState([
        {
            date: '11/30/2020',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            date: '11/30/2020',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            date: '11/30/2020',
            price: '$1,287',
            heading: 'Learn how to read and write',
        }
    ])

    useEffect(() => {


    }, [])
    const renderRecentTransactionItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer]}>
                <View style={styles.textContainer}>
                    <Text style={styles.greyText}>{'Balance withdraw'}</Text>
                    <Text style={styles.greyText}>{'$300'}</Text>
                    <Text style={styles.greyText}>{'11/30/2020'}</Text>
                </View>
                <View style={styles.secondRowContainer}>
                    <Text style={styles.greyText}>{'Online payment'}</Text>
                </View>
                {index + 1 != recentTransactionList.length && <View style={styles.barView} />}
            </Pressable>
        );
    };
    const renderMonthlySummaryItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer, { marginTop: 15 }]}>
                {index === 0 &&
                    <View>
                        <View style={[styles.monthlySumaryHeadingContainer]}>
                            <Text style={[styles.headingSummaryText, { width: '35%'}]}>Month</Text>
                            <Text style={[styles.headingSummaryText, { width: '30%' }]}>Earnings</Text>
                            <Text style={[styles.headingSummaryText, { width: '30%' }]}>Learners</Text>
                        </View>
                        <View style={styles.barView} />
                    </View>
                }
                <View style={styles.textContainer}>
                    <Text style={[styles.greyText, { width: '35%' }]}>{'November'}</Text>
                    <Text style={[styles.greyText, { width: '30%'}]}>{'$500'}</Text>
                    <Text style={[styles.greyText, { width: '30%'}]}>{'20'}</Text>
                </View>
                {index + 1 != recentTransactionList.length && <View style={styles.barView} />}
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No chat exist'}</Text>
            </View>
        )
    };

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Earnings'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    // navigation.toggleDrawer();
                }}
            />
            <ScrollView>

                <Text style={styles.headingText}>My earnings</Text>
                <Text style={styles.descriptionText}>Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. </Text>
                <Text style={styles.headingText}>Earnings summary</Text>
                <View style={styles.classesListContainer}>
                    {
                        classCountList.map((item, index) => {
                            return (
                                <View style={[styles.classListItemContainer, { width: classCountList.length === index + 1 ? '100%' : '47%' }]}>
                                    <Text style={styles.headingClassItem}>{item.classCount}</Text>
                                    <Text style={styles.descriptionClassItem}>{item.name}</Text>
                                </View>
                            )
                        })
                    }
                </View>
                <Text style={styles.headingText}>Recent transactions</Text>

                <View style={styles.mainView}>
                    <FlatList
                        data={recentTransactionList}
                        showsVerticalScrollIndicator={false}
                        // contentContainerStyle={{ }}
                        renderItem={renderRecentTransactionItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                </View>

                <Text style={[styles.headingText, { marginTop: 60 }]}>Monthly summary</Text>

                <View style={[styles.mainView, { marginBottom: 80 }]}>
                    <FlatList
                        data={monthlyList}
                        showsVerticalScrollIndicator={false}
                        // contentContainerStyle={{ }}
                        renderItem={renderMonthlySummaryItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                </View>

                <Loader loading={loading} />
            </ScrollView>
        </SafeAreaView>
    );
};

export default EarningScreen;
