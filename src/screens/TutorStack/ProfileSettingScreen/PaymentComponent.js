import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    Dimensions,
    StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'
import CheckBox from '../../../components/CustomCheckBox'
import { Picker } from 'native-base';

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts';
import { color } from "react-native-reanimated";
const { width, height } = Dimensions.get('window')

const PaymentComponent = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [cardName, setCardName] = useState("");
    const [cardNumber, setCardNumber] = useState("");
    const [branchAddress, setBranchAddress] = useState("");
    const [branchName, setBranchName] = useState("");
    const [swiftCode, setSwiftCode] = useState("");
    const [bankName, setBankName] = useState("");
    const [accountType, setAccountType] = useState("");

    const updatePasswordBtn = async () => {

    };
    const onAccountChange = (value: string) => {
        setAccountType(value)
      }
    return (
        <View style={styles.safeStyle}>
            <InputField
                autoCapitalizes={"none"}
                inputHeading={'SWIFT code'}
                placeholder={'SWIFT code'}
                value={swiftCode}
                onChangeText={(text) => {
                    setSwiftCode(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
            <InputField
                autoCapitalizes={"none"}
                inputHeading={'Bank name'}
                placeholder={'Bank name'}
                value={bankName}
                onChangeText={(text) => {
                    setBankName(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
            <Text style={styles.bankInfoText}>Account holder bank information</Text>
            <InputField
                autoCapitalizes={"none"}
                inputHeading={'IBAN number'}
                placeholder={'IBAN number'}
                keyboardType={'number-pad'}
                maxLength={16}
                value={cardNumber}
                onChangeText={(text) => {
                    setCardNumber(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            // mainStyle={{ marginTop: 10 }}

            />

            <InputField
                autoCapitalizes={"none"}
                inputHeading={'Name on account'}
                placeholder={'Name on account'}
                value={cardName}
                onChangeText={(text) => {
                    setCardName(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
            <InputField
                autoCapitalizes={"none"}
                inputHeading={'Branch name'}
                placeholder={'Branch name'}
                value={branchName}
                onChangeText={(text) => {
                    setBranchName(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
            <InputField
                autoCapitalizes={"none"}
                inputHeading={'Branch address'}
                placeholder={'Branch address'}
                value={branchAddress}
                onChangeText={(text) => {
                    setBranchAddress(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
            <Text style={styles.headerText}>Bank account type</Text>
            <View style={styles.pickerContainer}>
                <Picker
                    mode="dropdown"
                    style={{ width: width, }}
                    selectedValue={accountType}
                    textStyle={{ color: colors.dgColor }}
                    onValueChange={onAccountChange}
                >
                    <Picker.Item label="Select" value="key0" />
                    <Picker.Item label="Personal" value="key1" />
                    <Picker.Item label="Other" value="key2" />
                </Picker>
                {Platform.OS === 'ios' &&
                    <Image
                        style={styles.dropDownStyle}
                        source={icons.dropdownIcon}
                    />
                }
            </View>
            <InputField
                autoCapitalizes={"none"}
                inputHeading={'Name on account'}
                placeholder={'Name on account'}
                value={cardName}
                onChangeText={(text) => {
                    setCardName(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
            <View style={styles.buttonContainer}>
                <Button
                    text={'Save Profile'}
                    // textStyle={{ }}
                    backgroundColorStyle={styles.saveBtn}
                    clickAction={updatePasswordBtn.bind(this)}
                />
            </View>
            <Loader loading={loading} />
        </View>
    );
};
const styles = StyleSheet.create({
    safeStyle: {
    },
    fieldContainer: {
        marginBottom: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop: 30
    },
    saveBtn: {
        backgroundColor: colors.lightGreen,
        height: 44,
        width: 140,
        borderRadius: 8,
        marginTop: 10,
        shadowOpacity: 0
    },
    inputText: {
        fontSize: 12,
        fontFamily: fonts.Medium
    },
    inputBackground: {
        backgroundColor: colors.white,
        borderWidth: .8,
        borderRadius: 8,
        borderColor: colors.dgColor,
        paddingLeft: 20
    },
    bankInfoText: {
        color: colors.black,
        fontSize: 12,
        marginTop: 25,
        fontFamily: fonts.Medium
    },
    headerText: {
        fontFamily: fonts.Medium,
        fontSize: 12,
        marginTop: 10
      },
      pickerContainer: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '99%',
        alignSelf: 'center',
        backgroundColor: colors.white,
        borderRadius: 10,
        marginTop: 10,
        borderWidth:.8,
        borderColor:colors.dgColor
      },
      dropDownStyle: {
        width: 10,
        height: 6,
        resizeMode: 'contain',
        right: 18,
        position: 'absolute'
      },
});

export default PaymentComponent;
