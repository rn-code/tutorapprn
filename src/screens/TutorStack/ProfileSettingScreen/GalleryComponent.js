import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import Loader from '../../../components/Loader'
import ImagePickerModel from './../../../components/ImagePickerModel'

import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'


const GalleryComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [showPickerModel, setShowPickerModel] = useState(false);
  const [loading, setLoading] = useState(false);
  const [galleryList, setGalleryList] = useState([{}]);
  const [imagesList, setImagesList] = useState([{}, {}, {}, {}, {}, {}, {}, {}, {}, {}]);

  const handleChoosePhoto = (response) => {
    if (response.didCancel) {
      //// console.log('User cancelled image picker');
    } else if (response.error) {
      //// console.log('ImagePickerModel Error: ', response.error);
    } else {
      const source = { uri: response.uri };
      let tempArray = [...imagesList]
      tempArray[selectedIndex].selectedImage = response?.assets?.[0]?.uri
      setImagesList(tempArray)
    }
  }
  const dotImage = (resolution,index, width, height) => {
    return (
      <View style={[styles.itemMainContainer, {
        width: width,
        height: height,
        borderWidth: imagesList?.selectedImage != undefined ? 0 : 1
      }]}>
        <Image
          source={{ uri: imagesList[index]?.selectedImage != undefined ? imagesList?.selectedImage : null }}
          style={styles.galleryImage}
        />
        <TouchableOpacity
          onPress={() => {
            setShowPickerModel(true)
            setSelectedIndex(index)
          }}
          style={styles.plusContiner}>
          <Image
            source={icons.plusSignIcon}
            style={styles.plusSignIcon}
          />
        </TouchableOpacity>
        <Text style={styles.resolutionText}>{resolution}</Text>
      </View>
    )
  }
  const renderGalleryItem = ({ item, index }) => {
    return (
      <View style={styles.mainViewItem}>

        <View style={styles.firstBlockMainContainer}>
          <View style={{ width: '64%', justifyContent: 'space-between' }}>
            <View style={styles.firstLeftBlockContainer}>
              {dotImage('252 x 402',index, '48%', 170)}
              <View style={styles.firstLeftInnerRightContainer}>
                {dotImage('253 x 191',index + 1, '100%', 80)}
                {dotImage('253 x 191',index + 2, '100%', 80)}
              </View>
            </View>
            {dotImage('525 x 310',index + 3, '100%', 130)}
          </View>
          <View style={{ width: '33%', justifyContent: 'space-between' }}>
            {dotImage('252 x 315',index + 4, '100%', 129)}
            {dotImage('253 x 191',index + 5, '100%', 80)}
            {dotImage('253 x 191',index + 6, '100%', 80)}
          </View>
        </View>

        <View style={styles.secondBlockMainContainer}>
          <View style={styles.secondBlockLeftContainer}>
            {dotImage('363 x 191',index + 7, '100%', 82)}
            {dotImage('363 x 191',index + 8, '100%', 82)}
          </View>
          <View style={{ width: '53%' }}>
            {dotImage('414 x 402',index + 10, '100%', 175)}
          </View>
        </View>


      </View >
    );
  };
  const renderEmptyContainer = () => {
    return (
      <View style={[styles.emptyContainer]}>
        <Text style={styles.emptyText}>{'No item exist'}</Text>
      </View>
    )
  };
  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 10, marginBottom: 10 }}
        contentContainerStyle={{ flexGrow: 1 }}>
        <Text style={styles.headerText}>Gallery</Text>
        <View>
          <FlatList
            data={galleryList}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flex: 1 }}
            renderItem={renderGalleryItem}
            ListEmptyComponent={renderEmptyContainer}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
      <ImagePickerModel
        showPickerModel={showPickerModel}
        onHideModel={() => {
          setShowPickerModel(false)
        }}
        handleChoosePhoto={handleChoosePhoto}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
    backgroundColor: colors.white
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    lineHeight: 36,
  },
  emptyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    width: '31%',
    height: 132,
    // marginTop: 10,
    // marginLeft: 4,
    // marginRight: 4,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  galleryImage: {
    width: '100%',
    height: 132,
    resizeMode: 'cover',
    position: 'absolute',
    borderRadius: 5
  },
  plusSignIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain'
  },
  plusContiner: {
  },


  mainViewItem: {
    width: '100%'
  },
  firstBlockMainContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  firstLeftBlockContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 8
  },
  firstLeftInnerRightContainer: {
    width: '48%',
    justifyContent: 'space-between'
  },
  secondBlockMainContainer: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection:'row',
    marginTop: 10
  },
  secondBlockLeftContainer: {
    width: '44%',
    justifyContent:'space-between'
  },
  resolutionText:{
    fontFamily:fonts.Regular,
    fontSize:12,
    color:colors.dgColor,
    marginTop:8
  }
});

export default GalleryComponent;
