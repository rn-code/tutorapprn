import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    TouchableOpacity
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import ImagePickerModel from './../../../components/ImagePickerModel'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";

import AboutComponent from './AboutComponent'
import HighlightComponent from './HighlightComponent'
import GalleryComponent from './GalleryComponent'
import PricingComponent from './PricingComponent'
import PersonalInfoComponent from './PersonalInfoComponent'
import PaymentComponent from './PaymentComponent'
import UpdateSettingComponent from './UpdateSettingComponent'
import ChangePasswordComponent from './ChangePasswordComponent'
import CheckIdVerification from './CheckIdVerification'

const ProfileSettingScreen = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [activeLink, setActiveLink] = useState('')
    const [profileImage, setProfileImage] = useState('');
    const [showPickerModel, setShowPickerModel] = useState(false);

    const changeProfile = () => {
        setShowPickerModel(true)
    };
    const linkClick = (value) => {
        setActiveLink(value)
    }
    const handleChoosePhoto = (response) => {
        if (response.didCancel) {
            //// console.log('User cancelled image picker');
        } else if (response.error) {
            //// console.log('ImagePickerModel Error: ', response.error);
        } else {
            const source = { uri: response.uri };
            console.log(response?.assets?.[0]?.uri)
            setProfileImage(response?.assets?.[0]?.uri)
        }
    }
    return (
        <View style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Profile & Settings'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    // navigation.toggleDrawer();
                }}
            />
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={styles.fieldContainer}>
                <View style={styles.profileContainer}>
                    <Image
                        source={profileImage === '' ? icons.dumyIcon : { uri: profileImage }}
                        style={styles.profileIcon}
                    />
                    <Button
                        text={'Change Profile Picture'}
                        // textStyle={{ }}
                        backgroundColorStyle={styles.profileBtn}
                        clickAction={changeProfile.bind(this)}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => { linkClick('about') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>About</Text>
                    <Image
                        source={activeLink === 'about' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'about' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'about' &&
                    <AboutComponent />
                }
                <TouchableOpacity
                    onPress={() => { linkClick('kids') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>Highlights</Text>
                    <Image
                        source={activeLink === 'kids' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'kids' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'kids' &&
                    <HighlightComponent />
                }


                <TouchableOpacity
                    onPress={() => { linkClick('gallery') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>Gallery</Text>
                    <Image
                        source={activeLink === 'gallery' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'gallery' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'gallery' &&
                    <GalleryComponent />
                }

                <TouchableOpacity
                    onPress={() => { linkClick('pricing') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>Pricing</Text>
                    <Image
                        source={activeLink === 'pricing' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'pricing' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'pricing' &&
                    <PricingComponent />
                }




                <TouchableOpacity
                    onPress={() => { linkClick('personal') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>Contact information</Text>
                    <Image
                        source={activeLink === 'personal' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'personal' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'personal' &&
                    <PersonalInfoComponent />
                }




                <TouchableOpacity
                    onPress={() => { linkClick('idVerification') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>{'Background check & ID verification'}</Text>
                    <Image
                        source={activeLink === 'idVerification' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'idVerification' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'idVerification' &&
                    <CheckIdVerification />
                }





                <TouchableOpacity
                    onPress={() => { linkClick('changePassword') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>Change password</Text>
                    <Image
                        source={activeLink === 'changePassword' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'changePassword' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'changePassword' &&
                    <ChangePasswordComponent />
                }
                <TouchableOpacity
                    onPress={() => { linkClick('payment') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>Bank information</Text>
                    <Image
                        source={activeLink === 'payment' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'payment' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'payment' &&
                    <PaymentComponent />
                }
                <TouchableOpacity
                    onPress={() => { linkClick('update') }}
                    style={styles.linkContainer}>
                    <Text style={styles.linkText}>Update Settings</Text>
                    <Image
                        source={activeLink === 'update' ?
                            icons.dropUpIcon :
                            icons.dropDownProfileIcon
                        }
                        style={
                            activeLink === 'update' ?
                                styles.linkUpIcon :
                                styles.linkIcon
                        }
                    />
                </TouchableOpacity>
                {activeLink === 'update' &&
                    <UpdateSettingComponent />
                }
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
            <ImagePickerModel
                showPickerModel={showPickerModel}
                onHideModel={() => {
                    setShowPickerModel(false)
                }}
                handleChoosePhoto={handleChoosePhoto}
            />
        </View>
    );
};

export default ProfileSettingScreen;
