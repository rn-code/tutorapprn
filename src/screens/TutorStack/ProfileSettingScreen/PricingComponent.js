import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet,
  Dimensions
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { Picker } from 'native-base';

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import InputField from '../../../components/InputField'
import CheckBox from '../../../components/CustomCheckBox'

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
import { TouchableOpacity } from "react-native-gesture-handler";
const { width, height } = Dimensions.get('window')


const PricingComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [currentlyEnroll, setCurrentlyEnroll] = useState(false);
  const [hourlyRate, setHourlyRate] = useState('');
  const [thirtyMinute, setThirtyMinute] = useState('');
  const [weeklyPackage, setWeeklyPackage] = useState('');
  const [fortnightlyPackage, setFortnightlyPackage] = useState('');
  const [monthlyRate, setMonthlyRate] = useState('');
  const [threeGroupRate, setThreeGroupRate] = useState('');
  const [fiveGroupRate, setFiveGroupRate] = useState('');
  const [sevenGroupRate, setSevenGroupRate] = useState('');




  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginBottom: 10 }}
        contentContainerStyle={{ flexGrow: 1 }}>

        <Text style={styles.headerText}>Hourly rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Hourly rate'}
          // keyboardType={'number-pad'}
          value={hourlyRate}
          onChangeText={(text) => {
            setHourlyRate(text)
          }}
        />
        <Text style={styles.headerText}>30 minute rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'30 minute rate'}
          // keyboardType={'number-pad'}
          value={thirtyMinute}
          onChangeText={(text) => {
            setThirtyMinute(text)
          }}
        />
        <Text style={styles.headerText}>Weekly package rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Weekly package rate'}
          // keyboardType={'number-pad'}
          value={weeklyPackage}
          onChangeText={(text) => {
            setWeeklyPackage(text)
          }}
        />
        <Text style={styles.headerText}>Fortnightly package rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Fortnightly package rate'}
          // keyboardType={'number-pad'}
          value={fortnightlyPackage}
          onChangeText={(text) => {
            setFortnightlyPackage(text)
          }}
        />
        <Text style={styles.headerText}>Monthly package rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Monthly package rate'}
          // keyboardType={'number-pad'}
          value={monthlyRate}
          onChangeText={(text) => {
            setMonthlyRate(text)
          }}
        />
        <Text style={styles.headerText}>3-student group rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'3-student group rate'}
          // keyboardType={'number-pad'}
          value={threeGroupRate}
          onChangeText={(text) => {
            setThreeGroupRate(text)
          }}
        />
        <Text style={styles.headerText}>5-student group rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'5-student group rate'}
          // keyboardType={'number-pad'}
          value={fiveGroupRate}
          onChangeText={(text) => {
            setFiveGroupRate(text)
          }}
        />
        <Text style={[styles.headerText]}>7-student group rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'7-student group rate'}
          // keyboardType={'number-pad'}
          value={sevenGroupRate}
          onChangeText={(text) => {
            setSevenGroupRate(text)
          }}
        />
        <View style={[styles.termsContainer, { marginTop: 30 }]}>
          <CheckBox
            onChange={() => { setCurrentlyEnroll(!currentlyEnroll) }}
            onChangeTerm={() => {
            }}
            customTxt={'I offer a demo lesson too for my students.'}
            isChecked={currentlyEnroll}
            tintColor={colors.black}
            checkstyle={{
                borderWidth:.8,borderColor:colors.dgColor
            }}
          />
        </View>
        <Button
          text={'Save Profile'}
          // textStyle={{ }}
          backgroundColorStyle={{
            width: 140,
            shadowOpacity: 0,
            backgroundColor:colors.lightGreen
          }}
        />
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    backgroundColor: colors.white
  },
  headerText: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    marginTop: 30
  },
  itemName: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black,
    marginLeft: 20,
    width: '50%',
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    marginTop: 10,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  pickerContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '99%',
    alignSelf: 'center',
    backgroundColor: colors.white,
    borderRadius: 10,
    marginTop: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2
  },
  dropDownStyle: {
    width: 10,
    height: 6,
    resizeMode: 'contain',
    right: 18,
    position: 'absolute'
  },
  logoIconStyle: {
    width: 38,
    height: 38,
    resizeMode: 'contain'
  },
  unSelectedTick: {
    width: 32,
    height: 32,
    resizeMode: 'contain'
  },
  termsContainer: {
    flexDirection: 'row',
    marginTop: 30,
    alignItems: 'center',
  },
  inputSearchContainer: {
    backgroundColor: colors.white,
    width: '99%',
    marginTop: 20,
    borderWidth:.8,
    borderColor:colors.dgColor,
    alignSelf: 'center'
  },
});

export default PricingComponent;
