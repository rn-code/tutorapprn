import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    FlatList,
    StyleSheet,
    Dimensions
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { Picker } from 'native-base';

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import InputField from '../../../components/InputField'
import CheckBox from '../../../components/CustomCheckBox'

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
const { width, height } = Dimensions.get('window')


const AboutComponent = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [introduction, setIntroduction] = useState("");
    const [longIntro, setLongIntro] = useState("");
    const [name, setName] = useState("");
    const [lastName, setLastName] = useState("");

    const [currentlyEnroll, setCurrentlyEnroll] = useState(false);
    const [currentlyWorked, setCurrentlyWorked] = useState(false);

    const [instituteName, setInstituteName] = useState("");
    const [degreeTitle, setDegreeTitle] = useState("");
    const [teachingPlace, setTeachingPlace] = useState("");
    const [shortDescription, setShortDescription] = useState("");


    const [experienceList, setExperienceList] = useState([{}]);

    const [selectedCountryField, setSelectedCountryField] = useState('Any')
    const [selectedCityField, setSelectedCityField] = useState('Any')
    const [selectedCompletionField, setSelectedCompletionField] = useState('Any')
    const [selectedFromYearField, setSelectedFromYearField] = useState('Any')
    const [selectedToYearField, setSelectedToYearField] = useState('Any')

    const onCountryChange = (value: string) => {
        setSelectedCountryField(value)
    }
    const onCityChange = (value: string) => {
        setSelectedCityField(value)
    }
    const onCompletionChange = (value: string) => {
        setSelectedCompletionField(value)
    }
    const onFromYearChange = (value: string) => {
        setSelectedFromYearField(value)
    }
    const onToYearChange = (value: string) => {
        setSelectedToYearField(value)
    }
    const renderExperienceItem = ({ item, index }) => {
        return (
            <View style={[styles.itemMainContainer]}>
                <View style={styles.expeienceHeadingContainer}>
                    <View style={styles.circleStyle} />
                    <Text style={styles.insituteName}>ABC University, USA.</Text>
                </View>
                <Text style={[styles.yearText, { marginTop: 4, marginLeft: 18 }]}>2016 - 2018</Text>
                <Text style={[styles.degreeName, { marginLeft: 18 }]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisi, pellentesque eget venenatis, morbi venenatis sed posuere aenean scelerisque. Nisl pellentesque sit arcu lobortis nisl consequat sagittis.</Text>
            </View>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No item exist'}</Text>
            </View>
        )
    };

    return (
        <View style={styles.safeStyle}>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginBottom: 10 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <InputField
                    autoCapitalizes={"none"}
                    inputHeading={'First name'}
                    placeholder={'First name'}
                    value={name}
                    onChangeText={(text) => {
                        setName(text)
                    }}
                    customInputStyle={styles.inputText}
                    customStyle={styles.inputBackground}
                />
                <InputField
                    autoCapitalizes={"none"}
                    inputHeading={'Last name'}
                    placeholder={'Last name'}
                    value={lastName}
                    onChangeText={(text) => {
                        setLastName(text)
                    }}
                    customInputStyle={styles.inputText}
                    customStyle={styles.inputBackground}
                    mainStyle={{ marginTop: 10 }}
                />
                <InputField
                    customStyle={styles.inputSearchContainer}
                    autoCapitalizes={"none"}
                    inputHeading={'Short introduction'}
                    placeholder={'Short introduction'}
                    // keyboardType={'number-pad'}
                    value={introduction}
                    onChangeText={(text) => {
                        setIntroduction(text)
                    }}
                    customInputStyle={styles.inputText}
                    customStyle={styles.inputBackground}
                />
                <InputField
                    customStyle={[
                        styles.inputSearchContainer, {
                            height: 150,
                            paddingTop: 10,
                            paddingBottom: 10,
                            textAlignVertical: "top",
                            borderRadius: 8,
                            borderWidth: .8,
                            borderColor: colors.dgColor
                        }]}
                    customInputStyle={{
                        textAlignVertical: "top", flex: 1,
                        height: 130,
                    }}
                    numberOfLines={10}

                    multiline={true}
                    autoCapitalizes={"none"}
                    inputHeading={'About you'}
                    placeholder={'Tell something about you'}
                    value={longIntro}
                    onChangeText={(text) => {
                        setLongIntro(text)
                    }}
                />
                <Text style={styles.headerText}>Education</Text>
                <View style={styles.educationMainContainer}>
                    <View style={styles.imageContainer}>
                        <Image
                            style={styles.educationIcon}
                            source={icons.educationIcon}
                        />
                    </View>
                    <View style={styles.rightContainer}>
                        <Text style={styles.insituteName}>ABC University, USA.</Text>
                        <Text style={styles.degreeName}>Master of Science in Computer Science.</Text>
                        <View style={styles.combineBottomContainer}>
                            <View style={styles.itemBottomContainer}>
                                <View style={styles.circleStyle} />
                                <Text style={styles.yearText}>California, USA.</Text>
                            </View>
                            <View style={[styles.itemBottomContainer, { marginLeft: 30 }]}>
                                <View style={styles.circleStyle} />
                                <Text style={styles.yearText}>2016</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <InputField
                    customStyle={styles.inputSearchContainer}
                    autoCapitalizes={"none"}
                    inputHeading={'Name of Institution'}
                    placeholder={'Name of Institution'}
                    // keyboardType={'number-pad'}
                    value={instituteName}
                    onChangeText={(text) => {
                        setInstituteName(text)
                    }}
                    customInputStyle={styles.inputText}
                    customStyle={styles.inputBackground}
                />
                <InputField
                    customStyle={[styles.inputSearchContainer, { marginTop: 10 }]}
                    autoCapitalizes={"none"}
                    inputHeading={'Degree title'}
                    placeholder={'Degree title'}
                    // keyboardType={'number-pad'}
                    value={degreeTitle}
                    onChangeText={(text) => {
                        setDegreeTitle(text)
                    }}
                    customInputStyle={styles.inputText}
                    customStyle={styles.inputBackground}
                />
                <Text style={styles.headingText}>Country</Text>
                <View style={styles.pickerContainer}>
                    <Picker
                        mode="dropdown"
                        style={{ width: width, }}
                        selectedValue={selectedCountryField}
                        textStyle={{ color: colors.dgColor }}
                        onValueChange={onCountryChange}
                    >
                        <Picker.Item label="Country" value="key0" />
                        <Picker.Item label="United State" value="key1" />
                        <Picker.Item label="Pakistan" value="key2" />
                    </Picker>
                    {Platform.OS === 'ios' &&
                        <Image
                            style={styles.dropDownStyle}
                            source={icons.dropdownIcon}
                        />
                    }
                </View>
                <Text style={styles.headingText}>City</Text>
                <View style={styles.pickerContainer}>
                    <Picker
                        mode="dropdown"
                        style={{ width: width, }}
                        selectedValue={selectedCityField}
                        textStyle={{ color: colors.dgColor }}
                        onValueChange={onCityChange}
                    >
                        <Picker.Item label="City" value="key0" />
                        <Picker.Item label="Gujranwala" value="key1" />
                        <Picker.Item label="Lahore" value="key2" />
                    </Picker>
                    {Platform.OS === 'ios' &&
                        <Image
                            style={styles.dropDownStyle}
                            source={icons.dropdownIcon}
                        />
                    }
                </View>
                <Text style={styles.headingText}>Completion year</Text>

                <View style={styles.pickerContainer}>
                    <Picker
                        mode="dropdown"
                        style={{ width: width, }}
                        selectedValue={selectedCompletionField}
                        textStyle={{ color: colors.dgColor }}
                        onValueChange={onCompletionChange}
                    >
                        <Picker.Item label="Completion year" value="key0" />
                        <Picker.Item label="2019" value="key1" />
                        <Picker.Item label="1018" value="key2" />
                    </Picker>
                    {Platform.OS === 'ios' &&
                        <Image
                            style={styles.dropDownStyle}
                            source={icons.dropdownIcon}
                        />
                    }
                </View>
                <View style={[styles.termsContainer, { marginTop: 30 }]}>
                    <CheckBox
                        onChange={() => { setCurrentlyEnroll(!currentlyEnroll) }}
                        onChangeTerm={() => {
                        }}
                        customTxt={'Currently enrolled'}
                        isChecked={currentlyEnroll}
                        tintColor={colors.black}
                        checkstyle={{ borderWidth:.8,borderColor:colors.dgColor}}

                    />
                </View>
                <Button
                    text={'Add'}
                    textStyle={{ color: colors.lightOrange }}
                    backgroundColorStyle={{
                        width: 160,
                        backgroundColor: colors.white,
                        borderWidth: 1,
                        borderColor: colors.lightOrange,
                        shadowOpacity: 0,
                        alignSelf: 'flex-end'
                    }}
                />
                <Text style={styles.headerText}>Experience</Text>
                <FlatList
                    data={experienceList}
                    showsVerticalScrollIndicator={false}
                    // contentContainerStyle={{ }}
                    renderItem={renderExperienceItem}
                    ListEmptyComponent={renderEmptyContainer}
                    showsVerticalScrollIndicator={false}
                />
                <InputField
                    customStyle={[styles.inputSearchContainer, { marginTop: 10 }]}
                    autoCapitalizes={"none"}
                    inputHeading={'Teaching place'}
                    placeholder={'Teaching place'}
                    // keyboardType={'number-pad'}
                    value={teachingPlace}
                    onChangeText={(text) => {
                        setTeachingPlace(text)
                    }}
                    customInputStyle={styles.inputText}
                    customStyle={styles.inputBackground}
                />
                <Text style={styles.headingText}>From year</Text>

                <View style={styles.pickerContainer}>
                    <Picker
                        mode="dropdown"
                        style={{ width: width, }}
                        selectedValue={selectedFromYearField}
                        textStyle={{ color: colors.dgColor }}
                        onValueChange={onFromYearChange}
                    >
                        <Picker.Item label="From year" value="key0" />
                        <Picker.Item label="2010" value="key1" />
                        <Picker.Item label="2011" value="key2" />
                    </Picker>
                    {Platform.OS === 'ios' &&
                        <Image
                            style={styles.dropDownStyle}
                            source={icons.dropdownIcon}
                        />
                    }
                </View>
                <Text style={styles.headingText}>To year</Text>
                <View style={styles.pickerContainer}>
                    <Picker
                        mode="dropdown"
                        style={{ width: width, }}
                        selectedValue={selectedToYearField}
                        textStyle={{ color: colors.dgColor }}
                        onValueChange={onToYearChange}
                    >
                        <Picker.Item label="To year" value="key0" />
                        <Picker.Item label="2017" value="key1" />
                        <Picker.Item label="2018" value="key2" />
                    </Picker>
                    {Platform.OS === 'ios' &&
                        <Image
                            style={styles.dropDownStyle}
                            source={icons.dropdownIcon}
                        />
                    }
                </View>
                <InputField
                    customStyle={[styles.inputSearchContainer, { marginTop: 10 }]}
                    autoCapitalizes={"none"}
                    inputHeading={'Short description'}
                    placeholder={'Short description'}
                    // keyboardType={'number-pad'}
                    value={shortDescription}
                    onChangeText={(text) => {
                        setShortDescription(text)
                    }}
                    customInputStyle={styles.inputText}
                    customStyle={styles.inputBackground}
                />
                <View style={[styles.termsContainer, { marginTop: 30 }]}>
                    <CheckBox
                        onChange={() => { setCurrentlyWorked(!currentlyWorked) }}
                        onChangeTerm={() => {
                        }}
                        customTxt={'Currently Working here'}
                        isChecked={currentlyWorked}
                        tintColor={colors.black}
                        checkstyle={{ borderWidth:.8,borderColor:colors.dgColor}}
                    />
                </View>
                <Button
                    text={'Add'}
                    textStyle={{ color: colors.lightOrange }}
                    backgroundColorStyle={{
                        width: 160,
                        backgroundColor: colors.white,
                        borderWidth: 1,
                        borderColor: colors.lightOrange,
                        marginBottom: 50,
                        shadowOpacity: 0,
                        alignSelf: 'flex-end'
                    }}
                />
                <Button
                    text={'Save Profile'}
                    // textStyle={{ }}
                    backgroundColorStyle={{
                        width: 135,
                        backgroundColor: colors.lightGreen,
                        shadowOpacity: 0,
                        marginBottom: 30
                    }}
                />
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </View>
    );
};
const styles = StyleSheet.create({
    safeStyle: {
        width: '100%',
    },
    headerText: {
        fontFamily: fonts.Bold,
        fontSize: 18,
        marginTop: 30
    },
    descriptionText: {
        fontFamily: fonts.Regular,
        fontSize: 12,
        color: colors.dgColor
    },
    educationMainContainer: {
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center'
    },
    imageContainer: {
        width: 64,
        height: 64,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.InputFieldBackground,
        borderRadius: 10
    },
    educationIcon: {
        width: 28,
        height: 28,
        resizeMode: 'contain'
    },
    rightContainer: {
        marginLeft: 15
    },
    insituteName: {
        fontSize: 12,
        fontFamily: fonts.Bold,
        color: colors.black
    },
    degreeName: {
        fontSize: 12,
        fontFamily: fonts.Regular,
        color: colors.black,
        marginTop: 4,
    },
    combineBottomContainer: {
        marginTop: 4,
        flexDirection: 'row',
        width: '100%',
    },
    itemBottomContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    circleStyle: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: colors.dgColor,
        marginRight: 10
    },
    yearText: {
        fontSize: 12,
        color: colors.dgColor,
        fontFamily: fonts.Regular
    },
    emptyContainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyText: {
        textAlign: 'center',
        fontSize: 12,
        fontFamily: fonts.SemiBold,
        color: colors.dgColor
    },
    itemMainContainer: {
        marginTop: 10,
        marginBottom: 20
    },
    expeienceHeadingContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    pickerContainer: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '98%',
        alignSelf: 'center',
        backgroundColor: colors.white,
        borderRadius: 10,
        marginTop: 10,
        borderWidth: .8,
        borderColor: colors.dgColor
    },
    dropDownStyle: {
        width: 10,
        height: 6,
        resizeMode: 'contain',
        right: 18,
        position: 'absolute'
    },
    inputSearchContainer: {
        backgroundColor: colors.white,
        width: '99%',
        marginTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        alignSelf: 'center'
    },
    termsContainer: {
        flexDirection: 'row',
        marginTop: 30,
        alignItems: 'center',
    },
    inputText: {
        fontSize: 12,
        fontFamily: fonts.Medium
    },
    inputBackground: {
        backgroundColor: colors.white,
        borderWidth: .8,
        borderRadius: 8,
        borderColor: colors.dgColor,
        paddingLeft: 20
    },
    headingText: { fontSize: 12, color: colors.black, fontFamily: fonts.Bold, marginTop: 13 }
});

export default AboutComponent;

