import { StyleSheet } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'
export const styles = StyleSheet.create({
  safeStyle: {
    width:'100%',
    height:'100%',
  },
  fieldContainer:{
    // flexGrow: 1 ,
    paddingBottom:60,
    marginTop:15,
    marginLeft:20,
    marginRight:20
  },
  profileContainer:{
    flexDirection:'row',
    alignItems:'center'
  },
  profileIcon:{
    width:50,
    height:50,
    resizeMode:'cover',
    borderRadius:15
  },
  profileBtn:{
    width:'50%',
    backgroundColor:colors.lightGreen,
    marginLeft:20,
    shadowOpacity:0,
    height:44,
    borderRadius:7
  },
  linkContainer:{
    flexDirection:'row',
    marginTop:20,
    alignItems:'center',
  },
  linkText:{
    color:colors.lightOrange,
    fontSize:12,
    fontFamily:fonts.Medium
  },
  linkIcon:{
    width:10,
    height:5,
    resizeMode:'contain',
    marginLeft:10
  },
  linkUpIcon:{
    width:6,
    height:7,
    resizeMode:'stretch',
    marginLeft:10
  },
});
