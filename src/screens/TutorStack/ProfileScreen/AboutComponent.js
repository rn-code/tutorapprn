import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'


const AboutComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [experienceList, setExperienceList] = useState([{}, {}, {}]);
  const [timeTableList, setTimeTableList] = useState([
    {
      day: 'Monaday',
      slot: '3 pm - 7 pm'
    },
    {
      day: 'Monaday',
      slot: '4 pm - 5 pm & 7 pm - 10 pm'
    },
    {
      day: 'Wednesday',
      slot: '6 pm - 7 pm'
    },
    {
      day: 'Thursday',
      slot: '6 pm - 10 pm'
    },
    {
      day: 'Friday',
      slot: 'not available'
    },
    {
      day: 'Saturday',
      slot: '9 am - 12 pm & 3 pm - 7 pm'
    },
    {
      day: 'Sunday',
      slot: 'not available'
    }
  ]);

  const renderExperienceItem = ({ item, index }) => {
    return (
      <View style={[styles.itemMainContainer]}>
        <View style={styles.expeienceHeadingContainer}>
          <View style={styles.circleStyle} />
          <Text style={styles.insituteName}>ABC University, USA.</Text>
        </View>
        <Text style={[styles.yearText, { marginTop: 4, marginLeft: 18 }]}>2016 - 2018</Text>
        <Text style={[styles.degreeName, { marginLeft: 18 }]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisi, pellentesque eget venenatis, morbi venenatis sed posuere aenean scelerisque. Nisl pellentesque sit arcu lobortis nisl consequat sagittis.</Text>
      </View>
    );
  };
  const renderEmptyContainer = () => {
    return (
      <View style={[styles.emptyContainer]}>
        <Text style={styles.emptyText}>{'No item exist'}</Text>
      </View>
    )
  };
  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{marginBottom:10 }}
        contentContainerStyle={{ flexGrow: 1 }}>
        <Text style={styles.headerText}>About</Text>
        <Text style={styles.descriptionText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisi, pellentesque eget venenatis, morbi venenatis sed posuere aenean scelerisque. Nisl pellentesque sit arcu lobortis nisl consequat sagittis. Quis sed cras magna volutpat id ut turpis. Quam scelerisque turpis elementum leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisi, pellentesque eget venenatis, morbi venenatis sed posuere aenean scelerisque. Nisl pellentesque sit arcu lobortis nisl consequat sagittis. Quis sed cras magna volutpat id ut turpis. Quam scelerisque turpis elementum leo.</Text>
        <Text style={styles.headerText}>Education</Text>
        <View style={styles.educationMainContainer}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.educationIcon}
              source={icons.educationIcon}
            />
          </View>
          <View style={styles.rightContainer}>
            <Text style={styles.insituteName}>ABC University, USA.</Text>
            <Text style={styles.degreeName}>Master of Science in Computer Science.</Text>
            <View style={styles.combineBottomContainer}>
              <View style={styles.itemBottomContainer}>
                <View style={styles.circleStyle} />
                <Text style={styles.yearText}>California, USA.</Text>
              </View>
              <View style={[styles.itemBottomContainer, { marginLeft: 30 }]}>
                <View style={styles.circleStyle} />
                <Text style={styles.yearText}>2016</Text>
              </View>
            </View>
          </View>
        </View>

        <Text style={styles.headerText}>Experience</Text>
        <FlatList
          data={experienceList}
          showsVerticalScrollIndicator={false}
          // contentContainerStyle={{ }}
          renderItem={renderExperienceItem}
          ListEmptyComponent={renderEmptyContainer}
          showsVerticalScrollIndicator={false}
        />
        <View style={styles.AvailabilityHeadingContainer}>
          <Text style={[styles.headerText, { marginTop: 0,width:120 }]}>Availability</Text>
          <Text style={styles.showAvailablity}>Show schedule</Text>
        </View>
        {
          timeTableList.map((item) => {
            return (
              <View style={styles.timeTableItemContainer}>
                <Text style={[styles.dayText,{
                  width:120
                }]}>{item.day}</Text>
                <Text style={[styles.dayText,{
                  color:item.slot==='not available' ? colors.dgColor :colors.black,
                  marginLeft:15,
                  width:'50%',
                  }]}>{item.slot}</Text>
              </View>
            )
          })
        }
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    height: '100%',
    paddingLeft: 22,
    paddingRight: 22
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    marginTop: 30
  },
  descriptionText: {
    fontFamily: fonts.Regular,
    fontSize: 12,
    color: colors.dgColor
  },
  educationMainContainer: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center'
  },
  imageContainer: {
    width: 64,
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.InputFieldBackground,
    borderRadius: 10
  },
  educationIcon: {
    width: 28,
    height: 28,
    resizeMode: 'contain'
  },
  rightContainer: {
    marginLeft: 15
  },
  insituteName: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black
  },
  degreeName: {
    fontSize: 12,
    fontFamily: fonts.Regular,
    color: colors.black,
    marginTop: 4,
  },
  combineBottomContainer: {
    marginTop: 4,
    flexDirection: 'row',
    width: '100%',
  },
  itemBottomContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  circleStyle: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: colors.dgColor,
    marginRight: 10
  },
  yearText: {
    fontSize: 12,
    color: colors.dgColor,
    fontFamily: fonts.Regular
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    marginTop: 10,
    marginBottom: 20
  },
  expeienceHeadingContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  AvailabilityHeadingContainer: {
    flexDirection: 'row',
    marginTop: 30,
    alignItems: 'center'
  },
  showAvailablity: {
    color: colors.lightGreen,
    marginLeft: 15
  },
  timeTableItemContainer: {
    flexDirection: 'row',
    marginTop:10
  },
  dayText:{
    color:colors.black,
    fontSize:12,
    fontFamily:fonts.Regular
  }
});

export default AboutComponent;
