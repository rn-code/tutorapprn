import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
    backgroundColor: colors.white
  },
  backBtnContainer: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20
  },
  backImage: {
    width: 19,
    height: 19,
    resizeMode: 'contain',
    marginRight: 13
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 24,
    lineHeight: 36,
  },
  topHeaderContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 20
  },
  personCardContainer: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
  rightContainer: {
    width: width - 140,
    paddingLeft: 15,
    // backgroundColor:'red'
  },
  profileImageContainer: {
    width: 100,
    height: 100
  },
  personImage: {
    width: 100,
    height: 100,
    resizeMode: 'cover',
    borderRadius: 10
  },
  barcontainer: {
    backgroundColor: colors.lightOrange,
    width: '100%',
    padding: 3,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  featureText: {
    color: colors.white,
    fontFamily: fonts.Regular,
    fontSize: 12
  },
  subRightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%'
  },
  personNameStyle: {
    fontSize: 18,
    fontFamily: fonts.Bold,
    color: colors.black
  },
  priceStyle: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black
  },
  featureStyle: {
    fontFamily: fonts.Medium,
    fontSize: 12,
    color: colors.lightOrange
  },
  imageContainer:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
  },
  shareIconStyle: {
    width: 18,
    height: 20,
    resizeMode: 'contain'
  },
  ratingContainer: {
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center'
  },
  ratingCountStyle: {
    marginLeft: 5,
    fontSize: 12,
    fontFamily: fonts.Regular,
    color: colors.dgColor
  },
  onlineStyle: {
    width: 8,
    height: 8,
    backgroundColor: colors.brightGreen,
    position: 'absolute',
    top: 8,
    right: 12,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: colors.white
  },
  descriptionText:{
    marginTop:20,
    fontFamily:fonts.Regular,
    fontSize:12,
    color:colors.black
  },
  responseRateContainer:{
    flexDirection:'row',
    width:'100%',
    marginTop:10,
    justifyContent:'space-between'
  },
  responseRateText:{
    fontSize:12,
    fontFamily:fonts.Bold,
    color:colors.black
  },
  buttonContainer:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    marginTop:18
  },
  scheduleBtn:{
    backgroundColor:colors.lightGreen,
    shadowOpacity:0,
    width:'47%'
},
messageBtn:{
    backgroundColor:colors.white,
    shadowOpacity:0,
    width:'47%',
    borderWidth:1,
    borderColor:colors.lightOrange
}
});
