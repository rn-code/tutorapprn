import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    TouchableOpacity,
    Pressable,
} from "react-native";
import moment from 'moment'
import { SafeAreaView } from 'react-native-safe-area-context'

import { Rating } from 'react-native-ratings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useSelector } from 'react-redux'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import fonts from '../../../assets/fonts'
import Button from "../../../components/Button"
import colors from "../../../utils/colors";
import { styles } from "./Styles";

import HighlightComponent from './HighlightComponent'
import AboutComponent from './AboutComponent'
import GalleryComponent from './GalleryComponent'
import ReviewComponent from './ReviewComponent'
import PricingComponent from './PricingComponent'
import ScheduleComponent from './ScheduleComponent'


const { width, height } = Dimensions.get('window')
const TopTab = createMaterialTopTabNavigator();

const MyTopTabs = (props) => {

    useEffect(() => {
    }, [])
    return (
        <TopTab.Navigator
            headerMode={'none'}
            lazy={true}
            backBehavior="initialRoute"
            screenOptions={{
                scrollEnabled: true,
                tabBarActiveTintColor: colors.green,
                style: { backgroundColor: colors.lightGrey },
                tabBarBounces:true,
                tabBarScrollEnabled:true,
                tabBarStyle: {
                    width: 'auto',
                    paddingHorizontal: 10,
                    margin: 0,
                    borderTopWidth:.8,
                    borderBottomWidth:.8,
                    borderColor:colors.dgColor,
                    marginTop:30
                },
                tabBarItemStyle:{
                    width:120
                },
                tabBarLabelStyle: {
                    fontSize: 12,
                    fontFamily: fonts.Medium,
                    color: colors.dgColor,
                    textTransform: 'capitalize'
                },
                tabBarIndicatorStyle:{
                    backgroundColor: colors.green, height: 4
                }
            }}>
            <TopTab.Screen
                name={'Highlights'}
                children={(_props) => <HighlightComponent {..._props} />}
            />
            <TopTab.Screen
                name={'About'}
                children={(_props) => <AboutComponent {..._props} />}
            />
            <TopTab.Screen
                name={'Gallery'}
                children={(_props) => <GalleryComponent {..._props} />}
            />
            <TopTab.Screen
                name={'Reviews'}
                children={(_props) => <ReviewComponent {..._props} />}
            />
            <TopTab.Screen
                name={'Pricing'}
                children={(_props) => <PricingComponent {..._props} />}
            />
            <TopTab.Screen
                name={'Schedule'}
                children={(_props) => <ScheduleComponent {..._props} />}
            />


        </TopTab.Navigator>
    );
}

const ProfileScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);

    useEffect(() => {

    }, [])
    const ratingCompleted = (rating) => {
        console.log("Rating is: " + rating)
        setratingSelByUser(rating)
    }

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>Tutor Profile</Text>
            </Pressable>
            <View style={styles.topHeaderContainer}>
                <View style={[styles.personCardContainer]}>
                    <View style={styles.profileImageContainer}>
                        <Image
                            source={icons.dumyIcon}
                            style={styles.personImage}
                        />
                        <View style={styles.barcontainer}>
                            <Text style={styles.featureText}>Featured</Text>
                        </View>
                        <View style={styles.onlineStyle} />
                    </View>
                    <View style={styles.rightContainer}>
                        <View style={styles.subRightContainer}>
                            <Text style={styles.personNameStyle}>Ali Bashir</Text>
                            <Text style={styles.priceStyle}>$8.5/hr</Text>
                        </View>
                        <View style={[styles.subRightContainer, { marginTop: 10 }]}>
                            <View style={styles.ratingContainer}>
                                <Rating
                                    startingValue={5}
                                    ratingCount={5}
                                    showRating={false}
                                    imageSize={17}
                                    fractions={1}
                                    readonly={true}
                                    type={'custom'}
                                    selectedColor={'#EFC282'}
                                    ratingColor={'#EFC282'}
                                    ratingBackgroundColor='#c8c7c8'
                                    onFinishRating={ratingCompleted}
                                />
                                <Text style={styles.ratingCountStyle}>(77)</Text>
                            </View>
                            <View style={styles.imageContainer}>
                                <Image
                                    style={styles.shareIconStyle}
                                    source={icons.shareIcon}
                                />
                                <Image
                                    style={[styles.shareIconStyle, { marginLeft: 20 }]}
                                    source={icons.unFillStarIcon}
                                />
                            </View>
                        </View>
                        <Text style={[styles.ratingCountStyle, {
                            marginLeft: 0,
                            marginTop: 10
                        }]}>77 lessons</Text>
                    </View>
                </View>
                <Text style={styles.descriptionText}>This is my short description of 50 characters.</Text>
                <View style={styles.responseRateContainer}>
                    <Text style={styles.responseRateText}>Response rate</Text>
                    <Text style={styles.responseRateText}>2 hrs</Text>
                </View>
                <Text style={[styles.ratingCountStyle, {
                    marginLeft: 0,
                    marginTop: 6
                }]}>(excluding weekends)</Text>
                <View style={styles.buttonContainer}>
                    <Button
                        text={'Schedule'}
                        // textStyle={{ }}
                        backgroundColorStyle={styles.scheduleBtn}
                    // clickAction={}
                    />
                    <Button
                        text={'Message'}
                        textStyle={{ color: colors.lightOrange }}
                        backgroundColorStyle={styles.messageBtn}
                    // clickAction={}
                    />
                </View>
            </View>
            <View style={{flex:1}}>
                <MyTopTabs />
            </View>

            <Loader loading={loading} />

        </SafeAreaView>
    );
};

export default ProfileScreen;
