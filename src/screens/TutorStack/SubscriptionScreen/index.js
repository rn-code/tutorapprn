import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    TouchableOpacity,
    Pressable,
    ScrollView,
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { useSelector } from 'react-redux'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from "../../../components/Header";
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')



const PurchaseScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [cardName, setCardName] = useState("");
    const [cardNumber, setCardNumber] = useState("");
    const [expirationDate, setExpirationDate] = useState("");
    const [cvv, setCvv] = useState("");
    const [purchaseList, setPurchase] = useState([
        {
            date: '11/30/2020',
            detailDate: 'Mon ThuDec 10 - Jan 27 at 18:00 Istanbul time',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            date: '11/30/2020',
            detailDate: 'Mon ThuDec 10 - Jan 27 at 18:00 Istanbul time',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            date: '11/30/2020',
            detailDate: 'Mon ThuDec 10 - Jan 27 at 18:00 Istanbul time',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        }
    ])

    useEffect(() => {


    }, [])
    const renderChatItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer]}>
                <View style={styles.textContainer}>
                    <Text style={styles.detailDate}>{'$17.9 on 26/06/2021'}</Text>
                    <Text style={[styles.itemRightText]}>{'Successful'}</Text>
                </View>
                {index + 1 === purchaseList.length &&
                    <TouchableOpacity>
                        <Text style={styles.showOlderText}>Show older</Text>
                    </TouchableOpacity>
                }
                {index + 1 != purchaseList.length && <View style={styles.barView} />}
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No chat exist'}</Text>
            </View>
        )
    };

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>Subscription</Text>
            </Pressable>
            <ScrollView>

                <View style={styles.topPaddingContainer}>
                    <Text style={styles.greyHeading}>MONTHLY</Text>
                    <Text style={styles.priceText}>$17.9/month</Text>
                    <Text style={styles.renewText}>Renews on 26/06/2021.</Text>
                    <Text style={styles.greyHeading}>Saved PAYMENT METHOD</Text>
                    <View style={styles.visaContainer}>
                        <View style={styles.leftVisaContainer}>
                            <Image
                                style={styles.visaStyle}
                                source={icons.visaIcon}
                            />
                            <Text style={styles.cardNumberStyle}>*** 6597</Text>
                        </View>
                        <Text style={styles.editBtn}>Edit</Text>
                    </View>
                    <Text style={styles.greyHeading}>SUBSCRIPTION HISTORY</Text>

                </View>
                <View style={styles.mainView}>
                    <FlatList
                        data={purchaseList}
                        showsVerticalScrollIndicator={false}
                        // contentContainerStyle={{ }}
                        renderItem={renderChatItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                <View style={styles.bottomPaddingContainer}>
                    <Text style={styles.greyHeading}>PAYMENT METHOD</Text>
                    <InputField
                        autoCapitalizes={"none"}
                        inputHeading={'Name on card'}
                        placeholder={'Name on card'}
                        value={cardName}
                        onChangeText={(text) => {
                            setCardName(text)
                        }}
                        customInputStyle={styles.inputText}
                        customStyle={styles.inputBackground}
                    />
                    <InputField
                        autoCapitalizes={"none"}
                        inputHeading={'Card number'}
                        placeholder={'Card number'}
                        keyboardType={'number-pad'}
                        maxLength={16}
                        value={cardNumber}
                        onChangeText={(text) => {
                            setCardNumber(text)
                        }}
                        customInputStyle={styles.inputText}
                        customStyle={styles.inputBackground}
                        mainStyle={{ marginTop: 10 }}

                    />
                    <InputField
                        autoCapitalizes={"none"}
                        inputHeading={'Expiration date'}
                        placeholder={'Expiration date'}
                        value={expirationDate}
                        onChangeText={(text) => {
                            setExpirationDate(text)
                        }}
                        customInputStyle={styles.inputText}
                        customStyle={styles.inputBackground}
                        mainStyle={{ marginTop: 10 }}

                    />
                    <InputField
                        autoCapitalizes={"none"}
                        inputHeading={'CVV'}
                        placeholder={'CVV'}
                        maxLength={3}
                        keyboardType={'number-pad'}
                        value={cvv}
                        onChangeText={(text) => {
                            setCvv(text)
                        }}
                        customInputStyle={styles.inputText}
                        customStyle={styles.inputBackground}
                        mainStyle={{ marginTop: 10 }}

                    />

                    <View style={styles.buttonContainer}>
                        <Button
                            text={'Save'}
                            // textStyle={{ }}
                            backgroundColorStyle={styles.saveBtn}
                        />
                    </View>
                </View>
                <Loader loading={loading} />
            </ScrollView>
        </SafeAreaView>
    );
};

export default PurchaseScreen;
