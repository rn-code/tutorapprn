import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
  },
  topPaddingContainer: {
    paddingLeft: 20,
    paddingRight: 20
  },
  backBtnContainer: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
    paddingLeft: 17,
    paddingRight: 20,
    marginBottom:5
  },
  greyHeading: {
    fontFamily: fonts.Bold,
    fontSize: 14,
    color: colors.dgColor,
    marginTop: 22
  },
  priceText: {
    fontFamily: fonts.Bold,
    fontSize: 24,
    color: colors.black,
    marginTop: 10
  },
  renewText: {
    fontSize: 14,
    fontFamily: fonts.Regular,
    color: colors.dgColor,
    marginTop: 10
  },
  backImage: {
    width: 19,
    height: 19,
    resizeMode: 'contain',
    marginRight: 13
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 24,
    lineHeight: 36,
  },
  mainView: {
    width: '100%',
  },
  itemMainContainer: {
    marginTop: 22,
    width: '100%',
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 20,
    paddingRight: 20,
  },
  nameText: {
    fontFamily: fonts.SemiBold,
    fontSize: 12,
    color: colors.black,
    width: '97%',
  },
  detailDate: {
    color: colors.lightBlack,
    fontSize: 12,
    fontFamily: fonts.Medium,
  },
  itemRightText: {
    color: colors.chatMessage,
    fontSize: 12,
    fontFamily: fonts.Medium,
  },
  barView: {
    width: '95%',
    backgroundColor: colors.dgColor,
    height: 1,
    alignSelf: 'flex-end',
    marginTop: 20
  },
  showOlderText: {
    paddingLeft: 20,
    color: colors.lightGreen,
    fontFamily: fonts.SemiBold,
    marginTop: 20
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom:50
  },
  inputText: {
    fontSize: 12,
    fontFamily: fonts.Medium
  },
  inputBackground: {
    backgroundColor: colors.white,
    borderWidth: .8,
    borderRadius: 8,
    borderColor: colors.dgColor,
    paddingLeft: 20
  },
  bottomPaddingContainer:{
    paddingLeft:20,
    paddingRight:20
  },
  saveBtn: {
    backgroundColor: colors.lightGreen,
    height: 44,
    width: 80,
    borderRadius: 8,
    marginTop: 10,
    shadowOpacity: 0
},
visaStyle:{
  width:44,
  height:16,
  resizeMode:'contain'
},
visaContainer:{
  marginTop:12,
  flexDirection:'row',
  justifyContent:'space-between'
},
leftVisaContainer:{
  flexDirection:'row',
},
cardNumberStyle:{
  marginLeft:15,
  fontSize:12,
  fontFamily:fonts.Regular
},
editBtn:{
  fontFamily:fonts.Bold,
  fontSize:12,
  color:colors.lightOrange
}
});
