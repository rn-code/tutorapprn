import React, { useState } from "react";
import {
    View,
    Text,
    Linking,
    Image,
    SafeAreaView,
    Pressable,
    Share,
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'

const AboutUsScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [subscription, setSubscription] = useState('month');
    const [payMethed, setPayMethed] = useState('appleid');

    const redeemBtn = () => {

    }
    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                hearderText={'ABOUT'}
                leftIcon={icons.backIcon}
                onLeftAction={() => { navigation.goBack() }}
            />
            <KeyboardAwareScrollView
                contentContainerStyle={styles.mainContainer}
                showsVerticalScrollIndicator={false}>
                <Text style={styles.lightText}>
                    {`Onion Crypto Signals is an app made by Crypto enthusiasts. We aim to provide premium crypto signals and education with minimum membership fees. All of our signals are provided by renowned and premium crypto groups all over the world. But we fine tune these signals by our expert technical analysis team.

We are bringing revolutionary changes in Crypto world, by making it a simple tool for everyone. Join our Telegram and Discord Communities and get to know all the basics required to crack a perfect profitable trade. And get to know all of our upcoming crypto projects updates!`}
                </Text>
                <View style={styles.linkContainer}>
                    <View>
                        <Text style={[styles.lightText]}>{"Join us on our Discord Server here: "}</Text>
                    </View>
                    <Pressable
                        style={styles.linkButtonStyle}
                        onPress={() => {
                            Linking.openURL('https://discord.gg/YW3A4hjYpM')
                        }}>
                        <Image
                            source={icons.discordIcon}
                            style={styles.linkButtonLogo}
                        />
                        <Text style={[styles.linkText]}>{"Discord"}</Text>
                    </Pressable>
                </View>
                <View style={styles.linkContainer}>
                    <View>
                        <Text style={[styles.lightText]}>{"Join our Telegram Community here: "}</Text>
                    </View>
                    <Pressable
                        style={[styles.linkButtonStyle, { backgroundColor: '#40B3E0' }]}
                        onPress={() => {
                            Linking.openURL('https://t.me/onioncrypto')
                        }}>
                        <Image
                            source={icons.telegramPremiumIcon}
                            style={styles.linkButtonLogo}
                        />
                        <Text style={[styles.linkText]}>{"Telegram"}</Text>
                    </Pressable>
                </View>
                <Text style={styles.copyRightText}>
                    {`All Rights Reserved by "Onion Crypto Signals"`}
                    <Text
                        style={{ color: '#7289DA' }}
                        onPress={() => {
                            Linking.openURL('http://www.onioncryptosignals.com')
                        }}>
                        {`\nwww.onioncryptosignals.com`}
                    </Text>
                </Text>
                <Text style={styles.joinText}>{'Join'}</Text>
                <View style={styles.socialContainer}>
                    <Pressable
                        onPress={() => {
                            Linking.openURL('https://www.youtube.com/channel/UCG5YaPiaaWuKQBqEDysWqiA')
                        }}
                        style={[styles.socialTouch, { marginLeft: 0 }]}>
                        <Image style={styles.socialImage} source={icons.youtubeIcon} />
                    </Pressable>
                    <Pressable
                        onPress={() => {
                            Linking.openURL('https://t.me/onioncrypto')
                        }}
                        style={styles.socialTouch}>
                        <Image style={styles.socialImage} source={icons.telegramIcon} />
                    </Pressable>
                    <Pressable
                        onPress={() => {
                            Linking.openURL('https://www.facebook.com/onioncrypto')
                        }}
                        style={styles.socialTouch}>
                        <Image style={styles.socialImage} source={icons.facebookIcon} />
                    </Pressable>
                    <Pressable
                        onPress={() => {
                            Linking.openURL('https://discord.gg/YW3A4hjYpM')
                        }}
                        style={styles.socialTouch}>
                        <Image style={styles.socialImage} source={icons.socailNoneIcon} />
                    </Pressable>
                </View>
                <Pressable
                    onPress={() => {
                        Share.share({
                            message: 'Share app link in future',
                        }).then(result => {
                            console.log('share-result', result)
                        }).catch(errorMsg => {
                            console.log('share-errorMsg', errorMsg)
                        });
                    }}
                    style={styles.shareContainer}>
                    <Image style={styles.shareImage} source={icons.shareIcon} />
                    <Text style={styles.shareText}>Share this app</Text>
                </Pressable>
            </KeyboardAwareScrollView>
        </SafeAreaView>
    );
};
export default AboutUsScreen;
