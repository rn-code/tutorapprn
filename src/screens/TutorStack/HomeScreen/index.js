import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useSelector } from 'react-redux'

import InputField from '../../../components/InputField'
import Header from "../../../components/Header";
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import Button from "../../../components/Button"
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')



const HomeScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('');
    const [pageCounter, setPageCounter] = useState(1);
    const [categoryList, setCategoryList] = useState([
        {
            name: 'Education',
            icon: icons.schoolIcon
        },
        {
            name: 'Language',
            icon: icons.languageIcon
        },
        {
            name: 'Music',
            icon: icons.musicIcon
        }
    ])
    const [upcomingWorkshop, setUpcomingWorkshop] = useState([
        {
        }
    ])

    useEffect(() => {


    }, [])

    const renderCategory = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainerCategory]}>
                <Image
                    source={item.icon}
                    style={styles.categoryIcon}
                />
                <Text style={styles.categoryHeading}>{item.name}</Text>
            </Pressable>
        );
    };
    const renderUpcomingWorkshopItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer]}>
                <View style={styles.textContainer}>
                    <Image
                        source={icons.dumyBanerIcon}
                        style={styles.itemBannerStyle}
                    />
                    <Text style={styles.headingWorkshop}>{'My workshop'}</Text>
                    <Text style={styles.descWorkshop}>
                        {'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dui facilisis scelerisque et auctor tincidunt sed.'}
                    </Text>
                    <View style={styles.bottomBtnContaier}>
                        <Text style={styles.dateWorkshop}>{'August 18, 2021 at 3:00pm (PST).'}</Text>
                        <Button
                            text={'Save my seat'}
                            // textStyle={{ }}
                            backgroundColorStyle={styles.saveBtnStyle}
                            clickAction={()=>{
                                navigation.navigate('WorkshopDetailScreen')
                            }}
                        />
                    </View>
                </View>
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No item exist'}</Text>
            </View>
        )
    };

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Home'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    navigation.navigate('ProfileScreen')
                }}
            />
            <View style={styles.searchContainer}>
                <View style={{ width: '85%' }}>
                    <InputField
                        customStyle={styles.inputSearchContainer}
                        autoCapitalizes={"none"}
                        placeholder={'What you want to learn?'}
                        value={search}
                        onChangeText={(text) => {
                            setSearch(text)
                        }}
                        showLeftIcon={true}
                        customInputStyle={{ width: '89%' }}
                    />
                </View>
                <Pressable
                    onPress={() => {
                        navigation.navigate('FilterScreen')
                    }}
                    style={styles.filterContainer}>
                    <Image
                        style={styles.filterIcon}
                        source={icons.filterIcon}
                    />
                </Pressable>
            </View>
            <View style={styles.barSliderContainer}>
                <Text style={styles.topText}>Looking for English teacher?<Text style={styles.topTextBold}> Take a lesson now.</Text></Text>
            </View>
            <View style={styles.paginationContainer}>
                <View style={styles.dotStyle} />
                <View style={styles.dashStyle} />
                <View style={styles.dotStyle} />
            </View>
            <KeyboardAwareScrollView
                contentContainerStyle={{paddingBottom:30 }}
                showsVerticalScrollIndicator={false}
            >
                <View style={styles.mainView}>
                    <Text style={styles.headingStyles}>Categories</Text>
                    <FlatList
                        horizontal
                        data={categoryList}
                        showsVerticalScrollIndicator={false}
                        // contentContainerStyle={{ height: 110 }}
                        renderItem={renderCategory}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                    />
                    <Text style={[styles.headingStyles,{marginTop:30}]}>Upcoming workshop</Text>
                    <FlatList
                        data={upcomingWorkshop}
                        showsVerticalScrollIndicator={false}
                        // contentContainerStyle={{ }}
                        renderItem={renderUpcomingWorkshopItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default HomeScreen;
