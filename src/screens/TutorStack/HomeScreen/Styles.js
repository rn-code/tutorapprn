import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
    backgroundColor:colors.personBackground
  },
  searchContainer:{
    width:'90%',
    flexDirection:'row',
    borderRadius:10,
    marginTop:10,
    marginBottom:30,
    height:50,
    alignSelf:'center',
    backgroundColor:colors.filterBtnColor
  },
  filterContainer:{
    width:'15%',
    alignItems:'center',
    justifyContent:'center'
  },
  filterIcon:{
    width:21,
    height:15,
    resizeMode:'contain'
  },
  mainView: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  barSliderContainer:{
    width:width,
    padding:13,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#E4F3F1'
  },
  topText:{
    fontFamily:fonts.Regular,
    fontSize:12,
    color:colors.black
  },
  topTextBold:{
    fontFamily:fonts.SemiBold,
    fontSize:12,
    color:colors.lightOrange
  },
  itemMainContainer: {
    width:'98%',
    alignSelf:'center',
    marginTop: 12,
    backgroundColor: colors.white,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    borderRadius:10
  },
  paginationContainer:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    marginBottom:30,
    marginTop:10
  },
  itemMainContainerCategory: {
    marginTop: 10,
    marginRight:10,
    backgroundColor: colors.white,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    borderRadius:10,
    width:104,
    height:107,
    alignItems:'center',
    justifyContent:'center'
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  textContainer: {
    flex: 1,
    // justifyContent:'center',
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 13
  },
  headingWorkshop: {
    fontFamily: fonts.Regular,
    fontSize: 12,
    color: colors.black,
    width: '97%',
    marginTop:7
  },
  headerStyle: {
    paddingLeft: 20
  },
  headerText: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black
  },
  headingStyles: {
    fontFamily: fonts.SemiBold,
    fontSize: 18,
    color: colors.black
  },
  itemBannerStyle:{
    width:'100%',
    height:80,
    resizeMode:'cover',
    borderRadius:10
  },
  descWorkshop:{
    fontSize:12,
    fontFamily:fonts.Regular,
    color:colors.dgColor
  },
  dateWorkshop:{
    marginTop:19,
    color:colors.black,
    fontFamily:fonts.SemiBold,
    fontSize:12,
    marginBottom:18,
    width:'58%'
  },
  categoryIcon:{
    width:42,
    height:35,
    resizeMode:'contain'
  },
  categoryHeading:{
    fontSize:12,
    fontFamily:fonts.Regular,
    color:colors.black,
    textAlign:'center',
    marginTop:14
  },
  dotStyle:{
    width:6,
    height:6,
    borderRadius:4,
    backgroundColor:colors.dgColor
  },
  dashStyle:{
    width:20,
    height:6,
    borderRadius:4,
    backgroundColor:colors.lightGreen,
    marginLeft:5,
    marginRight:5
  },
  inputSearchContainer:{
    backgroundColor: colors.white,
    width: '100%',
    marginTop: 0,
    marginLeft: 0,
    marginRight:0,
    borderRadius: 0,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10
},
saveBtnStyle:{
  width:'34%',
  borderRadius:10,
  height:35,
  shadowOpacity:0
},
bottomBtnContaier:{
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center'
}
});
