import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
  },
  mainView: {
    width: '100%',
    marginBottom:170,
  },
  itemMainContainer: {
    marginTop: 22
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  textContainer: {
    flex: 1,
    // justifyContent:'center',
    paddingLeft: 20,
    paddingRight: 20,
  },
  nameText: {
    fontFamily: fonts.SemiBold,
    fontSize: 12,
    color: colors.black,
    width: '97%',
  },
  detailDate: {
    marginTop: 10,
    color: colors.lightBlack,
    fontSize: 12,
    fontFamily: fonts.Medium,
    // width:'50%'
  },
  barView: {
    width: '95%',
    backgroundColor: colors.dgColor,
    height: 1,
    alignSelf: 'flex-end',
    marginTop: 20
  },
  counterContainer: {
    borderWidth: 1,
    borderColor: colors.dgColor,
    padding: 6,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 5,
    marginLeft: 30,
    marginRight: 30
  },
  pagerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    position: 'absolute',
    width: '100%',
    bottom:10
  },
  arrowStyle: {
    width: 9,
    height: 14,
    resizeMode: 'contain'
  },
  counterText: {
    fontFamily: fonts.SemiBold,
    fontSize: 12,
    color: colors.black
  }
});
