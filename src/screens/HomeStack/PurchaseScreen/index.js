import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { useSelector } from 'react-redux'

import Header from "../../../components/Header";
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')



const PurchaseScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [pageCounter, setPageCounter] = useState(1);
    const [purchaseList, setPurchase] = useState([
        {
            date: '11/30/2020',
            detailDate: 'Mon ThuDec 10 - Jan 27 at 18:00 Istanbul time',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            date: '11/30/2020',
            detailDate: 'Mon ThuDec 10 - Jan 27 at 18:00 Istanbul time',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        },
        {
            date: '11/30/2020',
            detailDate: 'Mon ThuDec 10 - Jan 27 at 18:00 Istanbul time',
            price: '$160',
            heading: 'Learn how to read and write while having fun - kindergarten',
        }
    ])

    useEffect(() => {


    }, [])
    const renderChatItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer]}>
                <View style={styles.textContainer}>
                    <Text style={styles.nameText}>{item.heading}</Text>
                    <Text style={styles.detailDate}>{item.detailDate}</Text>
                    <Text style={[styles.detailDate, { marginTop: 30 }]}>{item.price}</Text>
                    <Text style={[styles.detailDate, { color: colors.chatMessage }]}>{item.date}</Text>
                </View>
              {index+1!=purchaseList.length && <View style={styles.barView} /> }
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No chat exist'}</Text>
            </View>
        )
    };

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Purchases'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    // navigation.toggleDrawer();
                }}
            />
            <View style={styles.mainView}>
                <FlatList
                    data={purchaseList}
                    showsVerticalScrollIndicator={false}
                    // contentContainerStyle={{ }}
                    renderItem={renderChatItem}
                    ListEmptyComponent={renderEmptyContainer}
                    showsVerticalScrollIndicator={false}
                />
            </View>
            <View style={styles.pagerContainer}>
                <Pressable
                    onPress={() => {
                        if (pageCounter != 1)
                            setPageCounter(pageCounter - 1)
                    }}
                >
                    <Image
                        source={icons.leftArrowIcon}
                        style={[styles.arrowStyle,
                        {
                            tintColor: pageCounter === 1 ?
                                colors.dgColor :
                                colors.black
                        }]}
                    />
                </Pressable>
                <View style={styles.counterContainer}>
                    <Text style={styles.counterText}>{pageCounter}</Text>
                </View>
                <Pressable
                    onPress={() => {
                        setPageCounter(pageCounter + 1)
                    }}
                >
                    <Image
                        source={icons.rightArrowIcon}
                        style={styles.arrowStyle}
                    />
                </Pressable>
            </View>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default PurchaseScreen;
