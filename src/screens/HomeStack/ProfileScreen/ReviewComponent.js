import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { Rating } from 'react-native-ratings';

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
import { color } from "react-native-reanimated";


const ReviewComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [reviewList, setReviewList] = useState([{}]);
  const [ratingSelByUser, setratingSelByUser] = useState(5);
  const ratingCompleted = (rating) => {
    console.log("Rating is: " + rating)
    setratingSelByUser(rating)
  }
  const renderReviewItem = ({ item, index }) => {
    return (
      <View
        style={[styles.itemMainContainer]}>
        <TouchableOpacity style={{flexDirection:'row'}}>
        <Image
          source={icons.dumyIcon}
          style={styles.personImage}
        />
        <View style={styles.rightContainer}>
            <Text style={styles.personNameStyle}>Ali Bashir</Text>
            <View style={styles.ratingContainer}>
              <Rating
                startingValue={5}
                ratingCount={5}
                showRating={false}
                imageSize={17}
                fractions={1}
                readonly={true}
                type={'custom'}
                selectedColor={'#EFC282'}
                ratingColor={'#EFC282'}
                ratingBackgroundColor='#c8c7c8'
                onFinishRating={ratingCompleted}
              />
              <Text style={styles.ratingCountStyle}>(77)</Text>
            </View>
            <Text style={styles.descriptionStyle}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit sit nibh amet nulla. </Text>        
        </View>
        
        </TouchableOpacity>
        <TouchableOpacity style={styles.loadMoreContainer}>
          <Text style={styles.loadText}>Load More</Text>
          <Image 
          source={icons.dropdownLoadIcon}
          style={styles.dropdownIcon}
          />
        </TouchableOpacity>
      </View>
    );
  };
  const renderEmptyContainer = () => {
    return (
      <View style={[styles.emptyContainer]}>
        <Text style={styles.emptyText}>{'No item exist'}</Text>
      </View>
    )
  };
  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 30 ,marginBottom:10}}
        contentContainerStyle={{ flexGrow: 1 }}>
        <Text style={styles.headerText}>Reviews</Text>
        <View style={styles.reviewMainContainer}>
          <View style={styles.reviewContainer}>
            <Image
              source={icons.unFillStarIcon}
              style={styles.starIcon}
            />
            <Text style={styles.ratingText}>4.9</Text>
          </View>
          <View style={styles.reviewContainer}>
            <Text style={[styles.ratingText, { color: colors.lightOrange }]}>77</Text>
            <Text style={styles.ratingText}>Reviews</Text>
          </View>
        </View>
        <FlatList
          data={reviewList}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flex: 1 }}
          renderItem={renderReviewItem}
          ListEmptyComponent={renderEmptyContainer}
          showsVerticalScrollIndicator={false}
        />
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    height: '100%',
    paddingLeft: 22,
    paddingRight: 22
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    lineHeight: 36,
  },
  reviewMainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  reviewContainer: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.dgColor,
    width: '48%',
    height: 94,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6
  },
  starIcon: {
    width: 33,
    height: 33,
    resizeMode: 'contain'
  },
  ratingText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    color: colors.dgColor,
    marginTop: 4
  },
  emptyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    width: '100%',
    // flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 30,
    backgroundColor: colors.white,
  },
  personImage: {
    width: 65,
    height:65,
    resizeMode: 'cover',
    borderRadius: 10
  },
  rightContainer: {
    width: '80%',
    paddingLeft: 20,
  },
  subRightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%'
  },
  personNameStyle: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black,
  },
  ratingContainer: {
    flexDirection: "row",
    marginTop:10
  },
  ratingCountStyle: {
    marginLeft: 5,
    fontSize: 12,
    fontFamily: fonts.Regular,
    color: colors.lightOrange,
  },
  descriptionStyle: {
    fontFamily: fonts.Regular,
    fontSize: 12,
    color: colors.black,
    marginTop:4
  },
  loadMoreContainer:{
    flexDirection:'row',
    alignSelf:'center',
    marginTop:50,
    alignItems:'center',
    justifyContent:'center'
  },
  loadText:{
    fontSize:16,
    fontFamily:fonts.Regular,
    color:colors.black,
    textDecorationLine:'underline'
  },
  dropdownIcon:{
    width:12,
    height:12,
    resizeMode:'contain',
    marginLeft:5
  }
});

export default ReviewComponent;
