import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  StyleSheet,
  TextInput,
  Dimensions,
  TouchableOpacity
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { Calendar, LocaleConfig } from 'react-native-calendars';

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
import { Item } from "native-base";
import { color } from "react-native-reanimated";

LocaleConfig.locales['fr'] = {
  monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
  monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
  dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
  today: 'Aujourd\'hui',
  dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
};
LocaleConfig.defaultLocale = 'fr';
const {width}=Dimensions.get('window')
const ScheduleComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [dayDetailList, setDayDetailList] = useState([
    {
      name: 'Booked an online class',
      color: colors.lightOrange
    },
    {
      name: 'Booked a class at student’s home',
      color: colors.black
    },
    {
      name: 'Booked a class at tutor’s home',
      color: colors.lightGreen
    }
  ]);

  const [slotList, setSlotList] = useState([
    {
      date: '10:30 AM', isSelected: false
    },
    {
      date: '11:30 AM', isSelected: false
    },
    {
      date: '11:30 AM', isSelected: false
    },
    {
      date: '09:30 AM', isSelected: false
    },
    {
      date: '11:30 AM', isSelected: false
    }
  ]);

  const selectBtn = async () => {

  };
  const IconLeft = () => {
    return (
      <View style={{ borderRadius: 7, borderWidth: 1, borderColor: colors.black, padding: 6 }}>
        <Image
          source={icons.leftArrowIcon}
          style={styles.arrowStyle}
        />
      </View>
    )
  }
  const IconRight = () => {
    return (
      <View style={{ borderRadius: 7, borderWidth: 1, borderColor: colors.black, padding: 6 }}>
        <Image
          source={icons.rightArrowIcon}
          style={styles.arrowStyle}
        />
      </View>
    )
  }
  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 30 }}
        contentContainerStyle={{ flexGrow: 1 }}>
        <Text style={styles.headerText}>Schedule</Text>
        <Text style={styles.descriptionText}>Please select your time slot</Text>
        <View style={styles.calendarAdjustment}>
        <Calendar
          firstDay={1}
          current={'2021-11-01'}
          minDate={'2012-05-10'}
          maxDate={'2050-05-30'}
          onPressArrowLeft={subtractMonth => subtractMonth()}
          onPressArrowRight={addMonth => addMonth()}
          onDayPress={(day) => { console.log('selected day', day) }}
          onDayLongPress={(day) => { console.log('selected day', day) }}
          onMonthChange={(month) => { console.log('month changed', month) }}
          renderArrow={(direction) => (direction === 'left' ? <IconLeft /> : <IconRight />)}


          theme={{

            dayTextColor: colors.dgColor,
            monthTextColor: colors.black,
            textSectionTitleColor: 'black',
            textSectionTitleDisabledColor: 'black',
            selectedDayBackgroundColor: 'black',
            selectedDayTextColor: 'black',

            textDayFontFamily: fonts.Medium,
            textMonthFontFamily: fonts.Medium,
            textDayHeaderFontFamily: fonts.Medium,
            textMonthFontWeight: '500',

            textDayFontSize: 14,
            textMonthFontSize: 14,
            textDayHeaderFontSize: 14
          }}

          markingType={'custom'}
          markedDates={{
            '2021-11-11': {
              customStyles: {
                container: {
                  backgroundColor: colors.lightOrange,
                },
                text: {
                  color: colors.white,
                  fontWeight: '600'
                }
              }
            },
            '2021-11-13': {
              customStyles: {
                container: {
                  backgroundColor: colors.black,
                },
                text: {
                  color: colors.white,
                  fontWeight: '600'
                }
              }
            },
            '2021-11-20': {
              customStyles: {
                container: {
                  backgroundColor: colors.lightGreen,
                },
                text: {
                  color: colors.white,
                  fontWeight: '600'
                }
              }
            }
          }}

        />
        </View>
        {dayDetailList.map((item) => {
          return (
            <View style={styles.dayDetail}>
              <View style={[styles.circleStyle, { backgroundColor: item.color }]} />
              <Text style={styles.detailText}>{item.name}</Text>
            </View>
          )
        })
        }
        <Text
          style={[styles.descriptionText,
          { marginTop: 30 }]}>
          Please select available time slot
        </Text>
        <View style={styles.slotMainContainer}>
          {slotList.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  let tempArray = [...slotList]
                  tempArray[index].isSelected = !tempArray[index].isSelected
                  setSlotList(tempArray)
                }}
                style={[
                  styles.dateContainer,
                  {
                   backgroundColor: item.isSelected ? colors.lightOrange : colors.white,
                   borderColor: item.isSelected ? colors.lightOrange : colors.dgColor
                  }
                ]}>
                <Text style={[
                  styles.slotText,
                  {
                    color:item.isSelected ? colors.white : colors.black,
                    fontFamily:item.isSelected ? fonts.Bold : fonts.Regular
                  }
                  ]}>{item.date}</Text>
              </TouchableOpacity>
            )
          })
          }
        </View>
        <TouchableOpacity>
          <Text style={styles.syncText}>Sunc with my google calendar</Text>
        </TouchableOpacity>
        <View style={styles.buttonContainer}>
          <Button
            text={'Next'}
            // textStyle={{ }}
            backgroundColorStyle={styles.btnStyle}
            clickAction={selectBtn.bind(this)}
          />
        </View>
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    height: '100%',
    paddingLeft: 22,
    paddingRight: 22
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    lineHeight: 36,
  },
  descriptionText: {
    fontSize: 12,
    fontFamily: fonts.Regular,
    color: colors.dgColor,
    marginTop: 7
  },
  buttonContainer: {
  },
  btnStyle: {
    backgroundColor: colors.lightGreen,
    marginBottom: 50,
    marginTop: 40
  },
  arrowStyle: {
    width: 10,
    height: 10,
    resizeMode: 'contain'
  },
  dayDetail: {
    marginTop: 8,
    flexDirection: 'row'
  },
  circleStyle: {
    width: 18,
    height: 18,
    borderRadius: 9,
    backgroundColor: colors.lightOrange
  },
  detailText: {
    fontSize: fonts.Regular,
    fontSize: 12,
    color: colors.black,
    marginLeft: 14
  },
  dateContainer: {
    borderRadius: 10,
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderWidth: 1,
    marginTop: 10,
    marginRight: 10,
    borderColor: colors.dgColor
  },
  slotMainContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  slotText: {
    fontSize: 12,
    color: colors.black,
    fontFamily: fonts.Regular
  },
  syncText: {
    fontFamily: fonts.Bold,
    fontSize: 12,
    color: colors.lightGreen,
    marginTop: 30,
  },
  calendarAdjustment:{
    width:width-10,
    alignSelf:'center'
  },
});

export default ScheduleComponent;
