import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  TextInput
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useDispatch } from "react-redux";

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'


const PricingComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [packageList, setPackageList] = useState([{},{},{}]);
  const [lessonRateList, setLessonRateList] = useState([{},{},{}]);

  const selectBtn = async () => {

  };
  const renderPackageItem = ({ item, index }) => {
    return (
      <View style={[styles.itemMainContainer]}>
        <View style={styles.packageitemTopRow}>
          <Text style={styles.itemHeading}>Weekly</Text>
          <Text style={[
            styles.itemHeading,
            { color: colors.lightOrange }
          ]}>
            $100
          </Text>
        </View>
        <Text style={styles.itemDescription}>Lorem ipsum dolor sit amest</Text>
        <Button
            text={'Select'}
            // textStyle={{ }}
            backgroundColorStyle={styles.itemBtn}
            clickAction={selectBtn.bind(this)}
          />
      </View>
    );
  };
  const renderLessonRateItem = ({ item, index }) => {
    return (
      <View style={[styles.itemMainContainer]}>
        <View style={styles.packageitemTopRow}>
          <Text style={styles.itemHeading}>Group of upto 3</Text>
          <Text style={[
            styles.itemHeading,
            { color: colors.lightOrange }
          ]}>
            $100
          </Text>
        </View>
        <Text style={styles.itemDescription}>Lorem ipsum dolor sit amest</Text>
        <Button
            text={'Select'}
            // textStyle={{ }}
            backgroundColorStyle={styles.itemBtn}
            clickAction={selectBtn.bind(this)}
          />
      </View>
    );
  };
  const renderEmptyContainer = () => {
    return (
      <View style={[styles.emptyContainer]}>
        <Text style={styles.emptyText}>{'No item exist'}</Text>
      </View>
    )
  };
  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 30 ,marginBottom:10}}
        contentContainerStyle={{ flexGrow: 1 }}>
        <Text style={styles.headerText}>Pricing</Text>
        <View style={styles.priceContainer}>
          <Text style={styles.priceText}>
            $8.5
            <Text style={styles.hourText}>
              / hour
            </Text>
          </Text>
          <Button
            text={'Select'}
            // textStyle={{ }}
            backgroundColorStyle={styles.selectBtntyle}
            clickAction={selectBtn.bind(this)}
          />
        </View>
        <View style={styles.priceContainer}>
          <Text style={styles.priceText}>
            $4.5
            <Text style={styles.hourText}>
              / 30 minutes
            </Text>
          </Text>
          <Button
            text={'Select'}
            // textStyle={{ }}
            backgroundColorStyle={styles.selectBtntyle}
            clickAction={selectBtn.bind(this)}
          />
        </View>

        <Text style={[styles.headerText]}>Package rates</Text>
        <View>
          <FlatList
            horizontal
            data={packageList}
            showsVerticalScrollIndicator={false}
            renderItem={renderPackageItem}
            ListEmptyComponent={renderEmptyContainer}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <Text style={[styles.headerText,{marginTop:30}]}>Group lesson rates</Text>
        <View>
          <FlatList
            horizontal
            data={lessonRateList}
            showsVerticalScrollIndicator={false}
            renderItem={renderLessonRateItem}
            ListEmptyComponent={renderEmptyContainer}
            showsVerticalScrollIndicator={false}
          />
        </View>
        <Text style={styles.headerText}>Demo</Text>
        <View style={styles.availableContainer}>
          <Image 
            style={styles.tickIcon}
            source={icons.tickIcon}
          />
          <Text style={styles.availableText}>Available</Text>
          </View>
        <Text style={styles.bottomText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisi, pellentesque eget venenatis, morbi venenatis sed posuere aenean scelerisque.</Text>
        <View style={styles.buttonContainer}>
          <Button
            text={'Start demo lesson'}
            textStyle={{ color:colors.lightGreen}}
            backgroundColorStyle={styles.btnStyle}
            clickAction={selectBtn.bind(this)}
          />
        </View>
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    height: '100%',
    paddingLeft: 22,
    paddingRight: 22
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    lineHeight: 36,
  },
  buttonContainer: {
  },
  btnStyle: {
    backgroundColor: colors.white,
    borderColor:colors.lightGreen,
    borderWidth:1,
    shadowOpacity: 0,
    marginTop:10,
    marginBottom:30
  },
  itemBtn: {
    backgroundColor: colors.lightGreen,
    shadowOpacity: 0,
    height:30,
    marginTop:10
  },
  selectBtntyle: {
    backgroundColor: colors.lightGreen,
    width: 85,
    height: 30,
    shadowOpacity: 0
  },
  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 25
  },
  priceText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    color: colors.lightOrange,
    // lineHeight: 27
  },
  hourText: {
    fontFamily: fonts.Bold,
    fontSize: 12,
    color: colors.dgColor,
    // lineHeight: 18
  },
  itemMainContainer: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.dgColor,
    width: 220,
    padding: 20,
    // alignItems: 'center',
    // justifyContent: 'center',
    marginTop: 6,
    marginRight:10
  },
  packageitemTopRow: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemHeading: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black
  },
  itemDescription: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.dgColor,
    marginTop: 10
  },
  bottomText:{
    fontFamily:fonts.Regular,
    fontSize:12,
    color:colors.black,
    marginTop:5
  },
  availableContainer:{
    flexDirection:'row',
    marginTop:5
  },
  tickIcon:{
    width:18,
    height:18,
    resizeMode:'contain'
  },
  availableText:{
    fontSize:12,
    fontFamily:fonts.Regular,
    color:colors.dgColor,
    marginLeft:10
  }
});

export default PricingComponent;
