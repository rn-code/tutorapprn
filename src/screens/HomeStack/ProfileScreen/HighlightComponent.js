import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";

import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'


const HighlightComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [languageList, setLanguageList] = useState([
    { name: 'English' }
  ]);
  const [serviceList, setServiceList] = useState([
    { name: 'Online' },
    { name: 'At your home' },
    { name: 'At tutor’s home' },
  ]);

  const [fieldList, setFieldList] = useState([
    { name: 'Computer science' }
  ]);
  const [teachesList, setTeachesList] = useState([
    { name: 'Python' },
    { name: 'C++' },
    { name: 'C' },
  ]);
  const [levelList, setLevelList] = useState([
    { name: 'Secondary' },
    { name: 'GCSE' },
    { name: 'B Tech' },
    { name: 'Undergraduate' },
  ]);
  const [ageList, setAgeList] = useState([
    { name: '15 - 22' }
  ]);
  const selectBtn = async () => {

  };
  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 30,marginBottom:10 }}
        contentContainerStyle={{ flexGrow: 1 }}
      >
        <View>
          <Text style={styles.headerText}>Highlights</Text>
          <Text style={styles.headingText}>Lesson language(s)</Text>
          <View style={[styles.listContainer,{marginTop:10}]}>
          {languageList.map((item) => {
            return (
              <View style={styles.itemContainer}>
                <Text style={styles.itemText}>{item.name}</Text>
              </View>
            )
          })
          }
          </View>
          <Text style={styles.headingText}>Field</Text>
          <View style={styles.listContainer}>
          {fieldList.map((item) => {
            return (
              <View style={styles.itemContainer}>
                <Text style={styles.itemText}>{item.name}</Text>
              </View>
            )
          })
          }
          </View>
          <Text style={styles.headingText}>Teaches</Text>
          <View style={styles.listContainer}>
          {teachesList.map((item) => {
            return (
              <View style={styles.itemContainer}>
                <Text style={styles.itemText}>{item.name}</Text>
              </View>
            )
          })
          }
          </View>
          <Text style={styles.headingText}>Levels</Text>
          <View style={styles.listContainer}>
          {levelList.map((item) => {
            return (
              <View style={styles.itemContainer}>
                <Text style={styles.itemText}>{item.name}</Text>
              </View>
            )
          })
          }
          </View>
          <Text style={styles.headingText}>Students age teachable</Text>
          <View style={styles.listContainer}>
          {ageList.map((item) => {
            return (
              <View style={styles.itemContainer}>
                <Text style={styles.itemText}>{item.name}</Text>
              </View>
            )
          })
          }
          </View>
        </View>
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    paddingLeft: 22,
    paddingRight: 22
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    // lineHeight: 36,
  },
  headingText: {
    fontFamily: fonts.Bold,
    fontSize: 12,
    color: colors.dgColor,
    marginTop: 20
  },
  listContainer:{
    flexDirection:'row',
    flexWrap:'wrap'
  },
  itemContainer: {
    marginTop: 10,
    padding: 8,
    paddingLeft: 27,
    paddingRight: 27,
    borderRadius: 10,
    marginRight:10,
    backgroundColor: '#F4F4F4',
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemText: {
    fontFamily: fonts.Regular,
    fontSize: 12
  }
});

export default HighlightComponent;
