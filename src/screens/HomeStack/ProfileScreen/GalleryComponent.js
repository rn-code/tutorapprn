import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'


const GalleryComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [galleryList, setGalleryList] = useState([{}, {}, {},{},{},{},{},{},{}]);

  const renderGalleryItem = ({ item, index }) => {
    return (
      <View style={styles.itemMainContainer}>
        <Image 
          source={icons.dumyIcon}
          style={styles.galleryImage}
        />
      </View>
    );
  };
  const renderEmptyContainer = () => {
    return (
      <View style={[styles.emptyContainer]}>
        <Text style={styles.emptyText}>{'No item exist'}</Text>
      </View>
    )
  };
  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 30 ,marginBottom:10}}
        contentContainerStyle={{ flexGrow: 1 }}>
        <Text style={styles.headerText}>Gallery</Text>

        <FlatList
          numColumns={3}
          data={galleryList}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{flex:1}}
          renderItem={renderGalleryItem}
          ListEmptyComponent={renderEmptyContainer}
          showsVerticalScrollIndicator={false}
        />
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    paddingLeft: 22,
    paddingRight: 22
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    lineHeight: 36,
  },
  emptyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    width:'33%',
    height:132,
    marginTop: 10,
    paddingLeft:5,
    paddingRight:5
  },
  galleryImage:{
    width:'100%',
    height:132,
    borderRadius:10
  }
});

export default GalleryComponent;
