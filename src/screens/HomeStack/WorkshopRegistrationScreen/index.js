import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import icons from '../../../assets/icons'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";

const WorkshopRegistrationScreen = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [cvvNo, setCvvNo] = useState("");
    const [expiryDate, setExpiryDate] = useState("");
    const [cardNo, setCardNo] = useState("");
    const [cardName, setCardName] = useState("");

   

    const payRegisterBtn = () => {
    }
    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>Workshop registration</Text>
            </Pressable>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 30 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.fieldContainer}>
                    <Image
                        style={styles.circleLogo}
                        source={icons.backgroundHomeIcon}
                    />
                    <Text style={[
                        styles.mediumText,
                        { marginTop: 20 }]}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. A sed sit quis nibh velit, at ridiculus. Tellus sed aliquet lobortis odio facilisis id ipsum a.                    </Text>
                    <View style={styles.dateTextContainer}>
                        <Text style={styles.mediumText}>Date</Text>
                        <Text style={styles.dateText}>August 18, 2021 at 3:00 (PST)</Text>
                    </View>
                </View>
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Name on card'}
                    value={cardName}
                    onChangeText={(text) => {
                        setCardName(text)
                    }}
                />
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Card number'}
                    keyboardType={'number-pad'}
                    maxLength={16}
                    value={cardNo}
                    onChangeText={(text) => {
                        setCardNo(text)
                    }}
                />
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Expiration date'}
                    keyboardType={'number-pad'}
                    maxLength={8}
                    value={expiryDate}
                    onChangeText={(text) => {
                        setExpiryDate(text)
                    }}
                />
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'CVV'}
                    value={cvvNo}
                    keyboardType={'number-pad'}
                    maxLength={3}
                    onChangeText={(text) => {
                        setCvvNo(text)
                    }}
                />
                <View style={styles.buttonContainer}>
                <Button
                        text={'Cancel'}
                        textStyle={{ color: colors.lightRed }}
                        backgroundColorStyle={styles.loginBtnStyle}
                    />
                    <Button
                        text={'Pay & Register'}
                        // textStyle={{ }}
                        backgroundColorStyle={{ width: '62%' }}
                        clickAction={payRegisterBtn.bind(this)}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default WorkshopRegistrationScreen;
