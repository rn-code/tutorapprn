import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
    backgroundColor: colors.white
  },
  searchContainer: {
    width: '90%',
    flexDirection: 'row',
    borderRadius: 10,
    marginTop: 10,
    marginBottom: 30,
    height: 50,
    alignSelf: 'center',
    backgroundColor: colors.filterBtnColor,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,

    elevation: 2,
  },
  filterContainer: {
    width: '15%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  filterIcon: {
    width: 21,
    height: 15,
    resizeMode: 'contain'
  },
  mainView: {
    width: '100%',
    height: '100%',
    paddingLeft: 20,
    paddingRight: 20
  },

  itemMainContainer: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 30,
    backgroundColor: colors.white,
  },
  rightContainer: {
    width: '80%',
    paddingLeft: 10,
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  personImage: {
    width: '20%',
    // height:90,
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 10
  },
  inputSearchContainer: {
    backgroundColor: colors.white,
    width: '100%',
    marginTop: 0,
    marginLeft: 0,
    marginRight: 0,
    borderRadius: 0,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10
  },
  detailBtnStyle: {
    marginTop: 10,
    borderRadius: 10,
    height: 32,
    shadowOpacity: 0,
    backgroundColor: colors.lightGreenOpacity,
    width: '100%',
    alignSelf: 'center',
  },
  btnText: {
    color: colors.lightGreen,
    fontFamily: fonts.Bold,
    fontSize: 12
  },
  subRightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%'
  },
  personNameStyle: {
    fontSize: 18,
    fontFamily: fonts.Bold,
    color: colors.black
  },
  priceStyle: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black
  },
  featureStyle: {
    fontFamily: fonts.Medium,
    fontSize: 12,
    color: colors.lightOrange
  },
  ratingContainer: {
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center'
  },
  ratingCountStyle: {
    marginLeft: 5,
    fontSize: 12,
    fontFamily: fonts.Regular,
    color: colors.dgColor
  },
  onlineStyle: {
    width: 8,
    height: 8,
    backgroundColor: colors.brightGreen,
    position: 'absolute',
    top:7,
    right:'82%',
    borderRadius:2,
    borderWidth:1,
    borderColor:colors.white
  }
});
