import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
} from "react-native";
import moment from 'moment'
import { Rating } from 'react-native-ratings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useSelector } from 'react-redux'

import InputField from '../../../components/InputField'
import Header from "../../../components/Header";
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import Button from "../../../components/Button"
import colors from "../../../utils/colors";
import ViewDetailComponent from './ViewDetailComponent'
import ChooseStudentModalComponent from './ChooseStudentModalComponent'
import ChooseLocationModalComponent from './ChooseLocationModalComponent'
import { styles } from "./Styles";


const { width, height } = Dimensions.get('window')



const SearchScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('');
    const [viewDetail, setViewDetail] = useState(false);
    const [viewStudentModal, setStudentModal] = useState(false);
    const [viewLocationModal, setLocationModal] = useState(false);
    const [ratingSelByUser, setratingSelByUser] = useState(5);
    const [searchList, setSearchList] = useState([
        {}, {}, {}
    ])

    useEffect(() => {

    }, [])
    const ratingCompleted = (rating) => {
        console.log("Rating is: " + rating)
        setratingSelByUser(rating)
    }
    const renderSearchItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                    if (index === 0) {
                        setViewDetail(true)
                    } else if (index === 1) {
                        setStudentModal(true)
                    } else if (index === 2) {
                        setLocationModal(true)
                    }
                }}
                style={[styles.itemMainContainer, { marginTop: index === 0 ? 0 : 30 }]}>
                <Image
                    source={icons.dumyBanerIcon}
                    style={styles.personImage}
                />
                <View style={styles.onlineStyle} />
                <View style={styles.rightContainer}>
                    <View style={styles.subRightContainer}>
                        <Text style={styles.personNameStyle}>Ali Bashir</Text>
                        <Text style={styles.priceStyle}>$8.5/hr</Text>
                    </View>
                    <View style={[styles.subRightContainer, { marginTop: 10 }]}>
                        <View style={styles.ratingContainer}>
                            <Rating
                                startingValue={5}
                                ratingCount={5}
                                showRating={false}
                                imageSize={17}
                                fractions={1}
                                readonly={true}
                                type={'custom'}
                                selectedColor={'#EFC282'}
                                ratingColor={'#EFC282'}
                                ratingBackgroundColor='#c8c7c8'
                                // starContainerStyle={{ backgroundColor: 'green', width: 30 }}
                                // ratingImage={Icons.crossImg}
                                onFinishRating={ratingCompleted}
                            />
                            <Text style={styles.ratingCountStyle}>(77)</Text>
                        </View>
                        <Text style={styles.featureStyle}>Featured</Text>
                    </View>
                    <Button
                        text={'View detail'}
                        textStyle={styles.btnText}
                        backgroundColorStyle={styles.detailBtnStyle}
                    // clickAction={}
                    />
                </View>
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No item exist'}</Text>
            </View>
        )
    };
    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Search'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    // navigation.toggleDrawer();
                }}
            />
            <View style={styles.searchContainer}>
                <View style={{ width: '85%' }}>
                    <InputField
                        customStyle={styles.inputSearchContainer}
                        autoCapitalizes={"none"}
                        placeholder={'What you want to learn?'}
                        value={search}
                        onChangeText={(text) => {
                            setSearch(text)
                        }}
                        showLeftIcon={true}
                        customInputStyle={{ width: '89%' }}
                    />
                </View>
                <Pressable
                    onPress={() => {
                        navigation.navigate('FilterScreen')
                    }}
                    style={styles.filterContainer}>
                    <Image
                        style={styles.filterIcon}
                        source={icons.filterIcon}
                    />
                </Pressable>
            </View>

            <KeyboardAwareScrollView
                contentContainerStyle={{ flex: 1 }}
                showsVerticalScrollIndicator={false}
            >
                <View style={styles.mainView}>
                    <FlatList
                        data={searchList}
                        showsVerticalScrollIndicator={false}
                        // contentContainerStyle={{ }}
                        renderItem={renderSearchItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
            <ViewDetailComponent
                modalVisible={viewDetail}
                onCancel={() => {
                    setViewDetail(false)
                }}
            />
            <ChooseStudentModalComponent
                modalVisible={viewStudentModal}
                onCancel={() => {
                    setStudentModal(false)
                }}
            />
            <ChooseLocationModalComponent
                modalVisible={viewLocationModal}
                onCancel={() => {
                    setLocationModal(false)
                }}
            />
        </SafeAreaView>
    );
};

export default SearchScreen;
