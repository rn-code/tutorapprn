import React, { useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image, Modal, FlatList, Dimensions, Pressable } from 'react-native'

import fonts from '../../../assets/fonts'
import colors from '../../../utils/colors'
import icons from '../../../assets/icons'
import { color } from 'react-native-reanimated';

const { width, height } = Dimensions.get('window')
const ChooseLocationModalComponent = (props) => {
    const { modalVisible, onCancel } = props
    const [LocationList, setLocationList] = useState([
        { locationName: 'Online class',image:icons.learningIcon },
        { locationName: 'At tutor’s home',image:icons.locationIcon },
        { locationName: 'At your home' ,image:icons.homeIcon}
    ])

    const renderLocationItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                style={[styles.itemMainContainer, { marginTop: index === 0 ? 40 : 25 }]}>
                <View style={styles.leftContainer}>
                    <Image
                        source={item.image}
                        style={styles.LocationIcon}
                    />
                </View>
                <View style={styles.rightContainer}>
                    <Text style={styles.LoactionNameStyle}>{item.locationName}</Text>
                </View>
            </TouchableOpacity>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No item exist'}</Text>
            </View>
        )
    };

    return (
        <View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >

                <Pressable
                    onPress={() => {
                        onCancel()
                    }}
                    style={styles.modalTransparentContainer}>
                    <Pressable style={styles.mainContainer}>
                        <Text style={styles.headingStyle}>Choose class location</Text>
                        <Text style={styles.descriptionStyle}>Choose a venue for the class from these available</Text>
                        <View style={styles.mainView}>
                            <FlatList
                                data={LocationList}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{ marginTop: 10 ,marginBottom:40}}
                                renderItem={renderLocationItem}
                                ListEmptyComponent={renderEmptyContainer}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                    </Pressable>
                </Pressable>
            </Modal>
        </View>
    )
}
export default ChooseLocationModalComponent
const styles = StyleSheet.create({
    modalTransparentContainer: {
        backgroundColor: '#000000AA',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    mainContainer: {
        width: '100%',
        borderRadius: 10,
        padding: 20,
        paddingTop: 30,
        backgroundColor: colors.white,
    },
    headingStyle: {
        fontFamily: fonts.Bold,
        fontSize: 18,
        color: colors.black,
        alignSelf: 'center'
    },
    descriptionStyle: {
        fontSize: 12,
        fontFamily: fonts.Medium,
        marginTop: 10,
        color: colors.dgColor,
        textAlign: 'center'
    },
    emptyContainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyText: {
        textAlign: 'center',
        fontSize: 12,
        fontFamily: fonts.SemiBold,
        color: colors.dgColor
    },
    itemMainContainer: {
        width: '99%',
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 25,
        marginBottom:5,
        backgroundColor: colors.white,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,

    },
    leftContainer: {
        flexDirection: 'row',
        width:'20%',
        height:70,
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor:'#F1F8F8'
    },
    rightContainer: {
        width:'80%',
        height:70,
        alignItems: 'center',
        justifyContent:'center'
    },
    LocationIcon: {
        width: 38,
        height: 38,
        resizeMode: 'contain',
    },
    LoactionNameStyle: {
        fontSize: 12,
        fontFamily: fonts.Bold,
        color: colors.black
    },


});




