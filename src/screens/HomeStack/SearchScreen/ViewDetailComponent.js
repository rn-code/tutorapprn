import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image, Modal, FlatList, Dimensions, Pressable } from 'react-native'
import { Rating } from 'react-native-ratings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import fonts from '../../../assets/fonts'
import colors from '../../../utils/colors'
import icons from '../../../assets/icons/'
import Button from "../../../components/Button"

const { width, height } = Dimensions.get('window')
const ViewDetailComponent = (props) => {
    const { modalVisible, onCancel } = props


    const ratingCompleted = (rating) => {
        console.log("Rating is: " + rating)
    }
    return (
        <View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >
                <Pressable
                    onPress={() => {
                        onCancel()
                    }}
                    style={styles.modalTransparentContainer}>
                    <Pressable style={styles.mainContainer}>
                        <KeyboardAwareScrollView
                            contentContainerStyle={{ flex: 1,alignItems:'center' }}
                            showsVerticalScrollIndicator={false}
                        >
                            <View style={styles.profileImageContainer}>
                                <Image
                                    source={icons.dumyIcon}
                                    style={styles.personImage}
                                />
                                <View style={styles.barcontainer}>
                                    <Text style={styles.featureText}>Featured</Text>
                                </View>
                            </View>
                            <Text style={styles.personNameStyle}>Ali Bashir</Text>
                            <View style={styles.ratingContainer}>
                                <Rating
                                    startingValue={5}
                                    ratingCount={5}
                                    showRating={false}
                                    imageSize={19}
                                    fractions={1}
                                    readonly={true}
                                    type={'custom'}
                                    selectedColor={'#EFC282'}
                                    ratingColor={'#EFC282'}
                                    ratingBackgroundColor='#c8c7c8'
                                    onFinishRating={ratingCompleted}
                                />
                                <Text style={[styles.ratingCountStyle, { marginLeft: 5 }]}>(77)</Text>
                            </View>
                            <Text style={styles.ratingCountStyle}>77 lessons</Text>
                            <View style={styles.iconsMainContainer}>
                                <Image
                                    source={icons.learningIcon}
                                    style={styles.iconStyle}
                                />
                                <Image
                                    source={icons.locationIcon}
                                    style={[styles.iconStyle, {
                                        marginLeft: 20,
                                        marginRight: 20
                                    }]}
                                />
                                <Image
                                    source={icons.homeIcon}
                                    style={styles.iconStyle}
                                />
                            </View>
                            <View style={styles.textPairContainer}>
                                <View style={styles.subContainer}>
                                    <Text style={styles.ratingCountStyle}>Teaches</Text>
                                    <Text style={styles.blackText}>Python, C++, C</Text>
                                </View>
                                <View style={[styles.subContainer, { alignItems: 'flex-end' }]}>
                                    <Text style={styles.ratingCountStyle}>Language(s)</Text>
                                    <Text style={styles.blackText}>English</Text>
                                </View>
                            </View>
                            <View style={[styles.textPairContainer, { marginTop: 30 }]}>
                                <View style={styles.subContainer}>
                                    <Text style={styles.ratingCountStyle}>Levels</Text>
                                    <Text style={styles.blackText}>Secondary, GCSE, B Tech, A Level, Undergraduate</Text>
                                </View>
                                <View style={[styles.subContainer, { alignItems: 'flex-end' }]}>
                                    <Text style={styles.ratingCountStyle}>Age teachable</Text>
                                    <Text style={styles.blackText}>15 - 22</Text>
                                </View>
                            </View>
                            <Text style={styles.priceStyle}>$8.5/hr</Text>
                            <Button
                                text={'Schedule a lesson'}
                                textStyle={styles.btnText}
                                backgroundColorStyle={styles.detailBtnStyle}
                            // clickAction={}
                            />
                            <TouchableOpacity>
                                <Text style={styles.vistTextStyle}>Visit profile</Text>
                            </TouchableOpacity>
                        </KeyboardAwareScrollView>
                    </Pressable>
                </Pressable>
            </Modal>
        </View>
    )
}
export default ViewDetailComponent
const styles = StyleSheet.create({
    modalTransparentContainer: {
        backgroundColor: '#000000AA',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    mainContainer: {
        width: '100%',
        height: '80%',
        borderRadius: 10,
        backgroundColor: colors.white,
        alignItems: 'center',
        padding: 20
    },
    profileImageContainer: {
        width: 100,
        height: 100
    },
    personImage: {
        width: 100,
        height: 100,
        resizeMode: 'cover',
        borderRadius: 10
    },
    barcontainer: {
        backgroundColor: colors.lightOrange,
        width: '100%',
        padding: 3,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    featureText: {
        color: colors.white,
        fontFamily: fonts.Regular,
        fontSize: 12
    },
    personNameStyle: {
        fontSize: 18,
        fontFamily: fonts.Bold,
        color: colors.black,
        marginTop: 10
    },
    ratingContainer: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center'
    },
    ratingCountStyle: {
        fontSize: 12,
        fontFamily: fonts.Regular,
        color: colors.dgColor
    },
    blackText: {
        fontSize: 12,
        fontFamily: fonts.Regular,
        color: colors.black,
        marginTop: 5
    },
    iconsMainContainer: {
        flexDirection: 'row',
        marginTop: 10
    },
    iconStyle: {
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    textPairContainer: {
        flexDirection: 'row',
        width: '100%',
        marginTop: 30,
        justifyContent: 'space-between'
    },
    subContainer: {
        width: '50%'
    },
    priceStyle: {
        fontSize: 24,
        fontFamily: fonts.Bold,
        color: colors.black,
        marginTop: 50
    },
    detailBtnStyle: {
        marginTop: 50,
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: colors.lightGreen,
        width: '100%',
        alignSelf: 'center',
    },
    vistTextStyle: {
        fontSize: 12,
        fontFamily: fonts.Bold,
        color: colors.lightGreen,
        marginTop: 30
    }
});




