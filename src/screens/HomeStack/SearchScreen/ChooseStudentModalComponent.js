import React, { useState } from 'react';
import { View, StyleSheet,ScrollView, Text, TouchableOpacity, Image, Modal, FlatList, Dimensions, Pressable,TouchableWithoutFeedback } from 'react-native'

import Button from '../../../components/Button';
import fonts from '../../../assets/fonts'
import colors from '../../../utils/colors'
import icons from '../../../assets/icons'

const { width, height } = Dimensions.get('window')
const ChooseStudentModalComponent = (props) => {
    const { modalVisible, onCancel } = props
    const [studentList, setStudentList] = useState([
        {isSelected:false}, {isSelected:false}, {isSelected:false}
    ])
    const [parentList, setParentList] = useState([
        {isSelected:false}
    ])
    const renderStudentItem = ({ item, index }) => {
        return (
            <View
                style={[styles.itemMainContainer, {
                     marginTop: index === 0 ? 0 : 20 ,
                     borderBottomWidth: index + 1 === studentList.length ? 0 : 1
                     }]}>
                <View style={styles.leftContainer}>
                   <Image 
                    source={icons.dumyIcon}
                    style={styles.personIcon}
                   />
                    <View>
                        <Text style={styles.personNameStyle}>Ali Bashir</Text>
                        <Text style={styles.gradeTextStyle}>O level</Text>
                    </View>
                </View>
                <TouchableOpacity
                onPress={()=>{
                    var tempArray=[...studentList]
                    tempArray[index].isSelected= !item.isSelected
                    setStudentList(tempArray)
                }}
                >
                <Image 
                    source={item?.isSelected ? icons.activeIcon : icons.unactiveIcon }
                    style={styles.tickStyle}
                   />
                </TouchableOpacity>
            </View>
        );
    };
    const renderParentItem = ({ item, index }) => {
        return (
            <View
                style={[styles.itemMainContainer, {
                     marginTop: index === 0 ? 0 : 20 ,
                     borderBottomWidth: index + 1 === parentList.length ? 0 : 1
                     }]}>
                <View style={styles.leftContainer}>
                   <Image 
                    source={icons.dumyIcon}
                    style={styles.personIcon}
                   />
                    <View>
                        <Text style={styles.personNameStyle}>Ali Bashir</Text>
                        <Text style={styles.gradeTextStyle}>O level</Text>
                    </View>
                </View>
                <TouchableOpacity
                 onPress={()=>{
                    var tempArray=[...parentList]
                    tempArray[index].isSelected= !item.isSelected
                    setParentList(tempArray)
                }}>
                <Image 
                    source={index===0 ? icons.activeIcon : icons.unactiveIcon }
                    style={styles.tickStyle}
                   />
                </TouchableOpacity>
            </View>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No item exist'}</Text>
            </View>
        )
    };
    return (
        <View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >

                <Pressable
                    onPress={() => {
                        onCancel()
                    }}
                    style={styles.modalTransparentContainer}>
                    <Pressable style={styles.mainContainer}>
                        <Text style={styles.headingStyle}>Choose student</Text>
                        <Text style={styles.descriptionStyle}>Choose your child(ren) who is / are going to attend this lesson</Text>
                        <ScrollView
                        // contentContainerStyle={{backgroundColor:'red'}}
                        showsVerticalScrollIndicator={false}
                        >
                        <View>
                        <Text style={styles.listHeading}>Kid(s) / Student(s)</Text>
                        <View style={styles.mainView}>
                            <FlatList
                                data={studentList}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{ marginTop:10}}
                                renderItem={renderStudentItem}
                                ListEmptyComponent={renderEmptyContainer}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                        <Text style={styles.listHeading}>Parent</Text>
                        <View style={styles.mainView}>
                            <FlatList
                                data={parentList}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{ marginTop:10}}
                                renderItem={renderParentItem}
                                ListEmptyComponent={renderEmptyContainer}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                        <Button
                            text={'Done'}
                            textStyle={styles.btnText}
                            backgroundColorStyle={styles.doneBtnStyle}
                        // clickAction={}
                        />
                        </View>
                        </ScrollView>
                    </Pressable>
                </Pressable>
            </Modal>
        </View>
    )
}
export default ChooseStudentModalComponent
const styles = StyleSheet.create({
    modalTransparentContainer: {
        backgroundColor: '#000000AA',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    mainContainer: {
        width: '100%',
        height: '80%',
        borderRadius: 10,
        padding: 20,
        paddingTop: 30,
        backgroundColor: colors.white,
    },
    headingStyle: {
        fontFamily: fonts.Bold,
        fontSize: 18,
        color: colors.black,
        alignSelf: 'center'
    },
    descriptionStyle: {
        fontSize: 12,
        fontFamily: fonts.Medium,
        marginTop: 10,
        color: colors.dgColor,
        textAlign: 'center'
    },
    listHeading: {
        fontFamily: fonts.Regular,
        fontSize: 12,
        color: colors.black,
        marginTop: 30
    },
    emptyContainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyText: {
        textAlign: 'center',
        fontSize: 12,
        fontFamily: fonts.SemiBold,
        color: colors.dgColor
    },
    itemMainContainer: {
        width: '100%',
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent:'space-between',
        alignItems:'center',
        marginTop: 30,
        backgroundColor: colors.white,
        borderBottomWidth:1,
        paddingBottom:20,
        borderColor:colors.dgColor
    },
    leftContainer: {
        flexDirection: 'row',
        alignItems:'center'
    },
    personIcon:{
        width:41,
        height:41,
        borderRadius:10,
        resizeMode:'contain',
        marginRight:15
    },
    personNameStyle:{
        fontSize:12,
        fontFamily:fonts.Bold,
        color:colors.black
    },
    gradeTextStyle:{
        fontSize:12,
        marginTop:5,
        fontFamily:fonts.Medium,
        color:colors.dgColor
    },
    tickStyle:{
        width:24,
        height:24,
        resizeMode:'contain'
    },
    doneBtnStyle:{
        backgroundColor:colors.lightGreen,
        shadowOpacity:0,
        marginTop:40
    }
});




