import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    FlatList,
    ImageBackground,
    StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { SafeAreaView } from 'react-native-safe-area-context'

import { styles } from './Styles'
import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'


const WorkshopDetailScreen = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [galleryList, setGalleryList] = useState([{}, {}, {}, {}, {}, {}]);

    const renderGalleryItem = ({ item, index }) => {
        return (
            <View style={styles.itemMainContainer}>
                <Image
                    source={icons.dumyIcon}
                    style={styles.galleryImage}
                />
            </View>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No item exist'}</Text>
            </View>
        )
    };
    return (
        <ImageBackground
            style={styles.imageBackgroundStyle}
            source={icons.backgroundHomeIcon}
        >
            <SafeAreaView style={styles.safeStyle}>
                <Pressable
                    onPress={() => {
                        navigation.goBack()
                    }}
                    style={styles.backBtnContainer}>
                    <Image
                        style={styles.backImage}
                        source={icons.backIcon}
                    />
                    <Text style={styles.headerText}>Home</Text>
                </Pressable>
                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    style={{
                        paddingLeft: 22,
                        paddingRight: 22
                    }}
                    contentContainerStyle={{ flexGrow: 1 }}>
                    <Button
                        backgroundColorStyle={styles.organizedBtnStyle}
                        text={'Organized by Lorem Ipsum'}
                        textStyle={{ color: colors.lightGreen }}
                    />
                    <Text style={styles.headingText}>Workshop on single line title goes here.</Text>
                    <Text style={styles.descriptionText}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Adipiscing habitasse tempor, pulvinar aliquam accumsan convallis imperdiet vestibulum, quis. Quisque in id porttitor quis. In at aliquam purus pellentesque arcu. Tincidunt donec orci ante tellus aliquam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Adipiscing habitasse tempor, pulvinar aliquam accumsan convallis imperdiet vestibulum, quis. Quisque in id porttitor quis. In at aliquam purus pellentesque arcu. Tincidunt donec orci ante tellus aliquam augue.
                    </Text>
                    <View style={styles.rowContainer}>
                        <Text style={styles.rowHeadingText}>Date</Text>
                        <Text style={styles.rowDescriptionText}>August 18, 2021 at 3:00 (PST)</Text>
                    </View>
                    <View style={styles.rowContainer}>
                        <Text style={styles.rowHeadingText}>Seats left</Text>
                        <Text style={styles.rowDescriptionText}>13</Text>
                    </View>
                    <View style={styles.rowContainer}>
                        <Text style={styles.rowHeadingText}>Location</Text>
                        <Text style={styles.rowDescriptionText}>Lorem ipsum dolor sit amet.</Text>
                    </View>
                    <View style={styles.rowContainer}>
                        <Text style={styles.rowHeadingText}>Price</Text>
                        <Text style={styles.rowDescriptionText}>$20</Text>
                    </View>
                    <Text style={[styles.descriptionText,{marginTop:20}]}>Gallery</Text>
                    <FlatList
                        numColumns={3}
                        data={galleryList}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ flex: 1 }}
                        renderItem={renderGalleryItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                    <Button
                        clickAction={()=>{
                             navigation.navigate('WorkshopRegistrationScreen')
                        }}
                        text={'Proceed to reservation'}
                        backgroundColorStyle={{ marginTop: 30,marginBottom:30 }}
                    />
                </KeyboardAwareScrollView>
                <Loader loading={loading} />
            </SafeAreaView>
        </ImageBackground>
    );
};


export default WorkshopDetailScreen;
