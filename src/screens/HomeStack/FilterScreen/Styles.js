import { StyleSheet } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'
export const styles = StyleSheet.create({
  safeStyle: {
    width:'100%',
    height:'100%',
    paddingLeft:20,
    paddingRight:20
  },
  headerText:{
    fontFamily:fonts.Bold,
    fontSize:24,
    lineHeight:36,
  },
  backBtnContainer:{
    flexDirection:'row',
    marginTop:15,
    alignItems:'center'
  },
  fieldContainer:{
    marginBottom:30
  },
  buttonContainer:{
    flexDirection:'row',
    marginTop:50,
    alignItems:'center',
    justifyContent:'space-between',
    marginBottom:30
  },

backImage:{
  width:19,
  height:19,
  resizeMode:'contain',
  marginRight:13
},
dropDownStyle:{
  width:10,
  height:6,
  resizeMode:'contain',
  right:18,
  position:'absolute'
},
pickerContainer:{
  height: 50,
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '98%',
  alignSelf:'center',
  backgroundColor: colors.white,
  borderRadius: 10,
  marginTop: 10,
  shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 1,
},
shadowOpacity: 0.20,
shadowRadius: 1.41,

elevation: 2
},
clearText:{
  fontFamily:fonts.Bold,
  fontSize:12,
  color:colors.lightOrange
},
headingText:{
  fontSize:12,
  fontFamily:fonts.Regular,
  color:colors.black,
  marginTop:36
},
sliderContainer:{
  width:'99%',
  alignSelf:'center',
  alignItems:'center',
  backgroundColor:colors.white,
  borderRadius:10,
  marginTop:10,
  paddingTop:5,
  paddingBottom:5,
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.20,
  shadowRadius: 1.41,
  elevation: 2
},
termsContainer: {
  flexDirection: 'row',
  marginTop:30,
  alignItems: 'center',
},
inputSearchContainer:{
  backgroundColor: colors.white,
  width: '99%',
  // marginTop: 0,
  // marginLeft: 0,
  // marginRight:0,
  // borderRadius: 0,
  // borderTopLeftRadius: 10,
  // borderBottomLeftRadius: 10,
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.20,
  shadowRadius: 1.41,
  elevation: 2,
  alignSelf:'center'
},
dashStyle:{
  fontSize:16,
  fontFamily:fonts.Regular,
  marginLeft:20,
  marginRight:20
},
priceRangeContainer:{
  flexDirection:'row',
  alignItems:'center',
  marginTop:10
},
valueContainer:{
  width:70,
  height:50,
  borderRadius:7,
  backgroundColor:'#F4F5FC',
  alignItems:'center',
  justifyContent:'center'
}
});
