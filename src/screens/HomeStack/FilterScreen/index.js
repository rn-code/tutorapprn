import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    Platform,
    TouchableOpacity,
    Dimensions,
    FlatList,
    TextInput
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Picker } from 'native-base';
import MultiSlider from '@ptomasroos/react-native-multi-slider'
import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import icons from '../../../assets/icons'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')
const FilterScreen = (props) => {

    const { navigation, route } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState("");
    const [age, setAge] = useState("");
    const [selectedClassLocation, setSelectedClassLocation] = useState('Any')
    const [selectedTutorLanguage, setSelectedTutorLanguage] = useState('Any')
    const [selectedParentField, setSetlectedParentField] = useState('Any')
    const [selectedSubCategory, setSelectedSubCategory] = useState('Any')
    const [selectedGrade, setSelectedGrade] = useState('Any')
    const [selectedLevel, setSelectedLevel] = useState('Any')
    const [selectedGender, setSelectedGender] = useState('Enter gender')
    const [selecteLocation, setSelecteLocation] = useState('Any')
    const [pickerList, setPickerList] = useState([])
    const [multiSliderValue, setMultiSliderValue] = useState([0, 100])
    const multiSliderValuesChange = (values) => setMultiSliderValue(values)
    const [checkBox, setBox] = useState(false);
    const [checkBoxOnline, setBoxOnline] = useState(false);
    
    const applyFilterBtn = () => {
        navigation.goBack()
    }
    const clearFilterBtn = () => {
        setSearch('')
        setAge('')
        setBox(false)
        setBoxOnline(false)
        setMultiSliderValue([0, 100])
        setSelectedClassLocation('Any')
        setSelectedTutorLanguage('Any')
        setSetlectedParentField('Any')
        setSelectedSubCategory('Any')
        setSelectedGrade('Any')
        setSelectedLevel('Any')
        setSelectedGender('Enter gender')
        setSelecteLocation('Any')
    }
    const onClassLocationChange = (value: string) => {
        setSelectedClassLocation(value)
    }
    const onVTutorLanguageChange = (value: string) => {
        setSelectedTutorLanguage(value)
    }
    const onParentFieldChange = (value: string) => {
        setSetlectedParentField(value)
    }
    const onSubCategoryChange = (value: string) => {
        setSelectedSubCategory(value)
    }
    const onGradeChange = (value: string) => {
        setSelectedGrade(value)
    }
    const onLevelChange = (value: string) => {
        setSelectedLevel(value)
    }
    const onGenderChange = (value: string) => {
        setSelectedGender(value)
    }
    const onLocationChange = (value: string) => {
        setSelecteLocation(value)
    }

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>Filter</Text>
            </Pressable>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 0 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.fieldContainer}>
                    <Text style={styles.headingText}>I want to learn</Text>
                    <InputField
                        customStyle={styles.inputSearchContainer}
                        autoCapitalizes={"none"}
                        placeholder={'Find a skill'}
                        value={search}
                        onChangeText={(text) => {
                            setSearch(text)
                        }}
                        showLeftIcon={true}
                        customInputStyle={{ width: '89%' }}
                    />
                    <Text style={styles.headingText}>Class location</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selectedClassLocation}
                            textStyle={{ color: colors.dgColor }}
                            onValueChange={onClassLocationChange}
                        >
                            <Picker.Item label="Any" value="key0" />
                            <Picker.Item label="Canada" value="key1" />
                            <Picker.Item label="America" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.headingText}>Tutor language</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selectedTutorLanguage}
                            textStyle={{ color: colors.dgColor }}
                            onValueChange={onVTutorLanguageChange}
                        >
                            <Picker.Item label="Any" value="key0" />
                            <Picker.Item label="English" value="key1" />
                            <Picker.Item label="Urdu" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.headingText}>Price range</Text>
                    <View style={styles.priceRangeContainer}>
                        <View style={styles.valueContainer}>
                            <Text>{multiSliderValue[0]}</Text>
                        </View>
                        <Text style={styles.dashStyle}>-</Text>
                        <View style={styles.valueContainer}>
                            <Text>{multiSliderValue[1]}</Text>
                        </View>
                    </View>
                    <View style={styles.sliderContainer}>
                        <MultiSlider
                            markerStyle={{
                                ...Platform.select({
                                    ios: {
                                        height: 30,
                                        width: 30,
                                        shadowColor: '#000000',
                                        shadowOffset: {
                                            width: 0,
                                            height: 3
                                        },
                                        shadowRadius: 1,
                                        shadowOpacity: 0.1
                                    },
                                    android: {
                                        height: 22,
                                        width: 22,
                                        borderRadius: 50,
                                        backgroundColor: '#1792E8'
                                    }
                                })
                            }}
                            pressedMarkerStyle={{
                                ...Platform.select({
                                    android: {
                                        height: 25,
                                        width: 25,
                                        borderRadius: 15,
                                        backgroundColor: '#148ADC'
                                    }
                                })
                            }}
                            selectedStyle={{
                                backgroundColor: '#1792E8'
                            }}
                            trackStyle={{
                                backgroundColor: '#CECECE'
                            }}
                            touchDimensions={{
                                height: 40,
                                width: 40,
                                borderRadius: 20,
                                slipDisplacement: 40
                            }}
                            values={[multiSliderValue[0], multiSliderValue[1]]}
                            sliderLength={230}
                            onValuesChange={multiSliderValuesChange}
                            min={0}
                            max={300}
                            allowOverlap={false}
                            minMarkerOverlapDistance={10}
                        />
                    </View>
                    <Text style={styles.headingText}>Field / Parent category</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selectedParentField}
                            textStyle={{ color: colors.dgColor }}
                            onValueChange={onParentFieldChange}
                        >
                            <Picker.Item label="Any" value="key0" />
                            <Picker.Item label="Computer Science" value="key1" />
                            <Picker.Item label="Electrical Engineer" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.headingText}>Sub-category</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selectedSubCategory}
                            textStyle={{ color: colors.dgColor }}
                            onValueChange={onSubCategoryChange}
                        >
                            <Picker.Item label="Any" value="key0" />
                            <Picker.Item label="Python" value="key1" />
                            <Picker.Item label="Java" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.headingText}>Age of student</Text>
                    <InputField
                        customStyle={styles.inputSearchContainer}
                        autoCapitalizes={"none"}
                        placeholder={'0'}
                        maxLength={3}
                        keyboardType={'number-pad'}
                        value={age}
                        onChangeText={(text) => {
                            setAge(text)
                        }}
                        customInputStyle={{ width: '89%' }}
                    />
                    <Text style={styles.headingText}>Grade of student</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selectedGrade}
                            textStyle={{ color: colors.dgColor }}
                            onValueChange={onGradeChange}
                        >
                            <Picker.Item label="Any" value="key0" />
                            <Picker.Item label="O Level" value="key1" />
                            <Picker.Item label="A Level" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.headingText}>Level</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selectedLevel}
                            textStyle={{ color: colors.dgColor }}
                            onValueChange={onLevelChange}
                        >
                            <Picker.Item label="Any" value="key0" />
                            <Picker.Item label="Xyz" value="key1" />
                            <Picker.Item label="Zyx" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.headingText}>Tutor gender</Text>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selectedGender}
                            textStyle={{ color: colors.dgColor }}
                            onValueChange={onGenderChange}
                        >
                            <Picker.Item label="Any" value="key0" />
                            <Picker.Item label="Male" value="key1" />
                            <Picker.Item label="Female" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <Text style={styles.headingText}>Location</Text>

                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selecteLocation}
                            textStyle={{ color: colors.dgColor }}
                            onValueChange={onLocationChange}
                        >
                            <Picker.Item label="Any" value="key0" />
                            <Picker.Item label="Canada" value="key1" />
                            <Picker.Item label="America" value="key2" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <View style={styles.termsContainer}>
                        <CheckBox
                            onChange={() => { setBox(!checkBox) }}
                            onChangeTerm={() => {
                            }}
                            customTxt={'Near you (around 20 kms)'}
                            isChecked={checkBox}
                            tintColor={colors.black}
                            checkstyle={{ backgroundColor: colors.InputFieldBackground }}
                        />
                    </View>
                    <View style={[styles.termsContainer, { marginTop: 10 }]}>
                        <CheckBox
                            onChange={() => { setBoxOnline(!checkBoxOnline) }}
                            onChangeTerm={() => {
                            }}
                            customTxt={'Currently online'}
                            isChecked={checkBoxOnline}
                            tintColor={colors.black}
                            checkstyle={{ backgroundColor: colors.InputFieldBackground }}
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <Pressable
                        onPress={()=>{
                            clearFilterBtn()
                        }}
                        >
                            <Text
                                style={styles.clearText}>
                                Clear all
                            </Text>
                        </Pressable>
                        <Button
                            text={'Apply'}
                            // textStyle={{ }}
                            backgroundColorStyle={{ width: '80%' }}
                            clickAction={applyFilterBtn.bind(this)}
                        />
                    </View>
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default FilterScreen;
