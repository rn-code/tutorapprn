import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'
import CheckBox from '../../../components/CustomCheckBox'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts';
const PaymentComponent = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [cardName, setCardName] = useState("");
    const [cardNumber, setCardNumber] = useState("");
    const [expirationDate, setExpirationDate] = useState("");
    const [cvv, setCvv] = useState("");

    const updatePasswordBtn = async () => {
       
    };
    return (
        <View style={styles.safeStyle}>
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'Name on card'}
                placeholder={'Name on card'}
                value={cardName}
                onChangeText={(text) => {
                    setCardName(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'Card number'}
                placeholder={'Card number'}
                keyboardType={'number-pad'}
                maxLength={16}
                value={cardNumber}
                onChangeText={(text) => {
                    setCardNumber(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
                mainStyle={{marginTop:10}}

            />
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'Expiration date'}
                placeholder={'Expiration date'}
                value={expirationDate}
                onChangeText={(text) => {
                    setExpirationDate(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
                mainStyle={{marginTop:10}}

            />
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'CVV'}
                placeholder={'CVV'}
                maxLength={3}
                keyboardType={'number-pad'}
                value={cvv}
                onChangeText={(text) => {
                    setCvv(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
                mainStyle={{marginTop:10}}

            />
      
            <View style={styles.buttonContainer}>
                <Button
                    text={'Save'}
                    // textStyle={{ }}
                    backgroundColorStyle={styles.saveBtn}
                    clickAction={updatePasswordBtn.bind(this)}
                />
            </View>
            <Loader loading={loading} />
        </View>
    );
};
const styles = StyleSheet.create({
    safeStyle: {
    },
    fieldContainer: {
        marginBottom: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop:30
    },
    saveBtn: {
        backgroundColor: colors.lightGreen,
        height: 44,
        width: 80,
        borderRadius: 8,
        marginTop: 10,
        shadowOpacity: 0
    },
    inputText:{
        fontSize:12,
        fontFamily:fonts.Medium
    },
    inputBackground:{
        backgroundColor:colors.white,
        borderWidth:.8,
        borderRadius:8,
        borderColor:colors.dgColor,
        paddingLeft:20
    },
   
});

export default PaymentComponent;
