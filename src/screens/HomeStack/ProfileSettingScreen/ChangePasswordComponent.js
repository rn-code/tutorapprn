import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'
import CheckBox from '../../../components/CustomCheckBox'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts';
const ChangePasswordComponent = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const updatePasswordBtn = async () => {
        if (password === "") {
            showErrorMsg("Password is required");
        } else if (password.length < 6) {
            showErrorMsg("Password must be atleast 6 character");
        } else {
        }
    };
    return (
        <View style={styles.safeStyle}>
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'Old password'}
                placeholder={'Old password'}
                value={oldPassword}
                onChangeText={(text) => {
                    setOldPassword(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'New password'}
                placeholder={'New password'}
                value={newPassword}
                onChangeText={(text) => {
                    setNewPassword(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
                mainStyle={{marginTop:10}}
            />
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'Confirm new password'}
                placeholder={'Confirm new password'}
                value={confirmPassword}
                onChangeText={(text) => {
                    setConfirmPassword(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
                mainStyle={{marginTop:10}}

            />
          
            <View style={styles.buttonContainer}>
                <Button
                    text={'Save'}
                    // textStyle={{ }}
                    backgroundColorStyle={styles.saveBtn}
                    clickAction={updatePasswordBtn.bind(this)}
                />
            </View>
            <Loader loading={loading} />
        </View>
    );
};
const styles = StyleSheet.create({
    safeStyle: {
    },
    fieldContainer: {
        marginBottom: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop:30
    },
    saveBtn: {
        backgroundColor: colors.lightGreen,
        height: 44,
        width: 80,
        borderRadius: 8,
        marginTop: 10,
        shadowOpacity: 0
    },
    inputText:{
        fontSize:12,
        fontFamily:fonts.Medium
    },
    inputBackground:{
        backgroundColor:colors.white,
        borderWidth:.8,
        borderRadius:8,
        borderColor:colors.dgColor,
        paddingLeft:20
    },
   
});

export default ChangePasswordComponent;
