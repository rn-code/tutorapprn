import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'
import CheckBox from '../../../components/CustomCheckBox'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts';
const UpdateSettingComponent = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [checkBox, setBox] = useState(false);
    const [feedbackCheck, setFeedbackCheck] = useState(false);
    const [reminderCheck, setReminderCheck] = useState(false);
    const [newsCheck, setNewsCheck] = useState(false);
    const [messageCheck, setMessageCheck] = useState(false);

    const updatePasswordBtn = async () => {

    };
    return (
        <View style={styles.safeStyle}>

            <View style={styles.termsContainer}>
                <Text style={styles.headingStyle}>Email settings</Text>
                <Text style={styles.descriptionStyle}>
                    Be careful when stopping these settings. You might miss your classes.
                </Text>
                <CheckBox
                    isCircle={true}
                    onChange={() => { setBox(!checkBox) }}
                    onChangeTerm={() => {
                    }}
                    isChecked={checkBox}
                    tintColor={colors.black}
                    customTxt={'Weekly summary emails'}
                    checkstyle={{ borderRadius: 20 }}
                    checkedColor={colors.lightGreen}
                    uncheckedColor={colors.white}
                />
                <CheckBox
                    isCircle={true}
                    onChange={() => { setFeedbackCheck(!feedbackCheck) }}
                    onChangeTerm={() => {
                    }}
                    isChecked={feedbackCheck}
                    tintColor={colors.black}
                    customTxt={'Feedback emails'}
                    checkstyle={{ borderRadius: 20 }}
                    checkedColor={colors.lightGreen}
                    uncheckedColor={colors.white}
                />
                <CheckBox
                    isCircle={true}
                    onChange={() => { setReminderCheck(!reminderCheck) }}
                    onChangeTerm={() => {
                    }}
                    isChecked={reminderCheck}
                    tintColor={colors.black}
                    customTxt={'Reminder emails'}
                    checkstyle={{ borderRadius: 20 }}
                    checkedColor={colors.lightGreen}
                    uncheckedColor={colors.white}
                />
                <CheckBox
                    isCircle={true}
                    onChange={() => { setNewsCheck(!newsCheck) }}
                    onChangeTerm={() => {
                    }}
                    isChecked={newsCheck}
                    tintColor={colors.black}
                    customTxt={'News emails'}
                    checkstyle={{ borderRadius: 20 }}
                    checkedColor={colors.lightGreen}
                    uncheckedColor={colors.white}
                />
                <CheckBox
                    isCircle={true}
                    onChange={() => { setMessageCheck(!messageCheck) }}
                    onChangeTerm={() => {
                    }}
                    isChecked={messageCheck}
                    tintColor={colors.black}
                    customTxt={'New message emails'}
                    checkstyle={{ borderRadius: 20 }}
                    checkedColor={colors.lightGreen}
                    uncheckedColor={colors.white}
                />
            </View>
            <View style={styles.buttonContainer}>
                <Button
                    text={'Update Settings'}
                    // textStyle={{ }}
                    backgroundColorStyle={styles.saveBtn}
                    clickAction={updatePasswordBtn.bind(this)}
                />
            </View>
            <Loader loading={loading} />
        </View>
    );
};
const styles = StyleSheet.create({
    safeStyle: {
    },
    fieldContainer: {
        marginBottom: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop: 30
    },
    saveBtn: {
        backgroundColor: colors.lightGreen,
        height: 44,
        width: '50%',
        borderRadius: 8,
        marginTop: 10,
        shadowOpacity: 0
    },
    inputText: {
        fontSize: 12,
        fontFamily: fonts.Medium
    },
    inputBackground: {
        backgroundColor: colors.white,
        borderWidth: .8,
        borderRadius: 8,
        borderColor: colors.dgColor,
        paddingLeft: 20
    },
    termsContainer: {
        marginTop: 20,
        // backgroundColor:'red',
    },
    descriptionStyle: {
        fontSize: 12,
        fontFamily: fonts.Medium,
        color: colors.chatMessage,
        marginBottom: 20,
        marginTop: 10
    },
    headingStyle: {
        fontSize: 12,
        fontFamily: fonts.Medium,
        color: colors.black,
    }
});

export default UpdateSettingComponent;
