import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    FlatList,
    TouchableOpacity,
    StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'
import CheckBox from '../../../components/CustomCheckBox'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts';
const KidComponent = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [kidsList, setKidsList] = useState([
        {name:"Ali "},
        {name:"Adnan"},
    ]);

    const updatePasswordBtn = async () => {
    };
    const renderKidItem = ({ item, index }) => {
        return (
            <View style={styles.itemMainContainer}>
                <Text style={styles.nameText}>{item.name}</Text>
                <View style={styles.mainBtnContainer}>
                    <TouchableOpacity
                        onPress={() => {
                         
                        }}
                    >
                        <Image
                            source={icons.editIcon}
                            style={[styles.imageItem, { marginLeft: 30 }]}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                       
                        }}
                    >
                        <Image
                            source={icons.deleteIcon}
                            style={styles.imageItem}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>No Kid added yet</Text>
            </View>
        )
    };
    return (
        <View style={styles.safeStyle}>
            <FlatList
                horizontal
                data={kidsList}
                showsVerticalScrollIndicator={false}
                // contentContainerStyle={{flex:1 }}
                renderItem={renderKidItem}
                ListEmptyComponent={renderEmptyContainer}
                showsHorizontalScrollIndicator={false}
            />
            <View style={styles.buttonContainer}>
                <Button
                    text={'+ Add new kids'}
                    // textStyle={{ }}
                    backgroundColorStyle={styles.saveBtn}
                    clickAction={updatePasswordBtn.bind(this)}
                />
            </View>
            <Loader loading={loading} />
        </View>
    );
};
const styles = StyleSheet.create({
    safeStyle: {
    },
    fieldContainer: {
        marginBottom: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
    },
    saveBtn: {
        backgroundColor: colors.lightGreen,
        height: 44,
        width: '40%',
        borderRadius: 8,
        marginTop: 10,
        shadowOpacity: 0
    },
    inputText: {
        fontSize: 12,
        fontFamily: fonts.Medium
    },
    inputBackground: {
        backgroundColor: colors.white,
        borderWidth: .8,
        borderRadius: 8,
        borderColor: colors.dgColor,
        paddingLeft: 20
    },
    termsContainer: {
        flexDirection: 'row',
        marginTop: 30,
        alignItems: 'center',
    },



    emptyText:{
        fontFamily:fonts.Bold,
        fontSize:14,
        color:colors.lightOrange
      },
      emptyContainer:{
        borderRadius:10,
        backgroundColor:colors.kidItemBackground,
        padding:20,
        paddingTop:15,
        paddingBottom:15,
        marginTop:10
      },
      itemMainContainer:{
        flexDirection:'row',
        backgroundColor:colors.kidItemBackground,
        padding:20,
        paddingTop:15,
        paddingBottom:15,
        borderRadius:10,
        marginRight:10,
        marginTop:10
      },
      nameText:{
        fontFamily:fonts.Bold,
        fontSize:14,
        color:colors.lightOrange
      },
      mainBtnContainer:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
      },
      imageItem:{
        width:18,
        height:18,
        resizeMode:'contain',
        marginLeft:15
      }


});

export default KidComponent;
