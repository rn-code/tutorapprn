import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import icons from '../../../assets/icons'
import CheckBox from '../../../components/CustomCheckBox'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts';
const PersonalInfoComponent = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [name, setName] = useState("");
    const [lastName, setLastName] = useState("");
    const [location, setLocation] = useState("");
    const [checkBox, setBox] = useState(false);

    const updatePasswordBtn = async () => {
        if (password === "") {
            showErrorMsg("Password is required");
        } else if (password.length < 6) {
            showErrorMsg("Password must be atleast 6 character");
        } else {
            navigation.navigate('LoginScreen')
        }
    };
    return (
        <View style={styles.safeStyle}>
                        <Text style={styles.descriptionStyles}>This won’t be a part of your public profile.</Text>
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'Email'}
                placeholder={'Email'}
                value={name}
                onChangeText={(text) => {
                    setName(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
            />
                        <Text style={[styles.descriptionStyles,{color:colors.lightGreen}]}>Email settings</Text>
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'Phone number'}
                placeholder={'Phone number'}
                value={lastName}
                onChangeText={(text) => {
                    setLastName(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
                mainStyle={{marginTop:10}}

            />
            <Text style={styles.descriptionStyles}>This is only used by tutorservices when we need to contact you.</Text>
            <InputField
                autoCapitalizes={"none"}
                inputHeading ={'Location'}
                placeholder={'Location'}
                value={location}
                onChangeText={(text) => {
                    setLocation(text)
                }}
                customInputStyle={styles.inputText}
                customStyle={styles.inputBackground}
                mainStyle={{marginTop:10}}

            />
            <View style={styles.buttonContainer}>
                <Button
                    text={'Save'}
                    // textStyle={{ }}
                    backgroundColorStyle={styles.saveBtn}
                    clickAction={updatePasswordBtn.bind(this)}
                />
            </View>
            <Loader loading={loading} />
        </View>
    );
};
const styles = StyleSheet.create({
    safeStyle: {
    },
    fieldContainer: {
        marginBottom: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
    },
    saveBtn: {
        backgroundColor: colors.lightGreen,
        height: 44,
        width: 80,
        borderRadius: 8,
        marginTop: 30,
        shadowOpacity: 0
    },
    inputText:{
        fontSize:12,
        fontFamily:fonts.Medium
    },
    inputBackground:{
        backgroundColor:colors.white,
        borderWidth:.8,
        borderRadius:8,
        borderColor:colors.dgColor,
        paddingLeft:20
    },
    termsContainer: {
        flexDirection: 'row',
        marginTop: 30,
        alignItems: 'center',
      },
      descriptionStyles:{
          marginTop:10,
          fontSize:12,
          color:'#878787'
      }
});

export default PersonalInfoComponent;
