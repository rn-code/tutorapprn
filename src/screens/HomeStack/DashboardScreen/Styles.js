import { StyleSheet ,Dimensions} from 'react-native';
import colors from '../../../utils/colors'
import fonts from '../../../assets/fonts'
const {width,height}=Dimensions.get('window')

export const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    height: '100%',
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    lineHeight: 36,
    marginLeft:15
  },
  descriptionText: {
    fontSize: 12,
    fontFamily: fonts.Regular,
    color: colors.dgColor,
    marginTop: 7
  },
  dayDetail: {
    marginTop: 8,
    flexDirection: 'row'
  },
  profileHeader:{
    flexDirection:'row',
    alignItems:'center'
  },
  profileIcon:{
    borderRadius:15,
    width:50,
    height:50,
    resizeMode:'cover'
  },
  topBarcontainer:{
    flexDirection:'row',
    width:'60%',
    marginTop:10
  },
  activeStyle:{
    width:'50%',
    padding:13,
    backgroundColor:colors.lightGreen,
    borderWidth:.7,
    borderRightWidth:.7,
    borderColor:colors.green
  },
  unActiveStyle:{
    width:'50%',
    padding:13,
    borderWidth:.7,
    borderRightWidth:.7,
    borderColor:colors.dgColor
  },
  activeTextStyle:{
      fontFamily:fonts.Bold,
      fontSize:12,  
      textAlign:'center',
      color:colors.white
  },
  unActiveTextStyle:{
      fontFamily:fonts.Regular,
      fontSize:12,  
      textAlign:'center',
      color:colors.borderColor
  },
  upcomingText:{
    fontSize:12,
    fontFamily:fonts.Medium,
    color:colors.chatMessage,
  },
  weeklyText:{
    fontSize:12,
    fontFamily:fonts.SemiBold,
    color:colors.chatMessage,
    textAlign:'center',
  },
  weeklyBorderText:{
    fontSize:12,
    fontFamily:fonts.SemiBold,
    color:colors.lightGreen,
    textAlign:'center',
  },
  calendarAdjustment:{
    width:width-10,
    alignSelf:'center'
  },
  upcomingViewContainer:{
    width:'50%',
    // height:41,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    borderRadius:10,
    borderWidth:1,
    borderColor:colors.dgColor,
    padding:12,
    paddingLeft:20,
    paddingRight:20,
    marginTop:10
  },
  upcomingContainerHeading:{
    fontFamily:fonts.Regular,
    fontSize:12,
    color:colors.black
  },
  arrowContainer:{
    flexDirection:'row',
    alignItems:'center'
  },
  arrowIcon:{
    width:10,
    height:10,
    resizeMode:'contain',
    marginLeft:20
  },
  itemMainContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop:30,
    borderTopWidth:1,
    borderColor:colors.dgColor,
    paddingBottom:30,
    paddingTop:22
  },
  leftImageItem:{
    width:'100%',
    height:70,
    borderRadius:10,
    resizeMode:'cover'
  },
  leftContainer:{
    width:'27%'
  },
  rightContainer:{
    width:'70%',
  },
  headingItem:{
    fontSize:12,
    color:colors.black,
    fontFamily:fonts.SemiBold
  },
  rowContainer:{
    flexDirection:'row',
    marginTop:15
  },
  itemName:{
    marginTop:15,
    fontSize:12,
    fontFamily:fonts.Bold,
    color:colors.black
  },
  editText:{
    fontFamily:fonts.Medium,
    fontSize:12,
    marginTop:20,
    textAlign:'center',
    color:colors.lightOrange
  },
  weeklyMainContainer:{
    marginTop:20,
    width:70,
    padding:6,
    marginRight:11
  },
  weeklyMainBorderContainer:{
    marginTop:20,
    width:50,
    borderColor:colors.lightGreen,
    borderRadius:10,
    borderWidth:1,
    padding:6,
    marginRight:11
  }
});
