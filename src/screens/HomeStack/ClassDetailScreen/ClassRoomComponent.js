import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
    TextInput,
    Keyboard,
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView
} from "react-native";
import moment from 'moment'
import { useSelector } from 'react-redux'
import { Picker } from 'native-base';

import { isIphoneX } from "../../../utils/isIphoneX"
import Header from "../../../components/Header";
import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";
import fonts from "../../../assets/fonts";

const { width, height } = Dimensions.get('window')



const ClassRoomComponent = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [selected, setSelected] = useState('Enter gender')
    const [keyboardStatus, setKeyboardStatus] = useState(undefined);
    const [messageText, setMessageText] = useState('');
    const [announcementList, setAnnouncementList] = useState([
        {
            discription: 'This is a post submitted by the teacher. Only the teacher can post new announcements. Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. ',
            name: 'Joey A.',
            date: 'Dec 10 at 12:01 Istanbul time',
            attachment: [
                { name: 'attachedFileName.jpeg' }, { name: 'FileName.pdf' }
            ]
        },
        {
            discription: 'This is a post submitted by the teacher. Only the teacher can post new announcements. Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. ',
            name: 'Ayesha,Ali,Ahmad',
            date: 'Mon 26 Oct, 2021 at 18:00.',
            attachment: [
                { name: 'attachedFileName.jpeg' }, { name: 'FileName.pdf' }
            ]
        }
    ])
    const [isClassRoom, setIsClassRoom] = useState(true);

    const [chatList, setChatList] = useState([
        {
            name: 'Tim A.',
            detail: 'This is a comment submitted by the enrolled parent for this section. Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. ',
            date: 'Dec 10 at 11:01 Istanbul time',
        },
        {
            name: 'Joey A.',
            detail: 'This is a reply by the teacher to this parent. Only teachers can reply. Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit.',
            date: 'Dec 10 at 11:01 Istanbul time',
        },
        {
            name: 'Grace  M.',
            detail: 'Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo. ',
            date: 'Dec 10 at 11:01 Istanbul time',
        }
    ])


    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
    }, [])
    const _keyboardDidShow = () => setKeyboardStatus(true);
    const _keyboardDidHide = () => setKeyboardStatus(false);
    const joinBtn = () => {

    }
    const onValueChange = (value: string) => {
        setSelected(value)
    }
    const attachBtn = () => {

    }
    const sendMessage = () => {
        if (messageText != '') {
            let tempArray = [...chatList]
            tempArray.unshift({
                detail: messageText,
                date: 'Dec 10 at 11:01 Istanbul time',
                isSender: true,
                name: 'xyz '
            })
            setChatList(tempArray)
            setMessageText('')
        }
    }
    const renderCommentItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer]}>
                <View style={{}}>
                    <Text style={styles.headingText}>{item.name}</Text>
                    <Text style={styles.detailText}>{item.detail}</Text>
                    <Text style={styles.deteText}>{item.date}</Text>
                </View>
            </Pressable>
        );
    };
    const renderAnnouncmentItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer, { paddingBottom: 20 }]}>
                <View style={styles.innerMainContainer}>
                    <View style={styles.personInnerContainer}>
                        <Image
                            source={icons.dumyIcon}
                            style={styles.personImage} />
                        <View>
                            <Text style={styles.personName}>{item.name}</Text>
                            <Text style={[styles.deteText, { marginTop: 0 }]}>{item.date}</Text>
                        </View>
                    </View>
                    <Text style={styles.detailText}>{item.discription}</Text>
                    <View style={styles.attachmentContaier}>
                        {
                            item.attachment.map((element) => {
                                return (
                                    <TouchableOpacity style={styles.attachmentTouch}>
                                        <Text style={styles.personName}>{element.name}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                </View>
                <View style={styles.barStyle} />

            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No chat exist'}</Text>
            </View>
        )
    };

    return (
        <SafeAreaView style={styles.safeStyle}>

            <ScrollView style={styles.mainContainer}>
                <Text style={styles.mainHeadingDetail}>
                    This page is for class announcements and discussions. To Join the meeting click on Join The Meeting button. Lorem ipsum dolor. Dolor sit amet consectetur adipiscing elit. Semper viverra nam libero justo.
                </Text>
                <Text style={styles.headingStyle}>Next meeting</Text>
                <Text style={styles.meetingTime}>Mon Dec 10 at 18:00 UK time</Text>
                <Text style={styles.meetingDescription}>Make sure to join the meeting on time!</Text>
                <Button
                    text={'Join The Meeting'}
                    // textStyle={{ }}
                    backgroundColorStyle={styles.joinMeetingBtn}
                    // clickAction={}
                />
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={announcementList}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderAnnouncmentItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                <Text style={styles.commentHeadingStyle}>View all 3 comments</Text>
                <View style={styles.mainView}>
                    <FlatList
                        data={chatList}
                        inverted={(chatList.length > 3) ? true : false}
                        showsVerticalScrollIndicator={false}
                        // contentContainerStyle={{ paddingBottom: 0, flex: 1 }}
                        contentContainerStyle={{ paddingLeft: 20, paddingRight: 20 }}
                        renderItem={renderCommentItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                    <KeyboardAvoidingView
                        behavior={Platform.OS == "ios" ? "padding" : "height"}
                        keyboardVerticalOffset={150}
                        style={{ width: '100%' }}
                    >
                        <View style={[styles.bottomContainer, { marginBottom: keyboardStatus === true ? 50 : isIphoneX() ? 10 : 30 }]}>
                            <TextInput
                                value={messageText}
                                onChangeText={(text) => {
                                    setMessageText(text)
                                }}
                                style={styles.commentInput}
                                placeholder={'Write a message'}
                                placeholderTextColor={colors.chatMessage}
                            />
                            <View style={styles.buttonContainer}>
                                <Button
                                    text={'Attach a File'}
                                    textStyle={{ color: colors.lightGreen }}
                                    backgroundColorStyle={styles.attachBtnStyle}
                                    clickAction={attachBtn.bind(this)}
                                />
                                <Button
                                    text={'Send'}
                                    // textStyle={{ }}
                                    backgroundColorStyle={styles.sendBtnStyle}
                                    clickAction={sendMessage.bind(this)}
                                />
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </View>
            </ScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};
export const styles = StyleSheet.create({
    safeStyle: {
        flex: 1,
    },
    mainHeadingDetail: {
        fontSize: 12,
        fontFamily: fonts.Medium,
        color: colors.chatMessage,
        marginBottom: 15,
        marginTop: 25,
        paddingLeft: 20,
        paddingRight: 20

    },
    headingStyle: {
        fontFamily: fonts.Regular,
        fontSize: 12,
        color: colors.black,
        marginBottom: 10,
        paddingLeft: 20
    },
    meetingTime: {
        fontFamily: fonts.SemiBold,
        fontSize: 12,
        color: colors.black,
        marginBottom: 3,
        paddingLeft: 20
    },
    meetingDescription: {
        fontFamily: fonts.Medium,
        fontSize: 12,
        color: colors.dgColor,
        marginBottom: 10,
        paddingLeft: 20
    },
    activeStyle: {
        width: '50%',
        padding: 12,
        backgroundColor: colors.lightGreen,
        borderWidth: .7,
        borderRightWidth: .7,
        borderColor: colors.green
    },
    activeTextStyle: {
        fontFamily: fonts.Bold,
        fontSize: 12,
        textAlign: 'center',
        color: colors.white
    },
    mainView: {
        flex: 1
    },
    mainContainer: {
        flex: 1,

    },
    itemMainContainer: {
        paddingBottom: 25
    },
    emptyContainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyText: {
        textAlign: 'center',
        fontSize: 12,
        fontFamily: fonts.SemiBold,
        color: colors.dgColor
    },
    headingText: {
        fontFamily: fonts.Bold,
        fontSize: 12,
        color: colors.black,
    },
    personName: {
        fontFamily: fonts.SemiBold,
        fontSize: 12,
        color: colors.black,
    },
    detailText: {
        fontFamily: fonts.Medium,
        fontSize: 12,
        color: colors.borderColor,
        marginTop: 5,
        lineHeight: 18
    },
    deteText: {
        fontFamily: fonts.Medium,
        fontSize: 12,
        color: colors.chatMessage,
        marginTop: 10,
    },
    commentHeadingStyle: {
        fontSize: 12,
        fontFamily: fonts.Medium,
        color: colors.chatMessage,
        marginBottom: 15,
        paddingLeft: 20,
        paddingRight: 20,
        marginTop: 20
    },

    bottomContainer: {
        width: width - 40,
        alignItems: 'flex-end',
        alignSelf: 'center',
        padding: 15,
        backgroundColor: colors.white,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors.borderColorChat,
    },
    commentInput: {
        width: '100%',
        height: 45,
        paddingLeft: 0,
        paddingRight: 10,
        marginTop: -10
    },
    sendStyle: {
        width: 37,
        height: 37,
        resizeMode: 'contain'
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'space-between',
        marginBottom: 0
    },
    attachBtnStyle: {
        width: 100,
        height: 32,
        backgroundColor: colors.white,
        borderWidth: .8,
        borderColor: colors.borderColorChat,
        marginRight: 10,
        elevation: 0,
        shadowColor: colors.white
    },
    joinMeetingBtn: {
        width: 170,
        backgroundColor: colors.lightOrange,
        height: 35,
        shadowOpacity:0,
        marginLeft:20,
        marginBottom:25
    },
    personInnerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 17
    },
    personImage: {
        width: 32,
        height: 32,
        borderRadius: 16,
        resizeMode: 'contain',
        marginRight: 14
    },
    attachmentContaier: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    attachmentTouch: {
        marginRight: 10,
        backgroundColor: colors.dgColor,
        padding: 10,
        paddingBottom: 6,
        paddingTop: 6,
        borderRadius: 4
    },
    barStyle: {
        height: 1,
        width: '97%',
        backgroundColor: colors.dgColor,
        alignSelf: 'flex-end',
        marginTop: 20
    },
    innerMainContainer: {
        paddingLeft: 20,
        paddingRight: 20
    },
    sendBtnStyle: {
        width: 65,
        backgroundColor: colors.lightGreen,
        height: 32
      },
});
export default ClassRoomComponent;
