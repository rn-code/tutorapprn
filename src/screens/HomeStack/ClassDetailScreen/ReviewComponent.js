import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
    TextInput,
    Keyboard,
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView
} from "react-native";
import moment from 'moment'
import { useSelector } from 'react-redux'
import { Picker } from 'native-base';
import { Rating } from 'react-native-ratings';

import { isIphoneX } from "../../../utils/isIphoneX"
import Header from "../../../components/Header";
import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";
import fonts from "../../../assets/fonts";

const { width, height } = Dimensions.get('window')



const ReviewComponent = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [keyboardStatus, setKeyboardStatus] = useState(undefined);
    const [messageText, setMessageText] = useState('');

    const [chatList, setChatList] = useState([

    ])


    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
    }, [])
    const _keyboardDidShow = () => setKeyboardStatus(true);
    const _keyboardDidHide = () => setKeyboardStatus(false);
    const joinBtn = () => {

    }
    const attachBtn = () => {

    }
    const sendMessage = () => {

    }
    const ratingCompleted = (rating) => {
        console.log("Rating is: " + rating)
        setratingSelByUser(rating)
    }
    const renderCommentItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {
                }}
                style={[styles.itemMainContainer]}>
                <View style={{}}>
                    <Text style={styles.headingText}>{item.name}</Text>
                    <Text style={styles.detailText}>{item.detail}</Text>
                    <Text style={styles.deteText}>{item.date}</Text>
                </View>
            </Pressable>
        );
    };



    return (
        <SafeAreaView style={styles.safeStyle}>
            <ScrollView style={styles.mainContainer}>
                <View style={styles.mainView}>
                    <Text style={styles.headingText}>Your review</Text>
                    <View style={styles.ratingContainer}>
                        <Rating
                            startingValue={5}
                            ratingCount={5}
                            showRating={false}
                            imageSize={20}
                            fractions={1}
                            readonly={true}
                            type={'custom'}
                            selectedColor={'#EFC282'}
                            ratingColor={'#EFC282'}
                            ratingBackgroundColor='#c8c7c8'
                            onFinishRating={ratingCompleted}
                        />
                    </View>
                    <FlatList
                        data={chatList}
                        inverted={(chatList.length > 3) ? true : false}
                        showsVerticalScrollIndicator={false}
                        // contentContainerStyle={{ paddingBottom: 0, flex: 1 }}
                        contentContainerStyle={{ paddingLeft: 20, paddingRight: 20 }}
                        renderItem={renderCommentItem}
                        showsVerticalScrollIndicator={false}
                    />
                    <KeyboardAvoidingView
                        behavior={Platform.OS == "ios" ? "padding" : "height"}
                        keyboardVerticalOffset={150}
                        style={{ width: '100%' }}
                    >
                        <View style={[styles.bottomContainer, { marginBottom: keyboardStatus === true ? 50 : isIphoneX() ? 10 : 30 }]}>
                            <TextInput
                                value={messageText}
                                onChangeText={(text) => {
                                    setMessageText(text)
                                }}
                                style={styles.commentInput}
                                placeholder={'Share details of your own experience here.'}
                                placeholderTextColor={colors.chatMessage}
                            />
                            <View style={styles.buttonContainer}>
                                <Button
                                    text={'Attach a File'}
                                    textStyle={{ color: colors.lightGreen }}
                                    backgroundColorStyle={styles.attachBtnStyle}
                                    clickAction={attachBtn.bind(this)}
                                />
                                <Button
                                    text={'Send'}
                                    // textStyle={{ }}
                                    backgroundColorStyle={styles.sendBtnStyle}
                                    clickAction={sendMessage.bind(this)}
                                />
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                    <Text style={styles.headingText}>Teacher</Text>
                    <View style={styles.teacherContainer}>
                        <Image
                            source={icons.dumyIcon}
                            style={styles.teacherImage}
                        />
                        <View style={{marginLeft:10}}>
                            <Text style={styles.teacherName}>Joey Amy </Text>
                            <Text style={styles.teacherDescription}>Teacher title will be here</Text>
                        </View>
                    </View>
                    <View style={styles.btnContainer}>
                        <Text style={styles.btnText}>View Profile</Text>
                    </View>
                    <Text style={styles.headingText}>Class</Text>
                    <Text style={styles.classDetailStyle}>Learn how to read and write while having fun - kindergarten</Text>
                    <View style={styles.btnContainer}>
                        <Text style={styles.btnText}>View Class</Text>
                    </View>
                </View>
            </ScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};
export const styles = StyleSheet.create({
    safeStyle: {
        flex: 1,
    },
    headingStyle: {
        fontFamily: fonts.Regular,
        fontSize: 12,
        color: colors.black,
        marginBottom: 10,
        paddingLeft: 20
    },
    mainView: {
        flex: 1
    },
    mainContainer: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20
    },
    itemMainContainer: {
        paddingBottom: 25
    },
    headingText: {
        fontFamily: fonts.Bold,
        fontSize: 12,
        color: colors.black,
    },
    detailText: {
        fontFamily: fonts.Medium,
        fontSize: 12,
        color: colors.borderColor,
        marginTop: 5,
        lineHeight: 18
    },
    deteText: {
        fontFamily: fonts.Medium,
        fontSize: 12,
        color: colors.chatMessage,
        marginTop: 10,
    },
    bottomContainer: {
        width: width - 40,
        alignItems: 'flex-end',
        alignSelf: 'center',
        padding: 15,
        backgroundColor: colors.white,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors.borderColorChat,
    },
    commentInput: {
        width: '100%',
        height: 45,
        paddingLeft: 0,
        paddingRight: 10,
        marginTop: -10
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'space-between',
        marginBottom: 0
    },
    attachBtnStyle: {
        width: 100,
        height: 32,
        backgroundColor: colors.white,
        borderWidth: .8,
        borderColor: colors.borderColorChat,
        marginRight: 10,
        elevation: 0,
        shadowColor: colors.white
    },
    sendBtnStyle: {
        width: 65,
        backgroundColor: colors.lightGreen,
        height: 32
    },
    headingText: {
        fontSize: 12,
        color: colors.black,
        fontFamily: fonts.Medium,
        marginTop: 30
    },
    ratingContainer: {
        flexDirection: "row",
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20
    },
    teacherContainer: {
        flexDirection: 'row',
        alignItems:'center',
        marginTop:10
    },
    teacherImage: {
        width: 56,
        height: 56,
        resizeMode: 'cover',
        borderRadius: 28
    },
    teacherName:{
        fontSize:12,
        fontFamily:fonts.Regular
    },
    teacherDescription:{
        fontSize:12,
        fontFamily:fonts.Medium,
        marginTop:7
    },
    btnContainer:{
        backgroundColor:colors.lightGrey,
        height:42,
        width:140,
        alignItems:'center',
        justifyContent:'center',
        marginTop:10,
        borderRadius:8
    },
    btnText:{
        fontFamily:fonts.SemiBold,
        fontSize:12,
        color:colors.black
    },
    classDetailStyle:{
        fontSize:12,
        fontFamily:fonts.Medium,
        color:colors.black,
        marginTop:10
    }
});
export default ReviewComponent;
