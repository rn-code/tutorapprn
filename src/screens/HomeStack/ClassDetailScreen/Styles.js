import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
  },
  backBtnContainer:{
    flexDirection:'row',
    marginTop:15,
    alignItems:'center',
  },
  backImage:{
    width:19,
    height:19,
    resizeMode:'contain',
    marginRight:13
  },
  headerText:{
    fontSize:12,
    fontFamily:fonts.SemiBold,
    color:colors.borderColor,
    width:'87%',
  },
  dateText:{
    fontSize:12,
    fontFamily:fonts.SemiBold,
    color:colors.borderColor,
    marginTop:20
  },
  mainHeadingDetail:{
    fontSize:12,
    fontFamily:fonts.Medium,
    color:colors.chatMessage,
    marginBottom:15,
    marginTop:25
  },
  topBarcontainer:{
    flexDirection:'row',
    width:'100%',
    marginTop:30
  },
  activeStyle:{
    width:'50%',
    padding:12,
    backgroundColor:colors.lightGreen,
    borderWidth:.7,
    borderRightWidth:.7,
    borderColor:colors.green
  },
  unActiveStyle:{
    width:'50%',
    padding:12,
    borderWidth:.7,
    borderRightWidth:.7,
    borderColor:colors.borderColor
  },
  activeTextStyle:{
      fontFamily:fonts.Bold,
      fontSize:12,  
      textAlign:'center',
      color:colors.white
  },
  unActiveTextStyle:{
      fontFamily:fonts.Regular,
      fontSize:12,  
      textAlign:'center',
      color:colors.borderColor
  },
  mainView: {
   flex:1
  },
  mainContainer: {
   flex:1
  },
 
});
