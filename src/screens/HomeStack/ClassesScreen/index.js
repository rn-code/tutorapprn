import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { useSelector } from 'react-redux'

import Header from "../../../components/Header";
import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";
import { Picker } from 'native-base';

const { width, height } = Dimensions.get('window')



const ClassesScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);
    const [selected, setSelected] = useState('Upcoming')

    const [classList, setClassList] = useState([
        {
            heading: 'Learn how to read and write while having fun - kindergarten',
            students: 'Ayesha,Ali,Ahmad',
            schedule: 'Mon 26 Oct, 2021 at 18:00.',
            totalClasses: 1
        },
        {
            heading: 'Class name will be here. Class name won’t be longer than one line. Class names will be here. ',
            students: 'Ayesha,Ali,Ahmad',
            schedule: 'Mon 26 Oct, 2021 at 18:00.',
            totalClasses: 2
        },
        {
            heading: 'Class name will be here. Class name won’t be longer than one line. Class names will be here. ',
            students: 'Ayesha,Ali,Ahmad',
            schedule: 'Mon 26 Oct, 2021 at 18:00.',
            totalClasses: 3
        }
    ])

    useEffect(() => {


    }, [])
    const joinBtn = () => {
        navigation.navigate('ClassDetailScreen')
    }
    const onValueChange = (value: string) => {
        setSelected(value)
    }
    const renderClassItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => {}}
                style={[styles.itemMainContainer]}>
                <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                    <Text style={styles.headingText}>{item.heading}</Text>
                    <View style={styles.textContainer}>
                        <Text style={styles.itemItemHeading}>{'Total classes'}</Text>
                        <Text style={[styles.headingText, { marginLeft: 10 }]}>{item.totalClasses}</Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text style={styles.itemItemHeading}>{'Students'}</Text>
                        <Text style={[styles.headingText, { marginLeft: 10 }]}>{item.students}</Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text style={styles.itemItemHeading}>{'Scheduled'}</Text>
                        <Text style={[styles.headingText, { marginLeft: 10 }]}>{item.schedule}</Text>
                    </View>
                    {index === 0 &&
                        <Button
                            text={'Join'}
                            backgroundColorStyle={styles.saveBtn}
                            clickAction={joinBtn.bind(this)}
                        />
                    }
                </View>
                {index != (classList.length - 1) &&
                    <View style={styles.barView} />
                }
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No chat exist'}</Text>
            </View>
        )
    };

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Classes'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    // navigation.toggleDrawer();
                }}
            />
            <View style={styles.pickerContainer}>
                <Picker
                    mode="dropdown"
                    style={{ width:'50%', }}
                    selectedValue={selected}
                    textStyle={{ color: colors.dgColor }}
                    onValueChange={onValueChange}
                >
                    <Picker.Item label="Upcoming" value="key0" />
                    <Picker.Item label="Ongoing" value="key1" />
                </Picker>
                {Platform.OS === 'ios' &&
                    <Image
                        style={styles.dropDownStyle}
                        source={icons.dropdownIcon}
                    />
                }
            </View>
            <View style={styles.mainView}>
                <FlatList
                    data={classList}
                    showsVerticalScrollIndicator={false}
                    // contentContainerStyle={{}}
                    renderItem={renderClassItem}
                    ListEmptyComponent={renderEmptyContainer}
                    showsVerticalScrollIndicator={false}
                />
            </View>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default ClassesScreen;
