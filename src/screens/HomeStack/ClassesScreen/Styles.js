import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
  },
  mainView: {
    width: '100%',
    height: '100%'
  },
  barView: {
    width:'95%',
    alignSelf:'flex-end',
    borderBottomWidth: 1,
    borderColor: colors.borderColorLight,
    marginTop:30
  },
  itemMainContainer: {
    paddingBottom: 20
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  textContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },
  headingText: {
    fontFamily: fonts.SemiBold,
    fontSize: 12,
    color: colors.black,
  },
  lastMsgText: {
    marginTop: 8,
    color: colors.dgColor,
    fontSize: 12,
    fontFamily: fonts.Medium,
    // width:'50%'
  },
  itemItemHeading: {
    fontFamily: fonts.Regular,
    fontSize: 12,
    color: colors.dgColor
  },
 
  saveBtn: {
    backgroundColor: colors.lightOrange,
    height: 44,
    width: 80,
    borderRadius: 8,
    marginTop: 10,
    shadowOpacity: 0
  },
  pickerContainer:{
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '50%',
    marginLeft:20,
    marginBottom:30,
    backgroundColor: colors.white,
    borderRadius: 10,
    marginTop: 10,
    shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.20,
  shadowRadius: 1.41,
  
  elevation: 2
  },
  dropDownStyle:{
    width:10,
    height:6,
    resizeMode:'contain',
    right:18,
    position:'absolute'
  },
});
