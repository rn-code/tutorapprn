import React, { useEffect } from 'react';
import { StyleSheet, View, Image, Text, Dimensions } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const { width, height } = Dimensions.get('window')
const tabActiveWidth = (width / 4)
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

//Screens
import HomeScreen from './HomeScreen'
import ClassesScreen from './ClassesScreen'
import ChatScreen from './ChatScreen'
import NotificationScreen from './NotificationScreen'
import ChatRoomScreen from './ChatRoomScreen'
import PurchaseScreen from './PurchaseScreen'
import FilterScreen from './FilterScreen'
import ProfileSettingScreen from './ProfileSettingScreen'
import ClassDetailScreen from './ClassDetailScreen'
import SearchScreen from './SearchScreen'
import ProfileScreen from './ProfileScreen'
import WorkshopRegistrationScreen from './WorkshopRegistrationScreen'
import WorkshopDetailScreen from './WorkshopDetailScreen'
import DashboardScreen from './DashboardScreen'
import AddKidScreen from '../AuthStack/AddKidScreen'

//Utils
import colors from '../../utils/colors';
import { isIphoneX } from '../../utils/isIphoneX'

//Components
import DrawerComponent from '../../components/DrawerComponent';

//Assets
import icons from '../../assets/icons'
import fonts from '../../assets/fonts'

const TabButton = (props) => {
    const { focused, icon, label } = props
    useEffect(() => {

    }, [])
    return (
        <View style={[styles.iconContainer, { width: tabActiveWidth }]}>
            <Image
                source={icon}
                style={[styles.tabIconStyle, {
                    // tintColor: focused ? colors.black : colors.black
                }]} />
            {focused &&
                <Text
                    style={styles.activeText}>
                    {label}
                </Text>
            }
        </View>
    )
}

const MyTabs = () => {
    return (
        <Tab.Navigator

            initialRouteName={'HomeScreen'}
            screenOptions={({ route }) => ({
                tabBarShowLabel: false,
                tabBarLabel: false,
                keyboardHidesTabBar: true,
                tabBarStyle: {
                    height: 60,
                    width: '100%',
                    backgroundColor: colors.white,
                    paddingLeft: 20,
                    paddingRight: 20,
                    marginBottom: isIphoneX() ? 25 : 0,
                    overflow: 'hidden',
                    borderTopWidth: 1,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 4,
                    },
                    shadowOpacity: 0.32,
                    shadowRadius: 5.46,
                    elevation: 9,
                },
                tabBarIcon: ({ focused }) => {
                    if (route.name === 'HomeScreen') {
                        if (focused)
                            return (
                                <TabButton
                                    icon={icons.homeBlackIcon}
                                    label={'Home'}
                                    focused={focused}
                                />
                            )
                        else
                            return (
                                <TabButton
                                    icon={icons.homeGreyIcon}
                                    label={'Home'}
                                    focused={focused}
                                />
                            )
                    }
                    if (route.name === 'ClassesScreen') {
                        if (focused)
                            return (
                                <TabButton
                                    icon={icons.fileBlackIcon}
                                    label={"Classes"}
                                    focused={focused}
                                />
                            )
                        else
                            return (
                                <TabButton
                                    icon={icons.fileGreyIcon}
                                    label={"Classes"}
                                    focused={focused}
                                />
                            )
                    }
                    if (route.name === 'ChatScreen') {
                        if (focused)
                            return (
                                <TabButton
                                    icon={icons.chatGreyIcon}
                                    label={"Messages"}
                                    focused={focused}
                                />
                            )
                        else
                            return (
                                <TabButton
                                    icon={icons.chatGreyIcon}
                                    label={'Messages'}
                                    focused={focused}
                                />
                            )
                    }
                    if (route.name === 'NotificationScreen') {
                        if (focused)
                            return (
                                <TabButton
                                    icon={icons.notificationBlackIcon}
                                    label={'Notifications'}
                                    focused={focused}
                                />
                            )
                        else
                            return (
                                <TabButton
                                    icon={icons.notificationGreyIcon}
                                    label={'Notifications'}
                                    focused={focused}
                                />
                            )
                    }
                },
            })}>
            <Tab.Screen
                name="HomeScreen"
                component={HomeScreen}
                options={{
                    tabBarLabel: 'Home',
                    header: () => null
                }}
            />
            <Tab.Screen
                name="ClassesScreen"
                component={ClassesScreen}
                options={{
                    tabBarLabel: 'Classes',
                    header: () => null
                }}
            />
            <Tab.Screen
                name="ChatScreen"
                component={ChatScreen}
                options={{
                    tabBarLabel: 'Chat',
                    header: () => null
                }}
            />
            <Tab.Screen
                name="NotificationScreen"
                component={NotificationScreen}
                options={{
                    tabBarLabel: 'Notification',
                    header: () => null
                }}
            />

        </Tab.Navigator >
    );
}

/** Home Drawer */
const HomeDrawerStack = (props) => (
    <Drawer.Navigator
        drawerStyle={{ backgroundColor: colors.white, width: '80%' }}
        headerMode="none"
        initialRouteName='HomeScreen'
        drawerContent={(props) => <DrawerComponent {...props} />}>
        <Drawer.Screen options={{ header: () => null }} name="HomeScreen" component={MyTabs} />
        <Drawer.Screen options={{ header: () => null }} name="ChatRoomScreen" component={ChatRoomScreen} />
        <Drawer.Screen options={{ header: () => null }} name="PurchaseScreen" component={PurchaseScreen} />
        <Drawer.Screen options={{ header: () => null }} name="ProfileSettingScreen" component={ProfileSettingScreen} />
        <Drawer.Screen options={{ header: () => null }} name="ClassDetailScreen" component={ClassDetailScreen} />
        <Drawer.Screen options={{ header: () => null }} name="SearchScreen" component={SearchScreen} />
        <Drawer.Screen options={{ header: () => null }} name="DashboardScreen" component={DashboardScreen} />
    </Drawer.Navigator>
);

/** Home Stack */
const HomeStack = () => (
    <Stack.Navigator
        headerMode="none"
        initialRouteName='HomeDrawerStack'>
        <Stack.Screen options={{ header: () => null }} name="HomeDrawerStack" component={HomeDrawerStack} />
        <Stack.Screen options={{ header: () => null }} name="FilterScreen" component={FilterScreen} />
        <Stack.Screen options={{ header: () => null }} name="ProfileScreen" component={ProfileScreen} />
        <Stack.Screen options={{ header: () => null }} name="WorkshopRegistrationScreen" component={WorkshopRegistrationScreen} />
        <Stack.Screen options={{ header: () => null }} name="WorkshopDetailScreen" component={WorkshopDetailScreen} />
        <Stack.Screen options={{ header: () => null }} name="AddKidScreen" component={AddKidScreen} />

    </Stack.Navigator>
);

export const styles = StyleSheet.create({
    activeText: {
        fontSize: 12,
        fontFamily: fonts.Medium,
        color: colors.black,
        marginTop: 5,
        marginLeft: 10,
    },
    iconContainer: {
        // padding: 5,
        height: 60,
        flexDirection: 'row',
        width: '99%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: isIphoneX() ? 25 : 0
    },
    tabIconStyle: {
        width: 15,
        height: 15,
        resizeMode: 'contain',
    }
});

export default HomeStack