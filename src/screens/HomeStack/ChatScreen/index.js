import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Pressable,
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { useSelector } from 'react-redux'

import Header from "../../../components/Header";
import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')



const ChatScreen = (props) => {

    const {navigation}=props
    const [loading, setLoading] = useState(false);
    const [chatList, setChatList] = useState([
        {
            name: 'Joey Amy',
            lastMsg: 'Teacher chat headline will be here',
        },
        {
            name: 'Joey Amy 5',
            lastMsg: 'Teacher chat headline will be here Teacher chat headline will be here Teacher chat headline will be here Teacher chat headline will be here Teacher chat headline will be here ',
        },
        {
            name: 'Joey Amy 2',
            lastMsg: 'Teacher chat headline will be here',
        }
    ])

    useEffect(() => {


    }, [])
    const renderChatItem = ({ item, index }) => {
        return (
            <Pressable
            onPress={()=>{
                navigation.navigate('ChatRoomScreen')
            }}
            style={[styles.itemMainContainer, {
                paddingBottom: index === chatList.length - 1 ? 35 : 20
            }]}>
                <Image
                    source={icons.dumyIcon}
                    style={styles.userImage}
                />
                <View style={styles.textContainer}>
                    <Text style={styles.nameText}>{item.name}</Text>
                    <Text style={styles.lastMsgText}>{item.lastMsg}</Text>
                </View>
            </Pressable>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'No chat exist'}</Text>
            </View>
        )
    };

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Header
                leftIcon={icons.drawerIcon}
                rightIcon={icons.dumyIcon}
                hearderText={'Messages'}
                onLeftAction={() => {
                    navigation.toggleDrawer();
                }}
                onRightAction={() => {
                    // navigation.toggleDrawer();
                }}
            />
            <View style={styles.mainView}>
                <FlatList
                    data={chatList}
                    showsVerticalScrollIndicator={false}
                    // contentContainerStyle={{}}
                    renderItem={renderChatItem}
                    ListEmptyComponent={renderEmptyContainer}
                    showsVerticalScrollIndicator={false}
                />
            </View>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default ChatScreen;
