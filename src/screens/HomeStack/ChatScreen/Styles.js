import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
  },
  mainView:{
    width:'100%',
    height:'100%',
    paddingLeft:20,
    paddingRight:20
  },
  itemMainContainer:{
    flexDirection:'row',
    borderBottomWidth:1,
    borderColor:colors.borderColorLight,
    paddingTop:20,
    paddingBottom:20,
    alignItems:'center',
  },
  emptyContainer:{
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'center'
  },
  emptyText:{
    textAlign:'center',
    fontSize:12,
    fontFamily:fonts.SemiBold,
    color:colors.dgColor
  },
  userImage:{
    width:50,
    height:50,
    backgroundColor:colors.lightGrey,
    borderRadius:10
  },
  textContainer:{
    flex:1,
    marginLeft:15,
    justifyContent:'center',
  },
  nameText:{
    fontFamily:fonts.SemiBold,
    fontSize:12,
    color:colors.black,
  },
  lastMsgText:{
    marginTop:8,
    color:colors.dgColor,
    fontSize:12,
    fontFamily:fonts.Medium,
    // width:'50%'
  }
});
