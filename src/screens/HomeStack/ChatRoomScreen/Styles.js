import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
  },
  backIconStyle:{
    width:20,
    height:20,
    marginRight:15,
    resizeMode:'contain'
  },
  itemContainerReceiver: {
    width: '75%',   
    alignSelf: 'flex-start',
    marginBottom:20
  },
  itemContainerSender: {
    width: '75%',   
    alignSelf: 'flex-end',
    marginBottom:20
  },
  flatListContainer: {
    flex:1,
    paddingLeft:20,
    paddingRight:20
  },
  userName: {
    fontSize: 12,
    color: colors.black,
    fontFamily: fonts.Regular,
    lineHeight: 19
  },
  lastName: {
    fontSize: 14,
    color: colors.grey,
    fontFamily: fonts.Regular,
    lineHeight: 19
  },
  userImage: {
    backgroundColor: colors.grey,
    width: 40,
    height: 40,
    borderRadius: 30,
    marginRight: 12
  },
  comment: {
    fontSize: 14,
    color: colors.black,
    fontFamily: fonts.Regular,
    lineHeight: 19,
    marginTop: 20,
  },
  dateStyle: {
    fontSize: 14,
    marginTop: 20,
    color: colors.grey,
    fontFamily: fonts.Regular,
    alignSelf: 'flex-end'
  },
  emptyContainer:{
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'center'
  },
  emptyText:{
    textAlign:'center',
    fontSize:12,
    fontFamily:fonts.SemiBold,
    color:colors.dgColor,
    lineHeight:20
  },
  msgTextContainer: {
    flexDirection: 'row',
    borderWidth: .7,
    borderRadius:10,
    padding: 17,
    borderColor: colors.borderColorChat,
  },
  msgTextContainerSender: {
    flexDirection: 'row',
    padding: 17,
    backgroundColor:colors.msgBackground,
    borderRadius:10
  },
  bottomContainer: {
    width: '100%',
    alignItems: 'flex-end',
    padding: 15,
    backgroundColor: colors.white,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: colors.borderColorChat
  },
  commentInput: {
    width: '100%',
    height: 45,
    paddingLeft: 0,
    paddingRight: 10,
      marginTop:-10
  },
  sendStyle: {
    width: 37,
    height: 37,
    resizeMode: 'contain'
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
    marginBottom: 0
  },
  attachBtnStyle: {
    width: 100,
    height:32,
    backgroundColor: colors.white,
    borderWidth: .8,
    borderColor: colors.borderColorChat,
    marginRight:10,
    elevation:0,
    shadowColor:colors.white
  },
  sendBtnStyle: {
    width: 65,
    backgroundColor: colors.lightGreen,
    height: 32
  },
  msgText:{
    fontFamily:fonts.Medium,
    fontSize:12,
    color:colors.lightBlack
  },
  dateText:{
    fontFamily:fonts.Medium,
    fontSize:10,
    color:colors.lightBlack,
    marginTop:6,
    marginLeft:5
  },
  messagesDate:{
    fontFamily:fonts.Medium,
    fontSize:10,
    color:colors.lightBlack,
    marginTop:30,
    marginBottom:20,
    textAlign:'center'
  },
  itemMainContainer:{
    flexDirection:'row',
    borderBottomWidth:1,
    borderColor:colors.borderColorLight,
    paddingTop:20,
    paddingLeft:20,
    paddingRight:20,
    paddingBottom:10,
    alignItems:'center',
  },
  userImage:{
    width:50,
    height:50,
    backgroundColor:colors.lightGrey,
    borderRadius:10
  },
  nameText:{
    fontFamily:fonts.SemiBold,
    fontSize:12,
    color:colors.black,
    marginLeft:15,
  },
  lastMsgText:{
    marginTop:8,
    color:colors.dgColor,
    fontSize:12,
    fontFamily:fonts.Medium,
    // width:'50%'
  },
  textContainer:{
    flex:1,
    flexDirection:'row',
    alignItems:'center'
  },
  moreStyle:{
    width:21,
    height:5,
    resizeMode:'contain'
  }
});
