import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    SafeAreaView,
    TouchableOpacity,
    TextInput,
    Keyboard,
    KeyboardAvoidingView
} from "react-native";
import { styles } from "./Styles";
import moment from 'moment'
import { useSelector } from 'react-redux'

import { isIphoneX } from "../../../utils/isIphoneX";
import Header from "../../../components/Header";
import Loader from '../../../components/Loader'
import Button from '../../../components/Button'
import icons from "../../../assets/icons";
import colors from "../../../utils/colors";

const { width, height } = Dimensions.get('window')



const ChatRoomScreen = ({ navigation }) => {


    const [loading, setLoading] = useState(false);
    const [messageText, setMessageText] = useState('');
    const [keyboardStatus, setKeyboardStatus] = useState(undefined);
    const [chatList, setChatList] = useState([
        // {
        //     msg: 'This is a message sent by the enrolled parent for this section. Lorem ipsum dolor.',
        //     date: new Date(),
        //     isSender: false,
        //     countryTime: '11:05 Istanbul time '
        // },
    ])

    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
    }, [])
    const _keyboardDidShow = () => setKeyboardStatus(true);
    const _keyboardDidHide = () => setKeyboardStatus(false);
    const attachBtn = () => {

    }
    const sendMessage = () => {
        if (messageText != '') {
            let tempArray = [...chatList]
            tempArray.unshift({
                msg: messageText,
                date: new Date(),
                isSender: true,
                countryTime: '11:05 Istanbul time '
            })
            setChatList(tempArray)
            setMessageText('')
        }
    }

    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>{'Your Messages \nSend private messages to the teacher'}</Text>
            </View>
        )
    };
    const renderCommentItem = ({ item, index }) => {
        return (
            <View>
                {index === 0 && <Text style={[styles.messagesDate]}>{'Saturday Dec 8'}</Text>}
                <View
                    style={item.isSender ?
                        styles.itemContainerSender :
                        styles.itemContainerReceiver}>
                    <View style={item.isSender ?
                        styles.msgTextContainerSender :
                        styles.msgTextContainer}>
                        <Text style={styles.msgText}>{item.msg}</Text>
                    </View>
                    <Text style={[styles.dateText, {
                        alignSelf: item.isSender ? 'flex-end' : 'flex-start'
                    }]}>{'11:05 Istanbul time '}</Text>
                </View>
            </View>
        );
    }
    return (
        <SafeAreaView style={styles.safeStyle}>
            <View
                style={styles.itemMainContainer}>
                <TouchableOpacity
                    onPress={() => {
                        navigation.goBack()
                    }}
                >
                    <Image
                        source={icons.backIcon}
                        style={styles.backIconStyle}
                    />
                </TouchableOpacity>
                <View style={styles.textContainer}>
                    <Image
                        source={icons.dumyIcon}
                        style={styles.userImage}
                    />
                    <Text style={styles.nameText}>{'Joey Amy'}</Text>
                </View>
                <Image
                    source={icons.moreIcon}
                    style={styles.moreStyle}
                />
            </View>
            <View style={styles.flatListContainer}>
                <FlatList
                    inverted={(chatList.length > 2) ? true : false}
                    data={chatList}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 0, flex: 1 }}
                    renderItem={renderCommentItem}
                    ListEmptyComponent={renderEmptyContainer}

                />
                <KeyboardAvoidingView
                    behavior={Platform.OS == "ios" ? "padding" : "height"}
                    keyboardVerticalOffset={150}
                    style={{ width: '100%' }}
                >
                    <View style={[styles.bottomContainer, { marginBottom: keyboardStatus === true ? 50 : isIphoneX() ? 10 : 30 }]}>
                        <TextInput
                            value={messageText}
                            onChangeText={(text) => {
                                setMessageText(text)
                            }}
                            style={styles.commentInput}
                            placeholder={'Write a message'}
                            placeholderTextColor={colors.chatMessage}
                        />
                        <View style={styles.buttonContainer}>
                            <Button
                                text={'Attach a File'}
                                textStyle={{ color: colors.lightGreen }}
                                backgroundColorStyle={styles.attachBtnStyle}
                                clickAction={attachBtn.bind(this)}
                            />
                            <Button
                                text={'Send'}
                                // textStyle={{ }}
                                backgroundColorStyle={styles.sendBtnStyle}
                                clickAction={sendMessage.bind(this)}
                            />
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </View>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default ChatRoomScreen;
