import { StyleSheet } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'
export const styles = StyleSheet.create({
  safeStyle: {
    width:'100%',
    height:'100%',
    paddingLeft:22,
    paddingRight:22
  },
  headerText:{
    fontFamily:fonts.Bold,
    fontSize:24,
    lineHeight:36,
  },
  headerDescriptiveContainer:{
    flexDirection:'row',
    marginTop:20
  },
  mediumText:{
    fontSize:12,
    fontFamily:fonts.Medium,
    color:colors.dgColor,
    textAlign:'center'
  },
  largeText:{
    fontSize:12,
    fontFamily:fonts.Bold,
    color:colors.lightGreen
  },
  fieldContainer:{
    alignItems:'center',
    marginBottom:30
  },
  circleContainer:{
    width:163,
    height:163,
    borderRadius:80,
    backgroundColor:colors.lightGreen,
    opacity:.5,
    alignItems:'center',
    justifyContent:'center'
  },
  signupLogoStyle:{
    width:124,
    height:162,
    resizeMode:'contain'
  },
  buttonContainer:{
    flexDirection:'row',
    marginTop:110,
    marginBottom:30,
    // position:'absolute',
    // bottom:40
  },
  loginBtnStyle:{
    width:'35%',
    backgroundColor:colors.white,
    borderWidth:.8,
    borderColor:colors.lightGreen
 },
 forgotContainer:{
  alignSelf:'flex-end' ,
  marginTop:15  
},
forgotText:{
  fontFamily:fonts.Regular,
  color:colors.lightGreen,
  fontSize:12
},
backBtnContainer:{
  flexDirection:'row',
  marginTop:15,
  alignItems:'center'
},
backImage:{
  width:19,
  height:19,
  resizeMode:'contain',
  marginRight:13
}
});
