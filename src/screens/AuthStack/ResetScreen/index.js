import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import icons from '../../../assets/icons'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";

import { resetPasswordAPI } from '../../../api/methods/auth'


const ResetScreen = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [password, setPassword] = useState("");

    const updatePasswordBtn = async () => {
        if (password === "") {
            showErrorMsg("Password is required");
        } else if (password.length < 6) {
            showErrorMsg("Password must be atleast 6 character");
        } else {
            resetPasswordApi()
        }
    };
    const resetPasswordApi = async () => {
        try {
            setLoading(true)
            const response = await resetPasswordAPI({
                "email": props?.route?.params?.emailResetPassword,
                "password": password,
            })
            setLoading(false)
            if (response.status === 200) {
                setPassword('')
                console.log('resetPasswordApi-response', response?.data)
                navigation.navigate('LoginScreen')
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('resetPasswordApi-error-->', error)
        }
    }
    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>Reset password</Text>
            </Pressable>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 30 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.fieldContainer}>
                    <Image
                        style={styles.signupLogoStyle}
                        source={icons.unlockIcon}
                    />
                    <Text style={[
                        styles.mediumText,
                        { marginTop: 5 }]}>
                       Verification successful. Please reset your password.
                    </Text>
                </View>
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Enter new password'}
                    value={password}
                    onChangeText={(text) => {
                        setPassword(text)
                    }}
                />
                <View style={styles.buttonContainer}>
                    <Button
                        text={'Update password'}
                        // textStyle={{ }}
                        backgroundColorStyle={{ }}
                        clickAction={()=>{
                            updatePasswordBtn()
                        }}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default ResetScreen;
