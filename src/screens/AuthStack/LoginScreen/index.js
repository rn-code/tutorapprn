import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import icons from '../../../assets/icons'

import { useDispatch, useSelector } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";

import { loginAPI } from '../../../api/methods/auth'
import { onLoginResponse } from '../../../actions/userSession'




const LoginScreen = ({ navigation }) => {

    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState("tutor@gmail.com");
    const [password, setPassword] = useState("");

    const loginBtn = async () => {
        if (email === "") {
            showErrorMsg("Email is required");
        } else if ((/^\w+([\.+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/).test(email.trim()) == false) {
            showErrorMsg('Email format is invalid')
        } else if (password === "") {
            showErrorMsg("Password is required");
        } else if (password.length < 6) {
            showErrorMsg("Password must be atleast 6 character");
        } else {
            loginApi()
        }
    };
    const signUpBtn = () => {
        navigation.navigate('SignupScreen')
    }
    const loginApi = async () => {
        console.log('loginAPI-call')
        try {
            setLoading(true)
            const response = await loginAPI({
                "email": email.toLowerCase(),
                "password": password,
            })
            setLoading(false)
            if (response.status === 200) {
                setEmail('')
                setPassword('')
                console.log('loginAPI-response', response?.data)
                dispatch(onLoginResponse(response.data))
                navigation.navigate('AfterSplash')
                
            }
            else {
                showErrorMsg('please check your creditional')
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('loginAPI-error-->', error)
        }
    }
    return (
        <SafeAreaView style={styles.safeStyle}>
            <Text style={styles.headerText}>Login</Text>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 30 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.fieldContainer}>
                    <View style={styles.circleContainer}>
                        <Image
                            style={styles.signupLogoStyle}
                            source={icons.loginLogoIcon}
                        />
                    </View>
                    <Text style={styles.headerText}>Welcome back!</Text>
                    <Text style={[styles.mediumText, { marginTop: 5 }]}>
                        Let’s get started where you left.
                    </Text>
                </View>
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Enter email address'}
                    value={email}
                    keyboardType={'email-address'}
                    autoComplete={'email'}
                    textContentType={'emailAddress'}
                    onChangeText={(text) => {
                        setEmail(text)
                    }}
                />
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Enter password'}
                    value={password}
                    onChangeText={(text) => {
                        setPassword(text)
                    }}
                />
                <Pressable
                    onPress={() => {
                        navigation.navigate('ForgotPasswordScreen')
                    }}
                    style={styles.forgotContainer}>
                    <Text style={styles.forgotText}>Forgot password?</Text>
                </Pressable>
                <View style={styles.socialContainer}>
                    <View style={styles.socialImageContainer}>
                        <Image
                            style={styles.socialIcon}
                            source={icons.googleIcon}
                        />
                    </View>
                    <View style={[styles.socialImageContainer, { marginLeft: 20, marginRight: 20 }]}>
                        <Image
                            style={styles.socialIcon}
                            source={icons.facebookIcon}
                        />
                    </View>
                    <View style={styles.socialImageContainer}>
                        <Image
                            style={styles.socialIcon}
                            source={icons.appleIcon}
                        />
                    </View>
                </View>
                <View style={styles.buttonContainer}>
                    <Button
                        text={'Sign up'}
                        textStyle={{ color: colors.lightGreen }}
                        backgroundColorStyle={styles.loginBtnStyle}
                        clickAction={signUpBtn.bind(this)}
                    />
                    <Button
                        text={'Log in'}
                        // textStyle={{ }}
                        backgroundColorStyle={{ width: '59%' }}
                        clickAction={() => {
                            loginBtn()
                        }}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default LoginScreen;
