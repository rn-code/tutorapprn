import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import Loader from '../../../components/Loader'
import ImagePickerModel from './../../../components/ImagePickerModel'
import moment from "moment";
import { showErrorMsg } from "../../../utils/flashMessage";

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
import { updateProfileAPI } from '../../../api/methods/updateTutor'


const GalleryComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()
  const [profileImage, setProfileImage] = useState('');
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [showPickerModel, setShowPickerModel] = useState(false);
  const [loading, setLoading] = useState(false);
  const [galleryList, setGalleryList] = useState([{}, {}, {}, {}, {}, {}, {}, {}, {}]);

  const handleChoosePhoto = (response) => {
    if (response.didCancel) {
      //// console.log('User cancelled image picker');
    } else if (response.error) {
      //// console.log('ImagePickerModel Error: ', response.error);
    } else {
      const source = { uri: response.uri };
      console.log(response?.assets?.[0]?.uri)
      setProfileImage(response?.assets?.[0]?.uri)
      let tempArray = [...galleryList]
      tempArray[selectedIndex].selectedImage = response?.assets?.[0]?.uri
      setGalleryList(tempArray)
    }
  }
  const updateProfileApi = async () => {
    console.log('updateProfileApi-Gallery-call')
    if (galleryList[0].selectedImage!=undefined) {
      try {
        setLoading(true)
        let formData = new FormData()
        galleryList.forEach(element => {
          formData.append("gallery[]", { uri: element.selectedImage, name: moment().format('x') + '.jpg', type: 'image/jpeg' })
        });
        const response = await updateProfileAPI(formData)
        setLoading(false)
        console.log('updateProfileApi-response', response)
        navigation.navigate('Pricing')
        if (response.status === 200) {
          console.log('updateProfileApi-response', response?.data)
          showErrorMsg('Gallery save successfully')
          // navigation.navigate('HomeStack')            
        }
      } catch (error) {
        setLoading(false)
        showErrorMsg(error?.response?.data?.message)
        console.log('updateProfileApi-error-->', error)
      }
    }
    else {
      showErrorMsg("Please Select atleast 1 image")
    }
  }
  const renderGalleryItem = ({ item, index }) => {
    return (

      <View style={[styles.itemMainContainer, {
        borderWidth: item?.selectedImage != undefined ? 0 : 1
      }]}>
        <Image
          source={{ uri: item?.selectedImage != undefined ? item?.selectedImage : null }}
          style={styles.galleryImage}
        />
        <TouchableOpacity
          onPress={() => {
            setShowPickerModel(true)
            setSelectedIndex(index)
          }}
          style={styles.plusContiner}>
          <Image
            source={icons.plusSignIcon}
            style={styles.plusSignIcon}
          />
        </TouchableOpacity>
      </View>

    );
  };
  const renderEmptyContainer = () => {
    return (
      <View style={[styles.emptyContainer]}>
        <Text style={styles.emptyText}>{'No item exist'}</Text>
      </View>
    )
  };
  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 30, marginBottom: 10 }}
        contentContainerStyle={{ flexGrow: 1 }}>
        <Text style={styles.headerText}>Gallery</Text>
        <View>
          <FlatList
            numColumns={3}
            data={galleryList}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flex: 1 }}
            renderItem={renderGalleryItem}
            ListEmptyComponent={renderEmptyContainer}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </KeyboardAwareScrollView>
      <Button
        text={'Save'}
        // textStyle={{ }}
        backgroundColorStyle={{
          width: '100%',
          shadowOpacity: 0,
          marginBottom: 30
        }}
        clickAction={() => {
          updateProfileApi()
        }}
      />
      <Loader loading={loading} />
      <ImagePickerModel
        showPickerModel={showPickerModel}
        onHideModel={() => {
          setShowPickerModel(false)
        }}
        handleChoosePhoto={handleChoosePhoto}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    paddingLeft: 22,
    paddingRight: 22,
    flex: 1,
    backgroundColor: colors.backgroundColorProfile
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    lineHeight: 36,
  },
  emptyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    width: '31%',
    height: 132,
    marginTop: 10,
    marginLeft: 4,
    marginRight: 4,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  galleryImage: {
    width: '100%',
    height: 132,
    resizeMode: 'cover',
    position: 'absolute',
    borderRadius: 5
  },
  plusSignIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain'
  },
  plusContiner: {
  }
});

export default GalleryComponent;
