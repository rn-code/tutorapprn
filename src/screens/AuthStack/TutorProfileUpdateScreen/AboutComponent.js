import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet,
  Dimensions
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { Picker } from 'native-base';

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import InputField from '../../../components/InputField'
import CheckBox from '../../../components/CustomCheckBox'

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
const { width, height } = Dimensions.get('window')

import { updateProfileAPI } from '../../../api/methods/updateTutor'


const AboutComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [introduction, setIntroduction] = useState("");
  const [longIntro, setLongIntro] = useState("");

  const [currentlyEnroll, setCurrentlyEnroll] = useState(false);
  const [currentlyWorked, setCurrentlyWorked] = useState(false);

  const [instituteName, setInstituteName] = useState("");
  const [degreeTitle, setDegreeTitle] = useState("");
  const [teachingPlace, setTeachingPlace] = useState("");
  const [shortDescription, setShortDescription] = useState("");


  const [educationList, setEducationList] = useState([]);
  const [experienceList, setExperienceList] = useState([]);

  const [selectedCountryField, setSelectedCountryField] = useState('Country')
  const [selectedCityField, setSelectedCityField] = useState('City')
  const [selectedCompletionField, setSelectedCompletionField] = useState('Completion year')
  const [selectedFromYearField, setSelectedFromYearField] = useState('From year')
  const [selectedToYearField, setSelectedToYearField] = useState('To year')

  const onCountryChange = (value: string) => {
    setSelectedCountryField(value)
  }
  const onCityChange = (value: string) => {
    setSelectedCityField(value)
  }
  const onCompletionChange = (value: string) => {
    setSelectedCompletionField(value)
  }
  const onFromYearChange = (value: string) => {
    setSelectedFromYearField(value)
  }
  const onToYearChange = (value: string) => {
    setSelectedToYearField(value)
  }
  const addEducation = () => {
    if (instituteName === "") {
      showErrorMsg("Institute name is required");
    } else if (degreeTitle === "") {
      showErrorMsg("Degree title is required");
    } else if (selectedCountryField === "Country") {
      showErrorMsg("Country is required");
    } else if (selectedCityField === "City") {
      showErrorMsg("City is required");
    } else if (selectedCompletionField === "Completion year") {
      showErrorMsg("completion year is required");
    } else {
      let tempList = [...educationList]
      tempList.push({
        intituteName: instituteName,
        degreeTitle: degreeTitle,
        country: selectedCountryField,
        city: selectedCityField,
        completionYear: selectedCompletionField,
        currentlyEnroll:currentlyEnroll
      })
      setEducationList(tempList)
      setInstituteName('')
      setDegreeTitle('')
      setSelectedCountryField('Country')
      setSelectedCityField('City')
      setSelectedCompletionField('Completion year')
      setCurrentlyEnroll(false)
    }
  }
  const addExperience = () => {
    if (teachingPlace === "") {
      showErrorMsg("Teaching place is required");
    } else if (selectedFromYearField === "From year") {
      showErrorMsg("From year is required");
    } else if (selectedToYearField === "To year") {
      showErrorMsg("To year is required");
    } else if (shortDescription === "") {
      showErrorMsg("Short description is required");
    } else {
      let tempList = [...experienceList]
      tempList.push({
        teachingPlace: teachingPlace,
        fromYear: selectedFromYearField,
        toYear: selectedToYearField,
        shortDescription: shortDescription,
        currentlyWorked:currentlyWorked
      })
      setExperienceList(tempList)
      setTeachingPlace('')
      setShortDescription('')
      setSelectedToYearField('To year')
      setSelectedFromYearField('From year')
      setCurrentlyWorked(false)
    }
  }
  const aboutFormMainValidation=()=>{
    if (introduction === "") {
      showErrorMsg("Short introduction is required");
    } else if (longIntro === "") {
      showErrorMsg("About yourself is required");
    } 
    else if (educationList.length === 0) {
      showErrorMsg("Add atleast 1 Education");
    } 
    else if (experienceList.length === 0) {
      showErrorMsg("Add atleast 1 experience");
    } 
    else{
      updateProfileApi()
    }
  }
  const updateProfileApi = async () => {
    console.log('updateProfileApi-call')
    try {
      setLoading(true)
      let formData = new FormData()
      formData.append("intro", introduction)
      formData.append("about_you", longIntro)
      educationList.forEach(element => {
        formData.append("education[][name_of_institution]", element?.intituteName)
        formData.append("education[][degree_title]", element?.degreeTitle)
        formData.append("education[][country]", element?.country)
        formData.append("education[][city]", element?.city)
        formData.append("education[][completion_year]", element?.completionYear)
        formData.append("education[][currently_enrolled]", element?.currentlyEnroll)
      });
      experienceList.forEach(element => {
        formData.append("experince[][teaching_place]", element?.teachingPlace)
        formData.append("experince[][from_year]", element?.fromYear)
        formData.append("experince[][to_year]", element?.toYear)
        formData.append("experince[][short_description]", element?.shortDescription)
        formData.append("experince[][currently_working_here]", element?.currentlyWorked)
      });
      const response = await updateProfileAPI(formData)
      setLoading(false)
      console.log('updateProfileApi-response', response)
      navigation.navigate('Highlights')            
      if (response.status === 200) {
        console.log('updateProfileApi-response', response?.data)
        showErrorMsg('Information save successfully')
      }
    } catch (error) {
      setLoading(false)
      showErrorMsg(error?.response?.data?.message)
      console.log('updateProfileApi-error-->', error)
    }
  }



  const renderExperienceItem = ({ item, index }) => {
    return (
      <View style={[styles.itemMainContainer]}>
        <View style={styles.expeienceHeadingContainer}>
          <View style={styles.circleStyle} />
          <Text style={styles.insituteName}>{item.teachingPlace}</Text>
        </View>
        <Text style={[styles.yearText, { marginTop: 4, marginLeft: 18 }]}>{item.fromYear+' - '+item.toYear}</Text>
        <Text style={[styles.degreeName, { marginLeft: 18 }]}>{item.shortDescription}</Text>
      </View>
    );
  };
  const renderEmptyContainer = () => {
    return (
      <View style={[styles.emptyContainer]}>
        <Text style={styles.emptyText}>{'No item exist'}</Text>
      </View>
    )
  };

  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginBottom: 10 }}
        contentContainerStyle={{ flexGrow: 1 }}>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Short introduction'}
          // keyboardType={'number-pad'}
          value={introduction}
          onChangeText={(text) => {
            setIntroduction(text)
          }}
        />
        <InputField
          customStyle={[
            styles.inputSearchContainer, {
              height: 150,
              paddingTop: 10,
              paddingBottom: 10,
              textAlignVertical: "top",
            }]}
          customInputStyle={{
            textAlignVertical: "top", flex: 1,
            height: 130,
          }}
          numberOfLines={10}

          multiline={true}
          autoCapitalizes={"none"}
          placeholder={'Write something about yourself'}
          value={longIntro}
          onChangeText={(text) => {
            setLongIntro(text)
          }}
        />
        <Text style={styles.headerText}>Education</Text>
          {
            educationList.map((item, index) => {
            return  <View style={styles.educationMainContainer}>
                <View style={styles.imageContainer}>
                  <Image
                    style={styles.educationIcon}
                    source={icons.educationIcon}
                  />
                </View>
                <View style={styles.rightContainer}>
                  <Text style={styles.insituteName}>{item?.intituteName}</Text>
                  <Text style={styles.degreeName}>{item?.degreeTitle}</Text>
                  <View style={styles.combineBottomContainer}>
                    <View style={styles.itemBottomContainer}>
                      <View style={styles.circleStyle} />
                      <Text style={styles.yearText}>{item?.city+', '+item?.country}</Text>
                    </View>
                    <View style={[styles.itemBottomContainer, { marginLeft: 30 }]}>
                      <View style={styles.circleStyle} />
                      <Text style={styles.yearText}>{item?.completionYear}</Text>
                    </View>
                  </View>
                </View>
              </View>
            })
          }
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Name of Institution'}
          // keyboardType={'number-pad'}
          value={instituteName}
          onChangeText={(text) => {
            setInstituteName(text)
          }}
        />
        <InputField
          customStyle={[styles.inputSearchContainer, { marginTop: 10 }]}
          autoCapitalizes={"none"}
          placeholder={'Degree title'}
          // keyboardType={'number-pad'}
          value={degreeTitle}
          onChangeText={(text) => {
            setDegreeTitle(text)
          }}
        />
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedCountryField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onCountryChange}
          >
            <Picker.Item label="Country" value="Country" />
            <Picker.Item label="United State" value="United State" />
            <Picker.Item label="Pakistan" value="Pakistan" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedCityField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onCityChange}
          >
            <Picker.Item label="City" value="City" />
            <Picker.Item label="Gujranwala" value="Gujranwala" />
            <Picker.Item label="Lahore" value="Lahore" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedCompletionField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onCompletionChange}
          >
            <Picker.Item label="Completion year" value="Completion year" />
            <Picker.Item label="2019" value="2019" />
            <Picker.Item label="1018" value="1018" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <View style={[styles.termsContainer, { marginTop: 30 }]}>
          <CheckBox
            onChange={() => { setCurrentlyEnroll(!currentlyEnroll) }}
            onChangeTerm={() => {
            }}
            customTxt={'Currently enrolled'}
            isChecked={currentlyEnroll}
            tintColor={colors.black}
            checkstyle={{
              backgroundColor: colors.white,
            }}
          />
        </View>
        <Button
          text={'Add'}
          textStyle={{ color: colors.lightOrange }}
          backgroundColorStyle={{
            width: 160,
            backgroundColor: colors.backgroundColorProfile,
            borderWidth: 1,
            borderColor: colors.lightOrange,
            shadowOpacity: 0,
            alignSelf: 'flex-end'
          }}
          clickAction={() => {
            addEducation()
          }}
        />
        <Text style={styles.headerText}>Experience</Text>
        <FlatList
          data={experienceList}
          showsVerticalScrollIndicator={false}
          // contentContainerStyle={{ }}
          renderItem={renderExperienceItem}
          ListEmptyComponent={renderEmptyContainer}
          showsVerticalScrollIndicator={false}
        />
        <InputField
          customStyle={[styles.inputSearchContainer, { marginTop: 10 }]}
          autoCapitalizes={"none"}
          placeholder={'Teaching place'}
          // keyboardType={'number-pad'}
          value={teachingPlace}
          onChangeText={(text) => {
            setTeachingPlace(text)
          }}
        />
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedFromYearField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onFromYearChange}
          >
            <Picker.Item label="From year" value="From year" />
            <Picker.Item label="2010" value="2010" />
            <Picker.Item label="2011" value="2011" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedToYearField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onToYearChange}
          >
            <Picker.Item label="To year" value="To year" />
            <Picker.Item label="2017" value="2017" />
            <Picker.Item label="2018" value="2018" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <InputField
          customStyle={[styles.inputSearchContainer, { marginTop: 10 }]}
          autoCapitalizes={"none"}
          placeholder={'Short description'}
          // keyboardType={'number-pad'}
          value={shortDescription}
          onChangeText={(text) => {
            setShortDescription(text)
          }}
        />
        <View style={[styles.termsContainer, { marginTop: 30 }]}>
          <CheckBox
            onChange={() => { setCurrentlyWorked(!currentlyWorked) }}
            onChangeTerm={() => {
            }}
            customTxt={'Currently Working here'}
            isChecked={currentlyWorked}
            tintColor={colors.black}
            checkstyle={{ backgroundColor: colors.white }}
          />
        </View>
        <Button
          text={'Add'}
          textStyle={{ color: colors.lightOrange }}
          backgroundColorStyle={{
            width: 160,
            backgroundColor: colors.backgroundColorProfile,
            borderWidth: 1,
            borderColor: colors.lightOrange,
            marginBottom: 50,
            shadowOpacity: 0,
            alignSelf: 'flex-end'
          }}
          clickAction={()=>{
            addExperience()
          }}
        />
        <Button
          text={'Save'}
          // textStyle={{ }}
          backgroundColorStyle={{
            width: '100%',
            shadowOpacity: 0,
            marginBottom: 30
          }}
          clickAction={()=>{
            aboutFormMainValidation()
          }}
        />
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    height: '100%',
    paddingLeft: 22,
    paddingRight: 22,
    backgroundColor: colors.backgroundColorProfile
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    marginTop: 30
  },
  descriptionText: {
    fontFamily: fonts.Regular,
    fontSize: 12,
    color: colors.dgColor
  },
  educationMainContainer: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center'
  },
  imageContainer: {
    width: 64,
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.InputFieldBackground,
    borderRadius: 10
  },
  educationIcon: {
    width: 28,
    height: 28,
    resizeMode: 'contain'
  },
  rightContainer: {
    marginLeft: 15
  },
  insituteName: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black
  },
  degreeName: {
    fontSize: 12,
    fontFamily: fonts.Regular,
    color: colors.black,
    marginTop: 4,
  },
  combineBottomContainer: {
    marginTop: 4,
    flexDirection: 'row',
    width: '100%',
  },
  itemBottomContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  circleStyle: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: colors.dgColor,
    marginRight: 10
  },
  yearText: {
    fontSize: 12,
    color: colors.dgColor,
    fontFamily: fonts.Regular
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    marginTop: 10,
    marginBottom: 20
  },
  expeienceHeadingContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  pickerContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '98%',
    alignSelf: 'center',
    backgroundColor: colors.white,
    borderRadius: 10,
    marginTop: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2
  },
  dropDownStyle: {
    width: 10,
    height: 6,
    resizeMode: 'contain',
    right: 18,
    position: 'absolute'
  },
  inputSearchContainer: {
    backgroundColor: colors.white,
    width: '99%',
    marginTop: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: 'center'
  },
  termsContainer: {
    flexDirection: 'row',
    marginTop: 30,
    alignItems: 'center',
  },
});

export default AboutComponent;
