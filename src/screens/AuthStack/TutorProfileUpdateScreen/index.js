import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    FlatList,
    Dimensions,
    Image,
    TouchableOpacity,
    Pressable,
} from "react-native";
import moment from 'moment'
import { SafeAreaView } from 'react-native-safe-area-context'

import { Rating } from 'react-native-ratings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useSelector } from 'react-redux'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Loader from '../../../components/Loader'
import icons from "../../../assets/icons";
import fonts from '../../../assets/fonts'
import Button from "../../../components/Button"
import colors from "../../../utils/colors";
import { styles } from "./Styles";

import HighlightComponent from './HighlightComponent'
import AboutComponent from './AboutComponent'
import GalleryComponent from './GalleryComponent'
import PricingComponent from './PricingComponent'


const { width, height } = Dimensions.get('window')
const TopTab = createMaterialTopTabNavigator();

const MyTopTabs = (props) => {

    useEffect(() => {
    }, [])
    return (
        <TopTab.Navigator
            headerMode={'none'}
            lazy={true}
            backBehavior="initialRoute"
            screenOptions={{
                scrollEnabled: true,
                tabBarActiveTintColor: colors.green,
                style: { backgroundColor: colors.lightGrey },
                tabBarBounces: true,
                tabBarScrollEnabled: true,
                tabBarStyle: {
                    width: 'auto',
                    paddingHorizontal: 10,
                    margin: 0,
                    // borderTopWidth: .8,
                    borderBottomWidth: .8,
                    borderColor: colors.dgColor,
                    marginTop: 15,
                    backgroundColor:colors.backgroundColorProfile
                },
                tabBarItemStyle: {
                    width: 120,
                    height:60,
                },
                tabBarLabelStyle: {
                    fontSize: 12,
                    fontFamily: fonts.Medium,
                    color: colors.dgColor,
                    textTransform: 'capitalize',
                },
                tabBarIndicatorStyle: {
                    backgroundColor: colors.lightOrange,
                    height: 4
                }
            }}>
            <TopTab.Screen
                name={'About'}
                children={(_props) => <AboutComponent {..._props} />}
            />
            <TopTab.Screen
                name={'Highlights'}
                children={(_props) => <HighlightComponent {..._props} />}
            />
            <TopTab.Screen
                name={'Gallery'}
                children={(_props) => <GalleryComponent {..._props} />}
            />
            <TopTab.Screen
                name={'Pricing'}
                children={(_props) => <PricingComponent {..._props} />}
            />
        </TopTab.Navigator>
    );
}

const TutorProfileUpdateScreen = (props) => {

    const { navigation } = props
    const [loading, setLoading] = useState(false);

    useEffect(() => {

    }, [])

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>Tutor Profile</Text>
            </Pressable>
            <View style={styles.topHeaderContainer}>
                <Text style={styles.descriptionText}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Libero hendrerit diam pretium tristique pretium sit est eget. Nunc, lacus, sagittis, non sed libero. At scelerisque ac rhoncus.
                </Text>
            </View>
            <View style={{ flex: 1 }}>
                <MyTopTabs />
            </View>

            <Loader loading={loading} />

        </SafeAreaView>
    );
};

export default TutorProfileUpdateScreen;
