import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet,
  Dimensions
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { Picker } from 'native-base';

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import InputField from '../../../components/InputField'
import CheckBox from '../../../components/CustomCheckBox'

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
import { TouchableOpacity } from "react-native-gesture-handler";
const { width, height } = Dimensions.get('window')


const HighlightComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);


  const [teachingLocationList, setTeachingLocationList] = useState([
    {
      logo: icons.learningIcon,
      name: 'Online class',
    }, {
      logo: icons.locationIcon,
      name: 'At tutor’s home',

    }, {
      logo: icons.homeIcon,
      name: 'At student’s home',

    }
  ]);
  const [classTypeList, setClassTypeList] = useState([
    {
      name: '1 -1 class',
    }, {
      name: 'Group class',
    }
  ]);

  const [selectedLanguageField, setSelectedLanguageField] = useState('Any')
  const [selectedCategoryField, setSelectedCategoryField] = useState('Any')
  const [subjectField, setSubjectField] = useState('Any')
  const [selectedFromYearField, setSelectedFromYearField] = useState('Any')
  const [selectedToYearField, setSelectedToYearField] = useState('Any')



  const onLanguageChange = (value: string) => {
    setSelectedLanguageField(value)
  }
  const onCategoryChange = (value: string) => {
    setSelectedCategoryField(value)
  }
  const onSubjectChange = (value: string) => {
    setSubjectField(value)
  }
  const onFromYearChange = (value: string) => {
    setSelectedFromYearField(value)
  }
  const onToYearChange = (value: string) => {
    setSelectedToYearField(value)
  }
  const renderTeachingLocationItem = ({ item, index }) => {
    return (
      <View
        style={[styles.itemMainContainer]}>
        <Image
          style={styles.logoIconStyle}
          source={item.logo}
        />
        <Text style={styles.itemName}>{item.name}</Text>
        <TouchableOpacity
          onPress={() => {
            let tempArray = [...teachingLocationList]
            if (!tempArray[index]?.isSelected) {
              tempArray[index].isSelected = true
              setTeachingLocationList(tempArray)
            }
            else {
              tempArray[index].isSelected = false
              setTeachingLocationList(tempArray)
            }
          }}
        >
          <Image
            style={styles.unSelectedTick}
            source={item?.isSelected ? icons.activeIcon : icons.unactiveIcon}
          />
        </TouchableOpacity>
      </View>
    );
  };
  const renderClassTypeItem = ({ item, index }) => {
    return (
      <View
        style={[styles.itemMainContainer]}>
        <Text style={[styles.itemName,{marginLeft:0}]}>{item.name}</Text>
        <TouchableOpacity
          onPress={() => {
            let tempArray = [...classTypeList]
            if (!tempArray[index]?.isSelected) {
              tempArray[index].isSelected = true
              setClassTypeList(tempArray)
            }
            else {
              tempArray[index].isSelected = false
              setClassTypeList(tempArray)
            }
          }}
        >
          <Image
            style={styles.unSelectedTick}
            source={item?.isSelected ? icons.activeIcon : icons.unactiveIcon}
          />
        </TouchableOpacity>
      </View>
    );
  };
  const renderEmptyContainer = () => {
    return (
      <View style={[styles.emptyContainer]}>
        <Text style={styles.emptyText}>{'No item exist'}</Text>
      </View>
    )
  };

  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginBottom: 10 }}
        contentContainerStyle={{ flexGrow: 1 }}>
        <Text style={styles.headerText}>Language you can teach in</Text>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedLanguageField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onLanguageChange}
          >
            <Picker.Item label="Select language" value="key0" />
            <Picker.Item label="English" value="key1" />
            <Picker.Item label="Arabic" value="key2" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <Text style={[styles.headerText,{marginBottom:30}]}>From where you can teach?</Text>
        <FlatList
          data={teachingLocationList}
          showsVerticalScrollIndicator={false}
          renderItem={renderTeachingLocationItem}
          ListEmptyComponent={renderEmptyContainer}
          showsVerticalScrollIndicator={false}
        />
        <Text style={styles.headerText}>Main field / Category</Text>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedCategoryField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onCategoryChange}
          >
            <Picker.Item label="Main field e.g. Computer Science" value="key0" />
            <Picker.Item label="Computer Science" value="key1" />
            <Picker.Item label="History" value="key2" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <Text style={styles.headerText}>What do you teach?</Text>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={subjectField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onSubjectChange}
          >
            <Picker.Item label="Select the subjects of your field" value="key0" />
            <Picker.Item label="Computer" value="key1" />
            <Picker.Item label="English" value="key2" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <Button
          text={'Add'}
          textStyle={{ color: colors.lightOrange }}
          backgroundColorStyle={{
            width: 160,
            backgroundColor: colors.backgroundColorProfile,
            borderWidth: 1,
            borderColor: colors.lightOrange,
            marginTop:20,
            shadowOpacity: 0,
            alignSelf: 'flex-end'
          }}
        />
        <Text style={styles.headerText}>Levels you teach</Text>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={subjectField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onSubjectChange}
          >
            <Picker.Item label="Select the student level you teach" value="key0" />
            <Picker.Item label="A Level" value="key1" />
            <Picker.Item label="O Level" value="key2" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <Button
          text={'Add'}
          textStyle={{ color: colors.lightOrange }}
          backgroundColorStyle={{
            width: 160,
            backgroundColor: colors.backgroundColorProfile,
            borderWidth: 1,
            borderColor: colors.lightOrange,
            marginTop:20,
            shadowOpacity: 0,
            alignSelf: 'flex-end'
          }}
        />
        <Text style={styles.headerText}>Age of student you teach</Text>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedFromYearField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onFromYearChange}
          >
            <Picker.Item label="From age" value="key0" />
            <Picker.Item label="2010" value="key1" />
            <Picker.Item label="2011" value="key2" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>
        <View style={styles.pickerContainer}>
          <Picker
            mode="dropdown"
            style={{ width: width, }}
            selectedValue={selectedToYearField}
            textStyle={{ color: colors.dgColor }}
            onValueChange={onToYearChange}
          >
            <Picker.Item label="To age" value="key0" />
            <Picker.Item label="2017" value="key1" />
            <Picker.Item label="2018" value="key2" />
          </Picker>
          {Platform.OS === 'ios' &&
            <Image
              style={styles.dropDownStyle}
              source={icons.dropdownIcon}
            />
          }
        </View>


        <Button
          text={'Add'}
          textStyle={{ color: colors.lightOrange }}
          backgroundColorStyle={{
            width: 160,
            backgroundColor: colors.backgroundColorProfile,
            borderWidth: 1,
            borderColor: colors.lightOrange,
            shadowOpacity: 0,
            marginTop:20,
            alignSelf: 'flex-end'
          }}
        />
        <Text style={[styles.headerText,{marginBottom:20}]}>Class type you offer</Text>
        <FlatList
          data={classTypeList}
          showsVerticalScrollIndicator={false}
          renderItem={renderClassTypeItem}
          ListEmptyComponent={renderEmptyContainer}
          showsVerticalScrollIndicator={false}
        />

        <Button
          text={'Next'}
          // textStyle={{ }}
          backgroundColorStyle={{
            width: '100%',
            shadowOpacity: 0,
            marginTop:60,
          }}
          clickAction={()=>{
            navigation.navigate('Gallery')
          }}
        />
        <Button
          text={'Previous'}
          textStyle={{color:colors.lightOrange}}
          backgroundColorStyle={{
            width: '100%',
            shadowOpacity: 0,
            marginTop:15,
            backgroundColor:colors.backgroundColorProfile,
            marginBottom: 30
          }}
        />
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    height: '100%',
    paddingLeft: 22,
    paddingRight: 22,
    backgroundColor: colors.backgroundColorProfile
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    marginTop: 30
  },
  itemName: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black,
    marginLeft: 20,
    width: '50%',
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    marginTop: 10,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  pickerContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '99%',
    alignSelf: 'center',
    backgroundColor: colors.white,
    borderRadius: 10,
    marginTop: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2
  },
  dropDownStyle: {
    width: 10,
    height: 6,
    resizeMode: 'contain',
    right: 18,
    position: 'absolute'
  },
  logoIconStyle: {
    width: 38,
    height: 38,
    resizeMode: 'contain'
  },
  unSelectedTick: {
    width: 32,
    height: 32,
    resizeMode: 'contain'
  }
});

export default HighlightComponent;
