import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  FlatList,
  StyleSheet,
  Dimensions
} from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useDispatch } from "react-redux";
import { Picker } from 'native-base';

import Button from '../../../components/Button'
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import InputField from '../../../components/InputField'
import CheckBox from '../../../components/CustomCheckBox'

import colors from "../../../utils/colors";
import fonts from '../../../assets/fonts'
import icons from '../../../assets/icons'
import { TouchableOpacity } from "react-native-gesture-handler";
const { width, height } = Dimensions.get('window')

import { updateProfileAPI } from '../../../api/methods/updateTutor'

const PricingComponent = (props) => {
  const { navigation } = props
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false);
  const [currentlyEnroll, setCurrentlyEnroll] = useState(false);
  const [hourlyRate, setHourlyRate] = useState('');
  const [thirtyMinute, setThirtyMinute] = useState('');
  const [weeklyPackage, setWeeklyPackage] = useState('');
  const [fortnightlyPackage, setFortnightlyPackage] = useState('');
  const [monthlyRate, setMonthlyRate] = useState('');
  const [threeGroupRate, setThreeGroupRate] = useState('');
  const [fiveGroupRate, setFiveGroupRate] = useState('');
  const [sevenGroupRate, setSevenGroupRate] = useState('');


  const updateProfileApi = async () => {
    console.log('updateProfileApi-Pricing-call')
      try {
        setLoading(true)
        let formData = new FormData()
        galleryList.forEach(element => {
          formData.append("gallery", { uri: galleryList.selectedImage, name: moment().format('x') + '.jpg', type: 'image/jpeg' })
        });
        const response = await updateProfileAPI(formData)
        setLoading(false)
        console.log('updateProfileApi-response', response)
        if (response.status === 200) {
          console.log('updateProfileApi-response', response?.data)
          showErrorMsg('Gallery save successfully')
          // navigation.navigate('HomeStack')            
        }
      } catch (error) {
        setLoading(false)
        showErrorMsg(error?.response?.data?.message)
        console.log('updateProfileApi-error-->', error)
      }
    
  
  }

  return (
    <View style={styles.safeStyle}>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginBottom: 10 }}
        contentContainerStyle={{ flexGrow: 1 }}>

        <Text style={styles.headerText}>Hourly rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Write your hourly rate for teaching'}
          // keyboardType={'number-pad'}
          value={hourlyRate}
          onChangeText={(text) => {
            setHourlyRate(text)
          }}
        />
        <Text style={styles.headerText}>30 minute rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Write your rate for 30 minutes for teaching'}
          // keyboardType={'number-pad'}
          value={thirtyMinute}
          onChangeText={(text) => {
            setThirtyMinute(text)
          }}
        />
        <Text style={styles.headerText}>Weekly package rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Write your rate for a week'}
          // keyboardType={'number-pad'}
          value={weeklyPackage}
          onChangeText={(text) => {
            setWeeklyPackage(text)
          }}
        />
        <Text style={styles.headerText}>Fortnightly package rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Write your rate for a fortnight'}
          // keyboardType={'number-pad'}
          value={fortnightlyPackage}
          onChangeText={(text) => {
            setFortnightlyPackage(text)
          }}
        />
        <Text style={styles.headerText}>Monthly package rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Write your rate for a month'}
          // keyboardType={'number-pad'}
          value={monthlyRate}
          onChangeText={(text) => {
            setMonthlyRate(text)
          }}
        />
        <Text style={styles.headerText}>3-student group rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Write your rate for a group of up to 3 students'}
          // keyboardType={'number-pad'}
          value={threeGroupRate}
          onChangeText={(text) => {
            setThreeGroupRate(text)
          }}
        />
        <Text style={styles.headerText}>5-student group rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Write your rate for a group of up to 5 students'}
          // keyboardType={'number-pad'}
          value={fiveGroupRate}
          onChangeText={(text) => {
            setFiveGroupRate(text)
          }}
        />
        <Text style={[styles.headerText]}>7-student group rate</Text>
        <InputField
          customStyle={styles.inputSearchContainer}
          autoCapitalizes={"none"}
          placeholder={'Write your rate for a group of up to 7 students'}
          // keyboardType={'number-pad'}
          value={sevenGroupRate}
          onChangeText={(text) => {
            setSevenGroupRate(text)
          }}
        />
        <View style={[styles.termsContainer, { marginTop: 30 }]}>
          <CheckBox
            onChange={() => { setCurrentlyEnroll(!currentlyEnroll) }}
            onChangeTerm={() => {
            }}
            customTxt={'I offer a demo lesson too for my students.'}
            isChecked={currentlyEnroll}
            tintColor={colors.black}
            checkstyle={{
              backgroundColor: colors.white,
            }}
          />
        </View>
        <Button
          text={'Done'}
          // textStyle={{ }}
          backgroundColorStyle={{
            width: '100%',
            shadowOpacity: 0,
            marginTop: 30,
          }}
        />
        <Button
          text={'Previous'}
          textStyle={{ color: colors.lightOrange }}
          backgroundColorStyle={{
            width: '100%',
            shadowOpacity: 0,
            marginTop: 15,
            backgroundColor: colors.backgroundColorProfile,
            marginBottom: 30
          }}
        />
      </KeyboardAwareScrollView>
      <Loader loading={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  safeStyle: {
    width: '100%',
    height: '100%',
    paddingLeft: 22,
    paddingRight: 22,
    backgroundColor: colors.backgroundColorProfile
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 18,
    marginTop: 30
  },
  itemName: {
    fontSize: 12,
    fontFamily: fonts.Bold,
    color: colors.black,
    marginLeft: 20,
    width: '50%',
  },
  emptyContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.SemiBold,
    color: colors.dgColor
  },
  itemMainContainer: {
    marginTop: 10,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  pickerContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '99%',
    alignSelf: 'center',
    backgroundColor: colors.white,
    borderRadius: 10,
    marginTop: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2
  },
  dropDownStyle: {
    width: 10,
    height: 6,
    resizeMode: 'contain',
    right: 18,
    position: 'absolute'
  },
  logoIconStyle: {
    width: 38,
    height: 38,
    resizeMode: 'contain'
  },
  unSelectedTick: {
    width: 32,
    height: 32,
    resizeMode: 'contain'
  },
  termsContainer: {
    flexDirection: 'row',
    marginTop: 30,
    alignItems: 'center',
  },
  inputSearchContainer: {
    backgroundColor: colors.white,
    width: '99%',
    marginTop: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: 'center'
  },
});

export default PricingComponent;
