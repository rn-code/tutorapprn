import { StyleSheet, Dimensions } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'

const { width, height } = Dimensions.get('window')
export const styles = StyleSheet.create({
  safeStyle: {
    flex: 1,
    backgroundColor:colors.backgroundColorProfile
  },
  backBtnContainer: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20
  },
  backImage: {
    width: 19,
    height: 19,
    resizeMode: 'contain',
    marginRight: 13
  },
  headerText: {
    fontFamily: fonts.Bold,
    fontSize: 20,
    lineHeight: 36,
  },
  topHeaderContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 10
  },
  personCardContainer: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
  rightContainer: {
    width: width - 140,
    paddingLeft: 15,
    // backgroundColor:'red'
  },
  descriptionText: {
    fontSize: 12,
    fontFamily: fonts.Medium,
    color: colors.dgColor,  }
});
