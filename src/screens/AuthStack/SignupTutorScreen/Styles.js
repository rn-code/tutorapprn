import { StyleSheet } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'
export const styles = StyleSheet.create({
  safeStyle: {
    width:'100%',
    height:'100%',
    paddingLeft:22,
    paddingRight:22
  },
  headerText:{
    fontFamily:fonts.Bold,
    fontSize:24,
    lineHeight:36,
    marginTop:15
  },
  headerDescriptiveContainer:{
    flexDirection:'row',
    marginTop:20
  },
  mediumText:{
    fontSize:12,
    fontFamily:fonts.Medium,
    color:colors.dgColor
  },
  largeText:{
    fontSize:12,
    fontFamily:fonts.Bold,
    color:colors.lightGreen
  },
  fieldContainer:{
    alignItems:'center'
  },
  circleContainer:{
    width:163,
    height:163,
    borderRadius:80,
    backgroundColor:colors.lightGreen,
    opacity:.5,
    alignItems:'center',
    justifyContent:'center'
  },
  signupLogoStyle:{
    width:56,
    height:56,
    resizeMode:'contain'
  },
  topBarcontainer:{
    flexDirection:'row',
    width:'100%',
    marginTop:30
  },
  activeStyle:{
    width:'50%',
    padding:16,
    backgroundColor:colors.lightGreen,
    borderWidth:.7,
    borderRightWidth:.7,
    borderColor:colors.green
  },
  unActiveStyle:{
    width:'50%',
    padding:16,
    borderWidth:.7,
    borderRightWidth:.7,
    borderColor:colors.borderColor
  },
  activeTextStyle:{
      fontFamily:fonts.Bold,
      fontSize:12,  
      textAlign:'center',
      color:colors.white
  },
  unActiveTextStyle:{
      fontFamily:fonts.Regular,
      fontSize:12,  
      textAlign:'center',
      color:colors.borderColor
  },
  termsContainer: {
    flexDirection: 'row',
    marginTop: 30,
    alignItems: 'center',
  },
  socialContainer:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    marginTop:50
  },
  socialImageContainer:{
    width:50,
    height:50,
    borderRadius:10,
    borderWidth:.8,
    borderColor:'#0A0D31',
    justifyContent:'center',
    alignItems:'center'
  },
  socialIcon:{
    width:21,
    height:21,
    resizeMode:'contain'
  },
  buttonContainer:{
    flexDirection:'row',
    marginTop:50,
    justifyContent:'space-between',
    marginBottom:30
  },
  loginBtnStyle:{
    width:'34%',
    backgroundColor:colors.white,
    borderWidth:.8,
    borderColor:colors.lightGreen
 }
});
