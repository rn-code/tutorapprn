import { StyleSheet } from 'react-native';
import colors from '../../../utils/colors'
import fonts from '../../../assets/fonts'
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.personBackground,
    paddingLeft: 22,
    paddingRight: 22,
    alignItems: "center",
  },
  headingText: {
    fontSize: 24,
    fontFamily: fonts.Bold,
    color: colors.black,
    marginTop: 42,
    lineHeight: 36,
    textAlign: 'center'
  },
  descriptiveText: {
    fontSize: 12,
    fontFamily: fonts.Regular,
    color: colors.dgColor,
    lineHeight: 18,
    textAlign: 'center',
    width: '55%',
    alignSelf:'center'
  },
  cardContainer: {
    backgroundColor: colors.white,
    width: '100%',
    marginTop:10,
    borderRadius: 15,
    padding: 28,
    paddingBottom: 17,
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    },
  cardImage: {
    width: '100%',
    height: 135,
    resizeMode: 'contain',
  },
  cardText: {
    fontFamily: fonts.Bold,
    lineHeight: 27,
    marginTop: 25
  }
});
