import React, { useEffect } from "react";
import { Image, ImageBackground, View, Text, Pressable } from "react-native";
import { styles } from './Styles';
import { SafeAreaView } from 'react-native-safe-area-context'
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useSelector } from 'react-redux'

//assets
import icons from '../../../assets/icons'
const PersonSelectionScreen = ( props) => {
    const {navigation}=props
    const { isSignedIn } = useSelector((state) => state.userSession)

    useEffect(() => {
        // console.log('isSignedIn',isSignedIn)
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ width: '100%', paddingBottom: 20 }}>
                <Text style={styles.headingText}>Are you a ...?</Text>
                <Text style={styles.descriptiveText}>
                    Please select an option for registration
                </Text>
                <Pressable
                    onPress={() => {
                        navigation.navigate('SignupScreen')
                    }}
                    style={[styles.cardContainer, { marginTop: 20 }]}
                >
                    <Image
                        style={styles.cardImage}
                        source={icons.studentIcon}
                    />
                    <Text style={styles.cardText}>Student / Parent</Text>
                </Pressable>
                <Pressable
                    onPress={() => {
                        navigation.navigate('SignupTutorScreen')
                    }}
                    style={styles.cardContainer}
                >
                    <Image
                        style={styles.cardImage}
                        source={icons.tutorIcon}
                    />
                    <Text style={styles.cardText}>Tutor</Text>
                </Pressable>
            </KeyboardAwareScrollView>
        </SafeAreaView>
    );
};
export default PersonSelectionScreen;
