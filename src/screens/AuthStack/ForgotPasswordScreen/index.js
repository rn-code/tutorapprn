import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import icons from '../../../assets/icons'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";


import { forgetPasswordAPI } from '../../../api/methods/auth'

const ForgotPasswordScreen = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState("");

    const resetBtn = async () => {
        if (email === "") {
            showErrorMsg("Email is required");
        } else if ((/^\w+([\.+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/).test(email.trim()) == false) {
            showErrorMsg('Email format is invalid')
        } else {
            forgotApi()
        }
    };


    const forgotApi = async () => {
        try {
            setLoading(true)
            const response = await forgetPasswordAPI({
                "email": email.toLowerCase(),
            })
            setLoading(false)
            if (response.status === 200) {
                setEmail('')
                console.log('loginAPI-response', response?.data)
                navigation.navigate('OtpScreen',{emailParam:email})
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('loginAPI-error-->', error)
        }
    }


    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>Forgot password?</Text>
            </Pressable>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 30 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.fieldContainer}>
                    <Image
                        style={styles.signupLogoStyle}
                        source={icons.forgotIcon}
                    />
                    <Text style={[
                        styles.mediumText,
                        { marginTop: 5, width: '50%' }]}>
                        Please provide your email for verification.
                    </Text>
                </View>
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Enter email address'}
                    value={email}
                    onChangeText={(text) => {
                        setEmail(text)
                    }}
                />
                <View style={styles.buttonContainer}>
                    <Button
                        text={'Reset'}
                        // textStyle={{ }}
                        backgroundColorStyle={{}}
                        clickAction={() => {
                            resetBtn()
                        }}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default ForgotPasswordScreen;
