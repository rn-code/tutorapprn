import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

//Screens
import LoginScreen from './LoginScreen'
import SignupScreen from './SignupScreen'
import SignupTutorScreen from './SignupTutorScreen'
import PersonSelectionScreen from './PersonSelectionScreen'
import ForgotPasswordScreen from './ForgotPasswordScreen'
import AddKidScreen from './AddKidScreen'
import ResetScreen from './ResetScreen'
import OtpScreen from './OtpScreen'
import TutorProfileUpdateScreen from './TutorProfileUpdateScreen'

/** Auth Stack of the app */
export default AuthStack = () => (
    <Stack.Navigator headerMode="none" initialRouteName='PersonSelectionScreen'>
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="SignupScreen" component={SignupScreen} />
        <Stack.Screen name="SignupTutorScreen" component={SignupTutorScreen} />
        <Stack.Screen name="PersonSelectionScreen" component={PersonSelectionScreen} />
        <Stack.Screen name="ForgotPasswordScreen" component={ForgotPasswordScreen} />
        <Stack.Screen name="ResetScreen" component={ResetScreen} />
        <Stack.Screen name="OtpScreen" component={OtpScreen} />
        {/* <Stack.Screen name="AddKidScreen" component={AddKidScreen} />
        <Stack.Screen name="TutorProfileUpdateScreen" component={TutorProfileUpdateScreen} /> */}
    </Stack.Navigator>
);