import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import icons from '../../../assets/icons'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";

import { otpVerifyPasswordAPI } from '../../../api/methods/auth'


const OtpScreen = (props) => {
    const { navigation } = props
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [code, setCode] = useState("");

    useEffect(()=>{
        console.log(props?.route?.params?.emailParam)
    },[])

    const onResetPress = async () => {
        if (code === "") {
            showErrorMsg("Code is required");
        } else {
            verifyOtpApi()
        }
    };
    const verifyOtpApi = async () => {
        try {
            setLoading(true)
            const response = await otpVerifyPasswordAPI({
                "email": props?.route?.params?.emailParam,
                "otp": code,
            })
            setLoading(false)
            if (response.status === 200) {
                setCode('')
                console.log('loginAPI-response', response?.data)
                navigation.navigate('ResetScreen',{
                    emailResetPassword: props?.route?.params?.emailParam
                })
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('loginAPI-error-->', error)
        }
    }

    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>OTP verification</Text>
            </Pressable>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 30 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.fieldContainer}>
                    <Image
                        style={styles.signupLogoStyle}
                        source={icons.forgotIcon}
                    />
                    <Text style={[
                        styles.mediumText,
                        { marginTop: 5}]}>
                        Please provide OTP verification code sent to your email.
                    </Text>
                </View>
                <InputField
                    autoCapitalizes={"none"}
                    keyboardType={'number-pad'}
                    placeholder={'Enter code'}
                    value={code}
                    onChangeText={(text) => {
                        setCode(text)
                    }}
                />
                <Pressable
                    style={styles.forgotContainer}>
                    <Image 
                    source={icons.resetIcon}
                    style={styles.resetIcon}
                    />
                    <Text style={styles.forgotText}>Resend email</Text>
                </Pressable>
                <View style={styles.buttonContainer}>
                    <Button
                        text={'Reset'}
                        // textStyle={{ }}
                        backgroundColorStyle={{ }}
                        clickAction={()=>{
                            onResetPress()
                        }}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default OtpScreen;
