import { StyleSheet } from 'react-native';
import fonts from '../../../assets/fonts';
import colors from '../../../utils/colors'
export const styles = StyleSheet.create({
  safeStyle: {
    width:'100%',
    height:'100%',
    paddingLeft:20,
    paddingRight:20
  },
  headerText:{
    fontFamily:fonts.Bold,
    fontSize:24,
    lineHeight:36,
  },
  backBtnContainer:{
    flexDirection:'row',
    marginTop:15,
    alignItems:'center'
  },
  headerDescriptiveContainer:{
    flexDirection:'row',
    marginTop:20
  },
  mediumText:{
    fontSize:16,
    fontFamily:fonts.Medium,
    color:colors.dgColor
  },
  largeText:{
    fontSize:12,
    fontFamily:fonts.Bold,
    color:colors.lightGreen
  },
  fieldContainer:{
    marginBottom:30
  },
  buttonContainer:{
    flexDirection:'row',
    marginTop:80,
    justifyContent:'space-between',
    marginBottom:30,
    width:'98%'
  },
 forgotContainer:{
  alignSelf:'flex-start' ,
  marginTop:30  
},
forgotText:{
  fontFamily:fonts.Regular,
  color:colors.black,
  fontSize:14
},
individualText:{
  fontFamily:fonts.Bold,
  color:colors.lightOrange,
  fontSize:14
},
backImage:{
  width:19,
  height:19,
  resizeMode:'contain',
  marginRight:13
},
dropDownStyle:{
  width:10,
  height:6,
  resizeMode:'contain',
  right:18,
  position:'absolute'
},
pickerContainer:{
  height: 50,
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '100%',
  backgroundColor: colors.InputFieldBackground,
  borderRadius: 10,
  marginTop: 10
},
emptyText:{
  fontFamily:fonts.Bold,
  fontSize:14,
  color:colors.lightOrange
},
emptyContainer:{
  marginBottom: 30,
  borderRadius:10,
  backgroundColor:colors.kidItemBackground,
  padding:20,
  paddingTop:15,
  paddingBottom:15
},
itemMainContainer:{
  flexDirection:'row',
  backgroundColor:colors.kidItemBackground,
  padding:20,
  paddingTop:15,
  paddingBottom:15,
  borderRadius:10,
  marginRight:10

},
nameText:{
  fontFamily:fonts.Bold,
  fontSize:14,
  color:colors.lightOrange
},
mainBtnContainer:{
  flexDirection:'row',
  justifyContent:'center',
  alignItems:'center'
},
imageItem:{
  width:18,
  height:18,
  resizeMode:'contain',
  marginLeft:15
}
});
