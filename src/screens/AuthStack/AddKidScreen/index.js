import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable,
    Platform,
    TouchableOpacity,
    Dimensions,
    FlatList
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Picker } from 'native-base';

import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import icons from '../../../assets/icons'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";
import fonts from "../../../assets/fonts";

import {
    addKidAPI,
    updateKidAPI,
    removeKidAPI,
    geKidDataAPI
} from '../../../api/methods/kidOperation'
import moment from "moment";

const { width, height } = Dimensions.get('window')

const AddKidScreen = (props) => {

    const { navigation, route } = props
    const dispatch = useDispatch()
    const isParentFunction = route.params?.isParentFunction
    const [kidsList, setKidsList] = useState([]);

    const [loading, setLoading] = useState(false);
    const [name, setName] = useState("");
    const [lastName, setLastName] = useState("");
    const [age, setAge] = useState("");
    const [studenClass, setStudenClass] = useState("");
    const [selected, setSelected] = useState('Enter gender')
    const [isEdit, setIsEdit] = useState(false)
    const [updateId, setUpdateId] = useState(null)

    const [pickerList, setPickerList] = useState([])
    const [selectedClassLocation, setSelectedClassLocation] = useState("Enter age (e.g. 10)")
    const [selectedLevelEducation, setSelectedLevelEducation] = useState("Enter level of education")

    useEffect(() => {
    }, [])

    const onAddBtnPress = async () => {
        if (name === "") {
            showErrorMsg("Name is required");
        } else if (lastName === "") {
            showErrorMsg("Last Name is required");
        } else if (selectedClassLocation === "Enter age (e.g. 10)") {
            showErrorMsg("Age is required");
        } else if (selected === "Enter gender") {
            showErrorMsg("gender is required");
        } else if (selectedLevelEducation === "Enter level of education") {
            showErrorMsg("Level of education is required");
        } else {
            if(isEdit){
                updateKidApi()
            }
            else{
                addKidApi()
            }
        }
    };

    const getKidApi = async () => {
        try {
            setLoading(true)
            const response = await geKidDataAPI()
            setLoading(false)
            if (response.status === 200) {
                console.log('getKidApi-response', response?.data?.kids)
                setKidsList(response?.data?.kids)

                setName('')
                setLastName('')
                setAge('')
                // setStudenClass('')
                setIsEdit(false)
                setUpdateId(null)
                setSelected('Enter gender')
                setSelectedClassLocation('Enter age (e.g. 10)')
                setSelectedLevelEducation("Enter level of education")
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('getKidApi-error-->', error)
        }
    }
    const addKidApi = async () => {
        try {
            setLoading(true)
            console.log(name, lastName, age, selected, studenClass)
            const response = await addKidAPI({
                'first_name': name,
                'last_name': lastName,
                'age': selectedClassLocation,
                'class_name': selectedLevelEducation,
                'gender': selected,
                image: ''
            })
            setLoading(false)
            if (response.status === 200) {
                setName('')
                setLastName('')
                setSelected('Enter gender')
                setSelectedClassLocation('Enter age (e.g. 10)')
                setSelectedLevelEducation("Enter level of education")
                getKidApi()
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('signUpApi-error-->', error)
        }
    }
    const updateKidApi = async () => {
        try {
            setLoading(true)
            let paramObj = {
                'first_name': name,
                'last_name': lastName,
                'age': age,
                'class_name': selected,
                'gender': studenClass
            }
            const response = await updateKidAPI({
                'first_name': name,
                'last_name': lastName,
                'age': selectedClassLocation,
                'class_name': selectedLevelEducation,
                'gender': selected,
                'id': updateId
            })
            setLoading(false)
            console.log(response)
            if (response?.status === 200) {
                setName('')
                setLastName('')
                setAge('')
                setStudenClass('')
                setIsEdit(false)
                setSelected('Enter gender')
                setSelectedClassLocation('Enter age (e.g. 10)')
                setSelectedLevelEducation("Enter level of education")
                getKidApi()
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('updateKidApi-error-->', error)
        }
    }
    const deleteKidApi = async (item) => {
        try {
            console.log(item?._id)
            setLoading(true)
            const response = await removeKidAPI(item?._id)
            setLoading(false)
            if (response.status === 200) {
                getKidApi()
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('deleteKidApi-error-->', error)
        }
    }

    const onValueChange = (value: string) => {
        setSelected(value)
    }
    const onValueChangeLevelEducation = (value: string) => {
        setSelectedLevelEducation(value)
    }
    const onClassLocationChange = (value: string) => {
        setSelectedClassLocation(value)
    }
    const renderKidItem = ({ item, index }) => {
        return (
            <View style={styles.itemMainContainer}>
                <Text style={styles.nameText}>{item.first_name}</Text>
                <View style={styles.mainBtnContainer}>
                    <TouchableOpacity
                        onPress={() => {
                            console.log(item.first_name, item.last_name, item.age, item.gender, item.class_name)
                            setName(item.first_name)
                            setLastName(item.last_name)
                            // setAge(item.age + '')
                            setSelectedClassLocation(item.age + '')
                            setSelected(item.gender)
                            // setStudenClass(item.class_name)
                            setSelectedLevelEducation(item.class_name)
                            setIsEdit(true)
                            setUpdateId(item._id)
                        }}
                    >
                        <Image
                            source={icons.editIcon}
                            style={[styles.imageItem, { marginLeft: 30 }]}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            deleteKidApi(item)
                        }}
                    >
                        <Image
                            source={icons.deleteIcon}
                            style={styles.imageItem}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
    const renderEmptyContainer = () => {
        return (
            <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>No student added yet</Text>
            </View>
        )
    };
    return (
        <SafeAreaView style={styles.safeStyle}>
            <Pressable
                onPress={() => {
                    navigation.goBack()
                }}
                style={styles.backBtnContainer}>
                <Image
                    style={styles.backImage}
                    source={icons.backIcon}
                />
                <Text style={styles.headerText}>Add kids</Text>
            </Pressable>
            <Text style={[styles.mediumText, { marginTop: 10 }]}>
                Add your kids as students. You can also add them later in settigns.
            </Text>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 30 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.fieldContainer}>
                    <FlatList
                        horizontal
                        data={kidsList}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ width: "90%", alignSelf: "center", marginBottom: 20 }}
                        renderItem={renderKidItem}
                        ListEmptyComponent={renderEmptyContainer}
                        showsVerticalScrollIndicator={false}
                    />
                    <InputField
                        autoCapitalizes={"none"}
                        placeholder={'Enter first name of student'}
                        value={name}
                        onChangeText={(text) => {
                            setName(text)
                        }}
                    />
                    <InputField
                        autoCapitalizes={"none"}
                        placeholder={'Enter last name of student'}
                        value={lastName}
                        onChangeText={(text) => {
                            setLastName(text)
                        }}
                    />
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width}}
                            selectedValue={selectedClassLocation}
                            style={{
                                width:'100%',
                                color: selectedClassLocation === "Enter age (e.g. 10)" ? colors.dgColor : colors.black,
                                fontFamily: fonts.Medium
                            }}
                            onValueChange={onClassLocationChange}
                        >
                            <Picker.Item color={colors.dgColor} label="Enter age (e.g. 10)" value="Enter age (e.g. 10)" />
                            <Picker.Item color={colors.dgColor} label="2" value="2" />
                            <Picker.Item color={colors.dgColor} label="3" value="3" />
                            <Picker.Item color={colors.dgColor} label="4" value="4" />
                            <Picker.Item color={colors.dgColor} label="5" value="5" />
                            <Picker.Item color={colors.dgColor} label="6" value="6" />
                            <Picker.Item color={colors.dgColor} label="7" value="7" />
                            <Picker.Item color={colors.dgColor} label="8" value="8" />
                            <Picker.Item color={colors.dgColor} label="9" value="9" />
                            <Picker.Item color={colors.dgColor} label="10" value="10" />
                            <Picker.Item color={colors.dgColor} label="11" value="11" />
                            <Picker.Item color={colors.dgColor} label="12" value="12" />
                            <Picker.Item color={colors.dgColor} label="13" value="13" />
                            <Picker.Item color={colors.dgColor} label="14" value="14" />
                            <Picker.Item color={colors.dgColor} label="15" value="15" />
                            <Picker.Item color={colors.dgColor} label="16" value="16" />
                            <Picker.Item color={colors.dgColor} label="17" value="17" />
                            <Picker.Item color={colors.dgColor} label="18" value="18" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selected}
                            style={{color: selected === "Enter gender" ? colors.dgColor : colors.black,
                            fontFamily: fonts.Medium }}
                            
                            onValueChange={onValueChange}
                        >
                            <Picker.Item color={colors.dgColor} label="Enter gender" value="Enter gender" />
                            <Picker.Item color={colors.dgColor} label="Male" value="male" />
                            <Picker.Item color={colors.dgColor} label="Female" value="female" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
                    <View style={styles.pickerContainer}>
                        <Picker
                            mode="dropdown"
                            style={{ width: width, }}
                            selectedValue={selectedLevelEducation}
                            style={{color: selectedLevelEducation === "Enter level of education" ? colors.dgColor : colors.black,
                            fontFamily: fonts.Medium }}
                            onValueChange={onValueChangeLevelEducation}
                        >
                            <Picker.Item color={colors.dgColor} label="Enter level of education" value="Enter level of education" />
                            <Picker.Item color={colors.dgColor} label="Pre-primary School" value="Pre-primary School" />
                            <Picker.Item color={colors.dgColor} label="Primary School" value="Primary School" />
                            <Picker.Item color={colors.dgColor} label="Middle School" value="Middle School" />
                            <Picker.Item color={colors.dgColor} label="High School" value="High School" />
                        </Picker>
                        {Platform.OS === 'ios' &&
                            <Image
                                style={styles.dropDownStyle}
                                source={icons.dropdownIcon}
                            />
                        }
                    </View>
               
                    <Pressable
                        onPress={() => {
                            if (isParentFunction && typeof isParentFunction == 'function') {
                                isParentFunction()
                                navigation.navigate('SignupScreen')
                            }
                        }}
                        style={styles.forgotContainer}>
                        <Text style={styles.forgotText}>
                            Not a parent? <Text style={styles.individualText}>
                                Sign up as an individual.
                            </Text>
                        </Text>
                    </Pressable>

                    <View style={styles.buttonContainer}>
                        <Button
                            text={'Add'}
                            // textStyle={{ }}
                            backgroundColorStyle={{ width: '100%' }}
                            clickAction={() => {
                                // if (isEdit) {
                                //     console.log('if isEdit', isEdit)
                                //     updateKidApi()
                                // } else {
                                //     console.log('else isEdit', isEdit)
                                //     onAddBtnPress()
                                // }
                                onAddBtnPress()
                            }}
                        />
                    </View>
                    {kidsList.length > 0 &&
                        <View style={[styles.buttonContainer, { marginTop: 0 }]}>
                            <Button
                                text={'Next'}
                                // textStyle={{ }}
                                backgroundColorStyle={{ width: '100%' }}
                                clickAction={() => {
                                    navigation.navigate('HomeDrawerStack')
                                }}
                            />
                        </View>
                    }
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default AddKidScreen;
