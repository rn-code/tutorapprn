import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    Image,
    Pressable
} from "react-native";
import { styles } from "./Styles";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useIsFocused } from '@react-navigation/native'
import InputField from '../../../components/InputField'
import Button from '../../../components/Button'
import Header from '../../../components/Header'
import CheckBox from '../../../components/CustomCheckBox'
import icons from '../../../assets/icons'

import { useDispatch } from "react-redux";
import Loader from '../../../components/Loader'
import { showErrorMsg } from "../../../utils/flashMessage";
import colors from "../../../utils/colors";


import { sigupAPI } from '../../../api/methods/auth'
import { onSignupResponse } from '../../../actions/userSession'


const SignupScreen = (props) => {

    const { navigation, route } = props
    const { params } = route
    const dispatch = useDispatch()
    const isFocus = useIsFocused()

    const [loading, setLoading] = useState(false);
    const [isParent, setIsParent] = useState(false);
    const [name, setName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [checkBox, setBox] = useState(false);

    useEffect(() => {
    }, [])

    const signUpBtn = async () => {
        if (name === "") {
            showErrorMsg("Name is required");
        } else if (lastName === "") {
            showErrorMsg("Last Name is required");
        } else if (email === "") {
            showErrorMsg("Email is required");
        } else if ((/^\w+([\.+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/).test(email.trim()) == false) {
            showErrorMsg('Email format is invalid')
        } else if (password === "") {
            showErrorMsg("Password is required");
        } else if (password.length < 6) {
            showErrorMsg("Password must be atleast 6 character");
        } else if (!checkBox) {
            showErrorMsg("Please accept the terms and condition");
        } else {
            signUpApi()
        }
    };
    const signUpApi = async () => {
        try {
            setLoading(true)
            const response = await sigupAPI({
                'user_type': isParent ? 'parent' : 'individual',
                'first_name': name,
                'last_name': lastName,
                "email": email.toLowerCase(),
                "password": password,
            })
            setLoading(false)
            if (response.status === 201) {
                setName('')
                setLastName('')
                setEmail('')
                setPassword('')
                setBox(false)
                console.log('signUpApi-response', response?.data)
                dispatch(onSignupResponse(response.data))
                if (isParent) {
                    setTimeout(() => {
                        navigation.navigate('AddKidScreen', {
                            isParentFunction: () => {
                                setIsParent(false)
                            }
                        })
                    }, 30)

                } else {
                    navigation.navigate('AfterSplash')
                }
            }
        } catch (error) {
            setLoading(false)
            showErrorMsg(error?.response?.data?.message)
            console.log('signUpApi-error-->', error)
        }
    }
    const loginBtn = () => {
        navigation.navigate('LoginScreen')
    }
    return (
        <SafeAreaView style={styles.safeStyle}>
            <Text style={styles.headerText}>Sign up</Text>
            <View style={styles.headerDescriptiveContainer}>
                {/* <Text style={styles.mediumText}>Tutor   |</Text> */}
                <Text style={styles.largeText}>Student / Parent</Text>
            </View>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 30 }}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.fieldContainer}>
                    <View style={styles.circleContainer}>
                        <Image
                            style={styles.signupLogoStyle}
                            source={icons.signupLogoIcon}
                        />
                    </View>
                    <Text style={styles.headerText}>Welcome!</Text>
                    <Text style={[styles.mediumText, { marginTop: 5 }]}>
                        You are a few clicks away.
                    </Text>
                </View>
                <View style={styles.topBarcontainer}>
                    <Pressable
                        style={
                            !isParent ?
                                [styles.activeStyle, {
                                    borderTopLeftRadius: 10,
                                    borderBottomLeftRadius: 10,
                                }] :
                                [styles.unActiveStyle, {
                                    borderTopLeftRadius: 10,
                                    borderBottomLeftRadius: 10,
                                }]
                        }
                        onPress={() => {
                            if (isParent) setIsParent(!isParent)
                        }}>
                        <Text
                            style={
                                !isParent ?
                                    styles.activeTextStyle :
                                    styles.unActiveTextStyle
                            }>
                            Individual
                        </Text>
                    </Pressable>
                    <Pressable
                        style={
                            isParent ?
                                [styles.activeStyle, {
                                    borderTopRightRadius: 10,
                                    borderBottomRightRadius: 10,
                                }] :
                                [styles.unActiveStyle, {
                                    borderTopRightRadius: 10,
                                    borderBottomRightRadius: 10,
                                    borderRightWidth: 1,
                                }]
                        }
                        onPress={() => {
                            if (!isParent) setIsParent(!isParent)
                        }}>
                        <Text
                            style={
                                isParent ?
                                    styles.activeTextStyle :
                                    styles.unActiveTextStyle
                            }>
                            Parent
                        </Text>
                    </Pressable>

                </View>

                <InputField
                    customStyle={{ marginTop: 30 }}
                    autoCapitalizes={"none"}
                    placeholder={'Enter first name'}
                    value={name}
                    onChangeText={(text) => {
                        setName(text)
                    }}
                />
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Enter last name'}
                    value={lastName}
                    onChangeText={(text) => {
                        setLastName(text)
                    }}
                />
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Enter email address'}
                    value={email}
                    onChangeText={(text) => {
                        setEmail(text)
                    }}
                />
                <InputField
                    autoCapitalizes={"none"}
                    placeholder={'Enter password'}
                    value={password}
                    onChangeText={(text) => {
                        setPassword(text)
                    }}
                    secureType={true}
                />
                <View style={styles.termsContainer}>
                    <CheckBox
                        onChange={() => { setBox(!checkBox) }}
                        onChangeTerm={() => {
                        }}
                        isChecked={checkBox}
                        tintColor={colors.black}
                        checkstyle={{ backgroundColor: colors.InputFieldBackground }}
                    />
                </View>
                <View style={styles.socialContainer}>
                    <View style={styles.socialImageContainer}>
                        <Image
                            style={styles.socialIcon}
                            source={icons.googleIcon}
                        />
                    </View>
                    <View style={[styles.socialImageContainer, { marginLeft: 20, marginRight: 20 }]}>
                        <Image
                            style={styles.socialIcon}
                            source={icons.facebookIcon}
                        />
                    </View>
                    <View style={styles.socialImageContainer}>
                        <Image
                            style={styles.socialIcon}
                            source={icons.appleIcon}
                        />
                    </View>
                </View>
                <View style={styles.buttonContainer}>
                    <Button
                        text={'Log in'}
                        textStyle={{ color: colors.lightGreen }}
                        backgroundColorStyle={styles.loginBtnStyle}
                        clickAction={loginBtn.bind(this)}
                    />
                    <Button
                        text={'Sign up'}
                        // textStyle={{ }}
                        backgroundColorStyle={{ width: '59%' }}
                        clickAction={() => {
                            signUpBtn()
                        }}
                    />
                </View>
            </KeyboardAwareScrollView>
            <Loader loading={loading} />
        </SafeAreaView>
    );
};

export default SignupScreen;
