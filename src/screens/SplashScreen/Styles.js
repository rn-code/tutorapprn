import { StyleSheet } from 'react-native';
import colors from '../../utils/colors'
import fonts from'../../assets/fonts'
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CAE9E6',
    justifyContent: "center",
    alignItems: "center",
  },
  splashIcon: {
    width: "100%",
    height: 135,
    resizeMode: 'contain'
  },
  circleContainer:{
    width:240,
    height:240,
    borderRadius:120,
    backgroundColor:colors.lightGrey,
    opacity:.5,
    alignItems:'center',
    justifyContent:'center'
  },
  backgroundContainer: {
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'center'
  },
  textTutor:{
    fontSize:24,
    fontFamily:fonts.Bold,
    color:colors.lightGreen
  }
});
