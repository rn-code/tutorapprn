import React, { useEffect } from "react";
import { Image, ImageBackground, View, Text } from "react-native";
import { styles } from './Styles';
import { SafeAreaView } from 'react-native-safe-area-context'
import { useSelector } from 'react-redux'

//assets
import icons from '../../assets/icons'
const SplashScreen = ({ navigation }) => {
    const { isSignedIn } = useSelector((state) => state.userSession)

    useEffect(() => {
        setTimeout(() => {
            console.log('isSignedIn', isSignedIn)
            navigation.replace('AfterSplash')
        }, 4000);
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <ImageBackground style={styles.backgroundContainer} source={icons.splashIcon}>
                <View style={styles.circleContainer}>
                    <Text style={styles.textTutor}>tutorservice</Text>
                </View>
            </ImageBackground>
        </SafeAreaView>
    );
};
export default SplashScreen;
